
<div class="container">
	<form action="" method="post">
		<input type="hidden" name="request_type" value="email">
		<div class="form-group">
			<label>To :</label>
			<input type="text" class="form-control" name="to">
		</div>
		<div class="form-group">
			<label>Subject :</label>
			<input type="text" class="form-control" name="subject">
		</div>
		<div class="form-group">
			<label>Message :</label>
			<input type="text" class="form-control" name="message">
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Kirim</button>
		</div>
	</form>
</div>