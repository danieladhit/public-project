<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends Api_Controller {

	public function index()
	{
		// API Doc page only accessible during development/testing environments
		if (in_array(ENVIRONMENT, array('development', 'testing')))
		{
			$this->mBodyClass = 'swagger-section';
			$this->render('home', 'empty');
		}
		else
		{
			redirect();
		}

	}

	public function login_post() {
		// passed validation
		$identity = $this->input->post('username');
		$password = $this->input->post('password');

		$this->ion_auth_model->tables['users'] = 'employees';
		$this->ion_auth_model->identity_column = 'username';
		if ($this->ion_auth->login($identity, $password))
		{
			// login succeed
			$status = "success";
			$alert = $this->ion_auth->messages();
			$message = $this->ion_auth->user()->row();
		}
		else
		{
			// login failed
			$status = "failed";
			$alert = $this->ion_auth->errors();
			$message = array();
		}

		$response = array(
			'status'	=> $status,
			'alert'		=> $alert,
			'message'	=> $message
		);

		$this->response([$response]);
	}

	public function schedule_post()
	{
		$identity = $this->input->post('username');
		$employee = $this->db->where('username', $identity)
							  ->get('employees')->row();
		$now_day = date("N");
		$schedule = $this->db->select("employee_id,employees_schedule.schedule_id, schedule.name as schedule, schedule.day, schedule.note, places.name as place, places.address, latitude, longitude, 
			employees_schedule.id as employees_schedule_id, schedule_groups.id as schedule_groups_id")
							 ->from("employees_schedule")
							 ->join("schedule", "schedule.id = employees_schedule.schedule_id")
							 ->join("schedule_groups","schedule_groups.schedule_id = schedule.id")
							 ->join("places","places.id = schedule_groups.place_id")
							 ->where("employee_id", $employee->id)
							 ->where("schedule.day", $now_day)
							 ->get()->result_array();

		// get status attendance
	    $filter_schedule = array();
		foreach ($schedule as $key => $res) {
			$checkin_status = 0;
			$checkout_status = 0;
			$attendance = $this->db->where("employees_schedule_id", $res["employees_schedule_id"])
			                       ->where("schedule_groups_id", $res["schedule_groups_id"])
			                       ->get("attendance")
			                       ->row_array();
			if (count($attendance) > 0) {
				$checkin_status = $attendance["checkin_status"];
				$checkout_status = $attendance["checkout_status"];
			}

			$res["checkin_status"] = $checkin_status;
			$res["checkout_status"] = $checkout_status;
			$filter_schedule[] = $res;
		}
		$this->response($filter_schedule);
	}

	public function submit_checkin_post()
	{
		$identity = $this->input->post('username');
		$employee = $this->db->where('username', $identity)
							  ->get('employees')->row();


		$employees_schedule_id = $this->input->post('employees_schedule_id');
		$schedule_groups_id = $this->input->post('schedule_groups_id');
		$caption = $this->input->post('caption');
		$checkin_image = $this->input->post('checkin_image');
		$checkin_status = $this->input->post('checkin_status');

		$attendance_data = array(
			"employees_schedule_id"		=>	$employees_schedule_id,
			"schedule_groups_id"		=>	$schedule_groups_id,
			"checkin_caption"					=>	$caption,
			"checkin_date"				=>	date("Y-m-d"),
			"checkin_time"				=>	date("H:i:s"),
			"checkin_image"				=>	$checkin_image,
			"checkin_status"			=>	$checkin_status
		);

		$insert_status = $this->db->insert("attendance", $attendance_data);

		if ($insert_status) {
			$status = "success";
			$message = "Successfully Create Attendance";
		} else {
			$status = "failed";
			$message = "Failed Create Attendance";
		}

		$this->response(["status"=>$status, "message"=>$message]);
	}

	public function submit_checkout_post()
	{
		$identity = $this->input->post('username');
		$employee = $this->db->where('username', $identity)
							  ->get('employees')->row();


		$employees_schedule_id = $this->input->post('employees_schedule_id');
		$schedule_groups_id = $this->input->post('schedule_groups_id');
		$caption = $this->input->post('caption');
		$checkout_status = $this->input->post('checkout_status');
		$checkout_image = $this->input->post('checkout_image');

		$attendance_data = array(
			"checkout_caption"			=>	$caption,
			"checkout_date"				=>	date("Y-m-d"),
			"checkout_time"				=>	date("H:i:s"),
			"checkout_status"			=>	$checkout_status,
			"checkout_image"			=>	$checkout_image,
			"process_status"			=>	1
		);

		$attendance_validation = array(
			"employees_schedule_id"		=>	$employees_schedule_id,
			"schedule_groups_id"		=>	$schedule_groups_id,
		);

		$update_status = $this->db->update("attendance", $attendance_data, $attendance_validation);

		if ($update_status) {
			$status = "success";
			$message = "Successfully Update Attendance";
		} else {
			$status = "failed";
			$message = "Failed Update Attendance";
		}

		$this->response(["status"=>$status, "message"=>$message]);
	}

	public function get_place_post()
	{
		$identity = $this->input->post('username');
		$day = $this->input->post('day');
		$employee = $this->db->where('username', $identity)
							  ->get('employees')->row();
		
		$schedule = $this->db->select("employee_id,employees_schedule.schedule_id, schedule.name as schedule, schedule.day, schedule.note, places.name as place, places.address, latitude, longitude, 
			employees_schedule.id as employees_schedule_id, schedule_groups.id as schedule_groups_id")
							 ->from("employees_schedule")
							 ->join("schedule", "schedule.id = employees_schedule.schedule_id")
							 ->join("schedule_groups","schedule_groups.schedule_id = schedule.id")
							 ->join("places","places.id = schedule_groups.place_id")
							 ->where("employee_id", $employee->id)
							 ->where("schedule.day", $day)
							 ->get()->result_array();

		// get status attendance
	    $filter_schedule = array();
		foreach ($schedule as $key => $res) {
			$checkin_status = 0;
			$checkout_status = 0;
			$attendance = $this->db->where("employees_schedule_id", $res["employees_schedule_id"])
			                       ->where("schedule_groups_id", $res["schedule_groups_id"])
			                       ->get("attendance")
			                       ->row_array();
			if (count($attendance) > 0) {
				$checkin_status = $attendance["checkin_status"];
				$checkout_status = $attendance["checkout_status"];
			}

			$res["checkin_status"] = $checkin_status;
			$res["checkout_status"] = $checkout_status;
			$filter_schedule[] = $res;
		}
		$this->response($filter_schedule);
	}

	public function get_attendance_post()
	{
		$identity = $this->input->post('username');
		$employee = $this->db->where('username', $identity)
							  ->get('employees')->row();
		$schedule = $this->db->select("employee_id,employees_schedule.schedule_id, schedule.name as schedule, schedule.day, schedule.note, places.name as place, places.address, latitude, longitude, 
			employees_schedule.id as employees_schedule_id, schedule_groups.id as schedule_groups_id,
			checkin_caption,checkout_caption,checkin_date,checkin_time,checkout_date,checkout_time,checkin_image,
			checkout_image,checkin_status,checkout_status,process_status")
							 ->from("attendance")
							 ->join("employees_schedule", "employees_schedule_id = employees_schedule.id")
							 ->join("schedule", "schedule.id = employees_schedule.schedule_id")
							 ->join("schedule_groups","schedule_groups.id = attendance.schedule_groups_id")
							 ->join("places","places.id = schedule_groups.place_id")
							 ->where("employees_schedule.employee_id", $employee->id)
							 ->where("process_status", 1)
							 ->order_by("attendance.id", "DESC")
							 ->get()->result_array();

		$this->response($schedule);
	}


	public function register_agent_post() {
		$serial_key = $this->post('serial_key');
		$mac_address = $this->post('mac_address');
		$host_name = $this->post('host_name');

		$agent_master = $this->db->where('agent_serialkey', $serial_key)
		->get('agent_master')
		->row_array();
		if (count($agent_master) == 0) {
			$data = array(
				"agent_servername"	=> "",
				"agent_serialkey" 	=> $serial_key,
				"agent_type"		=> "",
				"agent_status"		=> "0",
				"listener_status"	=> 0,
				"created_at"		=> date("Y-m-d H:i:s"),
				"updated_at"		=> date("Y-m-d H:i:s"),
				"updated_by"		=> "",
			);
			$this->db->insert("agent_master", $data);
			$agent_master_id = $this->db->insert_id();
			$data['id'] = $agent_master_id;
			$agent_master = $data;
		} else {
			$agent_master_id = $agent_master["id"];
		}

	    // insert to agent_client table
		$agent_code = random_string('alnum',5);
		$agent_client = $this->db->where('agent_host_name', $host_name)
		->get('agent_client')
		->row_array();
		if (count($agent_client) == 0) {
			$data = array(
				'agent_master_id' 	=> $agent_master_id,
				'agent_code'		=> $agent_code,
				'agent_mac_address' => $mac_address,
				'agent_host_name'	=> $host_name,
				'agent_status'		=> 0,
				"created_at"		=> date("Y-m-d H:i:s"),
				"updated_at"		=> date("Y-m-d H:i:s"),
			);
			$this->db->insert("agent_client", $data);
			$agent_client_id = $this->db->insert_id();
			$data['id'] = $agent_client_id;
			$agent_client = $data;
		} 

		$agent_client["main_master_id"] = $agent_master["id"];
		$agent_client["main_client_id"] = $agent_client["id"];
		$agent_client["server_name"] = $agent_master["agent_servername"];
		$agent_client["agent_serialkey"] = $agent_master["agent_serialkey"];
		$agent_client["agent_type"] = $agent_master["agent_type"];

		$this->response($agent_client);
	}

	public function sync_agent_post() {
		$agent_data = $this->post();
		$agent_master = $this->db->where('id', $agent_data["main_master_id"])
		->get('agent_master')
		->row_array();
		if (count($agent_master) > 0) {
			$agent_master_id = $agent_master["id"];

			$agent_client = $this->db->where('id', $agent_data["main_client_id"])
			->get('agent_client')
			->row_array();

			$agent_client_id = $agent_client["id"];
		} 

		$agent_client["server_name"] = $agent_master["agent_servername"];
		$agent_client["agent_serialkey"] = $agent_master["agent_serialkey"];
		$agent_client["agent_type"] = $agent_master["agent_type"];

		if ($agent_master["listener_status"] == 1) {
			$agent_client["agent_status"] = 1;
		} else {
			$agent_client["agent_status"] = 0;
		}

		$this->response($agent_client);
	}

	public function process_post()
	{
		$data = $this->post();
		$data["keterangan"] = "ini post";
		$this->response($data);
	}

	public function process_get()
	{
		$data = $this->get();
		$data["keterangan"] = "ini get";
		$this->response($data);
	}
}
