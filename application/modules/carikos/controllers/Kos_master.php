<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kos_master extends Admin_Controller {

	public function index()
	{
		$this->db->select('vendors.nama as nama_vendor, vendors.telepon as telepon_vendor, vendors.email as email_vendor, kos_master.*')
		->join('vendors', 'vendors.id = kos_master.id_vendor', 'left');
		$this->mViewData['kos_master'] = $this->db->get('kos_master')->result();
		$this->render('kos_master');
	}

	public function tambah()
	{
		$this->mViewData = array(
			'nama_kos' => '',
			'id_vendor' => '',
			'alamat' => '',
			'harga' => '',
			'jumlah' => '',
			'deskripsi' => '',
			'ex_kasur' => '',
			'ex_lemari' => '',
			'ex_ac' => '',
			'ex_kipas' => '',
			'ex_wifi' => '',
			'ex_tv' => ''
		);

		if ($this->input->post('nama_kos')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_kasur','ex_lemari','ex_ac','ex_kipas','ex_wifi','ex_tv');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->insert('kos_master', $data);
			$id_kos = $this->db->insert_id();

			set_alert('success', 'Informasi kos berhasil disimpan');
			redirect($this->mModule.'/kos_master/manage_gallery/'.$id_kos);
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_vendors = $this->db->get('vendors');
		$vendors = $get_vendors->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['vendors'] = $vendors;
		$this->mPageTitle = 'Tambah Kos';
		$this->render('add_kos');
	}

	public function ubah($id_kos)
	{
		// Params Validation
		$get_kos = $this->db->where('id', $id_kos)
		->get('kos_master');
		$kos = $get_kos->row();

		if (empty($kos)) {
			set_alert('danger', 'Data yang anda cari tidak ditemukan');
			redirect($this->mModule.'/kos_master/');
		}

		foreach ($kos as $key => $value) {
			$this->mViewData[$key] = $value;
		}

		if ($this->input->post('nama_kos')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_kasur','ex_lemari','ex_ac','ex_kipas','ex_wifi','ex_tv');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->update('kos_master', $data, ['id' => $id_kos]);

			set_alert('success', 'Informasi kos berhasil diubah');
			redirect($this->mModule.'/kos_master/manage_gallery/'.$id_kos);
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_vendors = $this->db->get('vendors');
		$vendors = $get_vendors->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['vendors'] = $vendors;
		$this->mPageTitle = 'Tambah Kos';
		$this->render('add_kos');
	}

	public function preview($id_kos)
	{
		// ---- KOS DATA -----
		// Params Validation
		$get_kos = $this->db->where('id', $id_kos)
		->get('kos_master');
		$kos = $get_kos->row();

		if (empty($kos)) {
			set_alert('danger', 'Data yang anda cari tidak ditemukan');
			redirect($this->mModule.'/kos_master/');
		}

		foreach ($kos as $key => $value) {
			$this->mViewData[$key] = $value;
		}

		if ($this->input->post('nama_kos')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_kasur','ex_lemari','ex_ac','ex_kipas','ex_wifi','ex_tv');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->update('kos_master', $data, ['id' => $id_kos]);

			set_alert('success', 'Informasi kos berhasil diubah');
			refresh();
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_vendors = $this->db->get('vendors');
		$vendors = $get_vendors->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['vendors'] = $vendors;


		// --- KOS GALLERY ----
		$get_kos_gallery = $this->db->where('id_kos_master', $id_kos)
		->limit(5)
		->order_by('urutan', 'ASC')
		->get('kos_gallery');
		$kos_gallery = $get_kos_gallery->result();

		$images_detail = array();

		// get images detail
		foreach ($kos_gallery as $key => $row) {
			$name = str_replace('./image_upload/kos/', '', $row->foto);
			$size = filesize($row->foto);
			$url = base_url($row->foto);
			$images_detail[$row->urutan] = array('name' => $name, 'size' => $size, 'url' => $url);
		}

		// validation images detail must have 5 files
		$i=1;
		while ($i <= 5) {
			if(!isset($images_detail[$i])) {
				$images_detail[$i] = array();
			}
			$i++;
		}
		$this->mViewData['id_kos'] = $id_kos;
		$this->mViewData['images_detail'] = $images_detail;

		$this->mPageTitle = 'Preview Kos';
		$this->render('preview_kos');
	}

	public function manage_gallery($id_kos = 0) {
		$get_kos_gallery = $this->db->where('id_kos_master', $id_kos)
		->limit(5)
		->order_by('urutan', 'ASC')
		->get('kos_gallery');
		$kos_gallery = $get_kos_gallery->result();

		$images_detail = array();

		// get images detail
		foreach ($kos_gallery as $key => $row) {
			$name = str_replace('./image_upload/kos/', '', $row->foto);
			$size = filesize($row->foto);
			$url = base_url($row->foto);
			$images_detail[$row->urutan] = array('name' => $name, 'size' => $size, 'url' => $url);
		}

		// validation images detail must have 5 files
		$i=1;
		while ($i <= 5) {
			if(!isset($images_detail[$i])) {
				$images_detail[$i] = array();
			}
			$i++;
		}

		//print_r($images_detail); exit();

		$this->mPageTitle = 'Upload Foto Kos';
		$this->mViewData['id_kos'] = $id_kos;
		$this->mViewData['images_detail'] = $images_detail;
		$this->render('add_kos_gallery');
	}

	public function upload_foto($urutan=0, $id_user=0, $id_kos=0)
	{
		// Only Vendor Can Be Upload -- Validation
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		// Params Validation
		$get_kos = $this->db->where('id', $id_kos)
		->get('kos_master');
		$kos = $get_kos->row();

		if (empty($kos)) {
			$this->output->set_status_header('401', 'Kos Data is Missing');
			exit('Kos Data is Missing');
		}

		if ($id_user != $user->id) {
			$this->output->set_status_header('401', 'User Not Found');
			exit('User Not Found');
		}

		$this->load->helper('file_upload_helper');
		$upload_path = './image_upload/kos/';
		$response = image_upload('file', $upload_path);

		if ($response['status']) {
			$data['id_kos_master'] = $id_kos;
			$data['foto'] = $response['value'];
			$data['urutan'] = $urutan;

			$get_kos_gallery = $this->db->where('id_kos_master', $id_kos)
			->where('urutan', $urutan)
			->get('kos_gallery');
			$kos_gallery = $get_kos_gallery->row();

			if (empty($kos_gallery)) {
				$this->db->insert('kos_gallery', $data);
			} else {
				$this->db->update('kos_gallery', $data, ['id' => $kos_gallery->id]);
			}

			exit($response['value']);
		} else {
			$this->output->set_status_header('401', $response['value']);
			exit($response['value']);
		}
	}

	public function delete_foto()
	{
		$filename = $this->input->post('name');
		$upload_path = './image_upload/kos/';
		$full_path = $upload_path . $filename;

		// Delete DB
		$this->db->delete('kos_gallery',['foto' => $full_path]);

		// Delete File
		unlink($full_path);

		exit('Deleted Succesfully');
	}
}
