<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendors extends Admin_Controller {

	public function index()
	{
		$this->mViewData['vendors'] = $this->db->get('vendors')->result();
		$this->render('vendors');
	}

	public function tambah()
	{
		$this->mViewData = array(
			'full_name' => '',
			'email' => '',
			'alamat' => '',
			'telepon' => '',
			'waktu_registrasi' => '',
			'status' => '',
			'username' => ''
		);

		if ($this->input->post('full_name')) {
			// passed validation
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$additional_data = array(
				'full_name'	=> $this->input->post('full_name'),
			);
			$groups = [2]; // vendors

			// create user (default group as "vendors")
			$user = $this->ion_auth->register($username, $password, $email, $additional_data, $groups);
			if ($user)
			{
				// success
				$messages = $this->ion_auth->messages();
				set_alert('success', $messages);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				set_alert('danger', $errors);
				//$this->system_message->set_error($errors);
				refresh();
			}

			$data['nama'] = $this->input->post('full_name');
			$data['email'] = $this->input->post('email');
			$data['alamat'] = $this->input->post('alamat');
			$data['telepon'] = $this->input->post('telepon');
			$data['waktu_registrasi'] = date('Y-m-d H:i:s');
			$data['status'] = 1;
			//$data['password'] = hash_password($data['password']);
			$this->db->insert('vendors', $data);

			set_alert('success', 'Data Vendor berhasil disimpan');
			redirect($this->mModule.'/vendors');
		}

		$this->render('add_vendor');
	}
}
