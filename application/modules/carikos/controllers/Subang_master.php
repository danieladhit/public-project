<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subang_master extends Admin_Controller {

	public function index()
	{
		$this->db->select('vendors.nama as nama_vendor, vendors.telepon as telepon_vendor, vendors.email as email_vendor, subang_master.*')
		->join('vendors', 'vendors.id = subang_master.id_vendor', 'left');
		$this->mViewData['subang_master'] = $this->db->get('subang_master')->result();
		$this->render('subang_master');
	}

	public function tambah()
	{
		$this->mViewData = array(
			'nama_kos' => '',
			'id_vendor' => '',
			'alamat' => '',
			'harga' => '',
			'telepon' => '',
			'deskripsi' => '',
			'latitude' => '',
			'longitude' => '',
			'ex_kasur' => '',
			'ex_lemari' => '',
			'ex_ac' => '',
			'ex_kipas' => '',
			'ex_wifi' => '',
			'ex_tv' => ''
		);

		if ($this->input->post('nama_kos')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_kasur','ex_lemari','ex_ac','ex_kipas','ex_wifi','ex_tv');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->insert('subang_master', $data);
			$id_kos = $this->db->insert_id();

			set_alert('success', 'Informasi Tempat berhasil disimpan');
			redirect($this->mModule.'/subang_master/manage_gallery/'.$id_kos);
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_vendors = $this->db->get('vendors');
		$vendors = $get_vendors->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['vendors'] = $vendors;
		$this->mPageTitle = 'Tambah Tempat';
		$this->render('add_subang');
	}

	public function ubah($id_kos)
	{
		// Params Validation
		$get_kos = $this->db->where('id', $id_kos)
		->get('subang_master');
		$kos = $get_kos->row();

		if (empty($kos)) {
			set_alert('danger', 'Data yang anda cari tidak ditemukan');
			redirect($this->mModule.'/subang_master/');
		}

		foreach ($kos as $key => $value) {
			$this->mViewData[$key] = $value;
		}

		if ($this->input->post('nama_kos')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_kasur','ex_lemari','ex_ac','ex_kipas','ex_wifi','ex_tv');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->update('subang_master', $data, ['id' => $id_kos]);

			set_alert('success', 'Informasi tempat berhasil diubah');
			redirect($this->mModule.'/subang_master/manage_gallery/'.$id_kos);
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_vendors = $this->db->get('vendors');
		$vendors = $get_vendors->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['vendors'] = $vendors;
		$this->mPageTitle = 'Edit tempat';
		$this->render('add_subang');
	}

	public function preview($id_kos)
	{
		// ---- KOS DATA -----
		// Params Validation
		$get_kos = $this->db->where('id', $id_kos)
		->get('subang_master');
		$kos = $get_kos->row();

		if (empty($kos)) {
			set_alert('danger', 'Data yang anda cari tidak ditemukan');
			redirect($this->mModule.'/subang_master/');
		}

		foreach ($kos as $key => $value) {
			$this->mViewData[$key] = $value;
		}

		if ($this->input->post('nama_kos')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_kasur','ex_lemari','ex_ac','ex_kipas','ex_wifi','ex_tv');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->update('subang_master', $data, ['id' => $id_kos]);

			set_alert('success', 'Informasi tempat berhasil diubah');
			refresh();
		}

		// Get User Admin Role
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', 1)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_vendors = $this->db->get('vendors');
		$vendors = $get_vendors->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['vendors'] = $vendors;


		// --- KOS GALLERY ----
		$get_subang_gallery = $this->db->where('id_kos_master', $id_kos)
		->limit(5)
		->order_by('urutan', 'ASC')
		->get('subang_gallery');
		$subang_gallery = $get_subang_gallery->result();

		$images_detail = array();

		// get images detail
		foreach ($subang_gallery as $key => $row) {
			$name = str_replace('./image_upload/kos/', '', $row->foto);
			$size = filesize($row->foto);
			$url = base_url($row->foto);
			$images_detail[$row->urutan] = array('name' => $name, 'size' => $size, 'url' => $url);
		}

		// validation images detail must have 5 files
		$i=1;
		while ($i <= 5) {
			if(!isset($images_detail[$i])) {
				$images_detail[$i] = array();
			}
			$i++;
		}
		$this->mViewData['id_kos'] = $id_kos;
		$this->mViewData['images_detail'] = $images_detail;

		$this->mPageTitle = 'Preview Tempat';
		$this->render('preview_subang');
	}

	public function manage_gallery($id_kos = 0) {
		$get_subang_gallery = $this->db->where('id_kos_master', $id_kos)
		->limit(5)
		->order_by('urutan', 'ASC')
		->get('subang_gallery');
		$subang_gallery = $get_subang_gallery->result();

		$images_detail = array();

		// get images detail
		foreach ($subang_gallery as $key => $row) {
			$name = str_replace('./image_upload/kos/', '', $row->foto);
			$size = filesize($row->foto);
			$url = base_url($row->foto);
			$images_detail[$row->urutan] = array('name' => $name, 'size' => $size, 'url' => $url);
		}

		// validation images detail must have 5 files
		$i=1;
		while ($i <= 5) {
			if(!isset($images_detail[$i])) {
				$images_detail[$i] = array();
			}
			$i++;
		}

		//print_r($images_detail); exit();

		$this->mPageTitle = 'Upload Foto Kos';
		$this->mViewData['id_kos'] = $id_kos;
		$this->mViewData['images_detail'] = $images_detail;
		$this->render('add_subang_gallery');
	}

	public function upload_foto($urutan=0, $id_kos=0)
	{
		// Only Vendor Can Be Upload -- Validation
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		// Params Validation
		$get_kos = $this->db->where('id', $id_kos)
		->get('subang_master');
		$kos = $get_kos->row();

		if (empty($kos)) {
			$this->output->set_status_header('401', 'Kos Data is Missing');
			exit('Kos Data is Missing');
		}

		$this->load->helper('file_upload_helper');
		$upload_path = './image_upload/kos/';
		$response = image_upload('file', $upload_path);

		if ($response['status']) {
			$data['id_kos_master'] = $id_kos;
			$data['foto'] = $response['value'];
			$data['urutan'] = $urutan;

			$get_subang_gallery = $this->db->where('id_kos_master', $id_kos)
			->where('urutan', $urutan)
			->get('subang_gallery');
			$subang_gallery = $get_subang_gallery->row();

			if (empty($subang_gallery)) {
				$this->db->insert('subang_gallery', $data);
			} else {
				$this->db->update('subang_gallery', $data, ['id' => $subang_gallery->id]);
			}

			exit($response['value']);
		} else {
			$this->output->set_status_header('401', $response['value']);
			exit($response['value']);
		}
	}

	public function delete_foto()
	{
		$filename = $this->input->post('name');
		$upload_path = './image_upload/kos/';
		$full_path = $upload_path.$filename;

		// Delete DB
		$this->db->delete('subang_gallery',['foto' => $full_path]);

		// Delete File
		unlink($full_path);

		exit('Deleted Succesfully');
	}
}
