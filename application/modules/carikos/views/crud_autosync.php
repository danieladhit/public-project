<?php if ( !empty($crud_note) ) echo "<p>$crud_note</p>"; ?>

<?php if ( !empty($crud_output) ) echo $crud_output; ?>

<div class="modal fade" id="modal-default">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Default Modal</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-3">
							<img src="https://cdn.dribbble.com/users/533705/screenshots/3811091/sublime-icon.png" class="img-responsive app_logo" alt="" width="100%" height="auto">
						</div>
						<div class="col-sm-9">
							<p>First Opened : <span class="first_opened"></span></p>
							<p>Last Opened : <span class="last_opened"></span></p>
							<p>Total Duration : <span class="duration"></span></p>
						</div>
					</div>
					<hr>
					<h4>Summary</h4>
					<p>Last Usage : <span class="current"></span></p>
					<p>Today Usage : <span class="today_usage"></span></p>
					<p>Usage of the last 7 days : <span class="weekly"></span></p>
					<p>Usage of the last 1 month : <span class="monthly"></span></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<script>
		function showPreview (app) {
			var review = app.data("review");
			var first_opened = app.data("firstopened");
			var last_opened = app.data("lastopened");
			var duration = app.data("duration");

			console.log('Review : ' + review);
			console.log('First Opened : ' + first_opened);
			console.log('Last Opened : ' + last_opened);
			console.log('Duration : ' + duration);

			$(".modal-title").html(review['name']);
			$(".first_opened").html(first_opened['process_start']);
			$(".last_opened").html(last_opened['process_start']);
			$(".duration").html(first_opened['duration']);

			$(".app_logo").attr("src", review["logo"]);
			$(".today_usage").html(duration['today']);
			$(".current").html(duration['current']);
			$(".weekly").html(duration['weekly']);
			$(".monthly").html(duration['monthly']);
		}
	</script>

	<script>
	/*setInterval(function() {
        window.location.reload();
    }, 5000 );*/

    var startTime = new Date(),
    outputDiv = document.getElementsByClassName('process_duration'); // or $('#output').get(0);
    outputDiv.innerHTML = "hoho";

    function getTimezoneOffset() {
    	var d = new Date();
		var n = d.getTimezoneOffset();
		var timezone = n / -60;
		console.log(timezone);

		return timezone;
    }
    
    var timeCounter = 0; // on second
    setInterval(function () {
		//$(".process_duration").html(new Date() - startTime);
	    //outputDiv.innerHTML = "ms since the start: " + (new Date() - startTime);

	    //time counter
	    timeCounter += 1000;

	    jQuery('.process_duration').each(function() {
	    	var currentElement = $(this);

	    	var now_date = currentElement.data('now');
	    	var process_start = currentElement.data('start');
	    	var value = currentElement.data('value');
	    	var id = currentElement.data('id'); 

	    	var now = new Date();

	    	var now_server = new Date(now_date).getTime(); // convert to milisecond
	    	var now_server_runtime = now_server + timeCounter; // sum per 1000ms local time with interval client time

	    	var dateStart = new Date(process_start);
	    	var differenceTime = (now_server_runtime - dateStart);
	    	console.log("timecounter : "+timeCounter);
	    	console.log("now : "+now_server_runtime);
	    	console.log("dateStart : "+dateStart);

	    	var seconds = new Date(differenceTime).getSeconds();
	    	var minutes = new Date(differenceTime).getMinutes();

	    	var offset = getTimezoneOffset(); //Singapore Timezone set
	    	console.log(offset);

	    	//var hour = new Date(differenceTime).getHours() - offset;
	    	var hour = Math.floor(differenceTime/3600000);

	    	if (value == "00:00:00") {
	    		$(id).html("<em>" + hour + " hr " + minutes + " min " + seconds + " sec</em>");
				//$(id).html(differenceTime);
			}
		    // TODO: do something with the value
		});
	}, 1000);
</script>

