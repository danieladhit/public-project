<?=alert_box() ?>
<div class="row">
	<div class="col-md-6">
		<!--begin::Portlet-->
		<div class="m-portlet m-portlet--tab">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
							<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">
							Informasi Kos
						</h3>
					</div>
				</div>
			</div>

			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="">
				<div class="m-portlet__body">
					<?php if($user_role->role_name == 'admin'){ ?>
					<div class="form-group m-form__group">
						<label for="id_vendor">*Pilih Vendor</label>
						<select class="form-control m-input" id="id_vendor"  name="id_vendor" value="<?=$id_vendor ?>" required="required">
							<?php foreach($vendors as $key => $row){ ?>
							<option <?=$row->id == $id_vendor ? 'selected' : ''  ?> value="<?=$row->id ?>"><?=$row->nama.' - '.$row->email ?></option>
							<?php } ?>
						</select>
					</div>
					<?php } ?>
					<div class="form-group m-form__group">
						<label for="nama_kos">*Nama Kos</label>
						<input type="text" class="form-control m-input" id="nama_kos" aria-describedby="nama_kos" name="nama_kos" value="<?=$nama_kos ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="alamat">*Alamat Kos</label>
						<input type="text" class="form-control m-input" id="alamat" aria-describedby="alamat" name="alamat" value="<?=$alamat ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="harga">*Harga Kos</label>
						<input type="number" class="form-control m-input" id="harga" aria-describedby="harga" name="harga" value="<?=$harga ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="jumlah">*Jumlah</label>
						<input type="number" class="form-control m-input" id="jumlah" aria-describedby="jumlah" name="jumlah" value="<?=$jumlah ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="deskripsi">*Deskripsi</label>
						<textarea class="form-control m-input" id="deskripsi" rows="3" name="deskripsi" required="required"><?=$deskripsi ?></textarea>
					</div> 
					<div class="m-form__group form-group">
						<label>Fasilitas</label>
						<div class="m-checkbox-list">
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_kasur == 1 ? 'checked' : '' ?> name="ex_kasur"> Kasur
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_lemari == 1 ? 'checked' : '' ?> name="ex_lemari"> Lemari
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_ac == 1 ? 'checked' : '' ?> name="ex_ac"> AC
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_kipas == 1 ? 'checked' : '' ?> name="ex_kipas"> Kipas
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_wifi == 1 ? 'checked' : '' ?> name="ex_wifi"> Wifi
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_tv == 1 ? 'checked' : '' ?> name="ex_tv"> TV
								<span></span>
							</label>
						</div>
						<span class="m-form__help">Pilih fasilitas yang ada di kos ini</span>
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="./kos_master" class="btn btn-secondary">Back</a>
					</div>
				</div>
			</form>

			<!--end::Form-->
		</div>

		<!--end::Portlet-->

	</div>
	<div class="col-md-6">
		<!--begin::Portlet-->
		<div class="m-portlet m-portlet--tab">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
							<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">
							Foto Kos
						</h3>
					</div>
				</div>
			</div>

			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12">Foto-1</label>
						<div class="col-md-9 col-sm-12">
							<div class="m-dropzone dropzone" action="kos_master/upload_foto/1/<?=$user->id ?>/<?=$id_kos ?>" id="m-dropzone-one">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12">Foto-2</label>
						<div class="col-md-9 col-sm-12">
							<div class="m-dropzone dropzone" action="kos_master/upload_foto/2/<?=$user->id ?>/<?=$id_kos ?>" id="m-dropzone-two">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12">Foto-3</label>
						<div class="col-md-9 col-sm-12">
							<div class="m-dropzone dropzone" action="kos_master/upload_foto/3/<?=$user->id ?>/<?=$id_kos ?>" id="m-dropzone-three">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12">Foto-4</label>
						<div class="col-md-9 col-sm-12">
							<div class="m-dropzone dropzone" action="kos_master/upload_foto/4/<?=$user->id ?>/<?=$id_kos ?>" id="m-dropzone-four">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12">Foto-5</label>
						<div class="col-md-9 col-sm-12">
							<div class="m-dropzone dropzone" action="kos_master/upload_foto/5/<?=$user->id ?>/<?=$id_kos ?>" id="m-dropzone-five">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions">
						<div class="row">
							<div class="col-lg-9 ml-lg-auto">
								<a href="./kos_master" class="btn btn-brand">Selesai</a>
							</div>
						</div>
					</div>
				</div>
			</form>

			<!--end::Form-->
		</div>
	</div>
</div>

<script>

	var DropzoneDemo= {
		init:function() {
			Dropzone.options.mDropzoneOne= {
				paramName:"file",
				maxFiles:1,
				maxFilesize:5,
				addRemoveLinks:!0,
				acceptedFiles:"image/*",
				accept:function(e, o) {
					"justinbieber.jpg"==e.name?o("Naha, you don't."): o()
				},
				removedfile: function(file) {
					delete_foto(file);
				},
				init: function() {
					var file = <?=json_encode($images_detail[1]) ?>;
					var isAvailable = <?=empty($images_detail[1]) ? 0 : 1 ?>;
					if (isAvailable) {
						var myDropzone = this;
						myDropzone.emit("addedfile", file);
						myDropzone.emit("thumbnail", file, file.url);
						myDropzone.emit("complete", file);  
						myDropzone.files.push(file);
						$('.dz-image img').css('width', '100%');
						$('.dz-image img').css('height', '100%');
					}
				}
			},
			Dropzone.options.mDropzoneTwo= {
				paramName:"file",
				maxFiles:1,
				maxFilesize:5,
				addRemoveLinks:!0,
				acceptedFiles:"image/*",
				accept:function(e, o) {
					"justinbieber.jpg"==e.name?o("Naha, you don't."): o()
				},
				removedfile: function(file) {
					delete_foto(file);
				},
				init: function() {
					var file = <?=json_encode($images_detail[2]) ?>;
					var isAvailable = <?=empty($images_detail[2]) ? 0 : 1 ?>;
					if (isAvailable) {
						var myDropzone = this;
						myDropzone.emit("addedfile", file);
						myDropzone.emit("thumbnail", file, file.url);
						myDropzone.emit("complete", file);  
						myDropzone.files.push(file);
						$('.dz-image img').css('width', '100%');
						$('.dz-image img').css('height', '100%');
					}
				}
			},
			Dropzone.options.mDropzoneThree= {
				paramName:"file",
				maxFiles:1,
				maxFilesize:5,
				addRemoveLinks:!0,
				acceptedFiles:"image/*",
				accept:function(e, o) {
					"justinbieber.jpg"==e.name?o("Naha, you don't."): o()
				},
				removedfile: function(file) {
					delete_foto(file);
				},
				init: function() {
					var file = <?=json_encode($images_detail[3]) ?>;
					var isAvailable = <?=empty($images_detail[3]) ? 0 : 1 ?>;
					if (isAvailable) {
						var myDropzone = this;
						myDropzone.emit("addedfile", file);
						myDropzone.emit("thumbnail", file, file.url);
						myDropzone.emit("complete", file);  
						myDropzone.files.push(file);
						$('.dz-image img').css('width', '100%');
						$('.dz-image img').css('height', '100%');
					}
				}
			},
			Dropzone.options.mDropzoneFour= {
				paramName:"file",
				maxFiles:1,
				maxFilesize:5,
				addRemoveLinks:!0,
				acceptedFiles:"image/*",
				accept:function(e, o) {
					"justinbieber.jpg"==e.name?o("Naha, you don't."): o()
				},
				removedfile: function(file) {
					delete_foto(file);
				},
				init: function() {
					var file = <?=json_encode($images_detail[4]) ?>;
					var isAvailable = <?=empty($images_detail[4]) ? 0 : 1 ?>;
					if (isAvailable) {
						var myDropzone = this;
						myDropzone.emit("addedfile", file);
						myDropzone.emit("thumbnail", file, file.url);
						myDropzone.emit("complete", file);  
						myDropzone.files.push(file);
						$('.dz-image img').css('width', '100%');
						$('.dz-image img').css('height', '100%');
					}
				}
			},
			Dropzone.options.mDropzoneFive= {
				paramName:"file",
				maxFiles:1,
				maxFilesize:5,
				addRemoveLinks:!0,
				acceptedFiles:"image/*",
				accept:function(e, o) {
					"justinbieber.jpg"==e.name?o("Naha, you don't."): o()
				},
				removedfile: function(file) {
					delete_foto(file);
				},
				init: function() {
					var file = <?=json_encode($images_detail[5]) ?>;
					var isAvailable = <?=empty($images_detail[5]) ? 0 : 1 ?>;
					if (isAvailable) {
						var myDropzone = this;
						myDropzone.emit("addedfile", file);
						myDropzone.emit("thumbnail", file, file.url);
						myDropzone.emit("complete", file);  
						myDropzone.files.push(file);
						$('.dz-image img').css('width', '100%');
						$('.dz-image img').css('height', '100%');
					}
				}
			}
		}
	}

	;
	DropzoneDemo.init();

	function delete_foto(file) {
		var name = file.name; 
		$.ajax({
			type: 'POST',
			url: 'kos_master/delete_foto',
			data: {name: name},
			success: function(data){
				console.log('success: ' + data);
				var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
			}
		});
		
	}
</script>