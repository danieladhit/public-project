
<?php
foreach ($scripts['foot'] as $file)
{
	$url = starts_with($file, 'http') ? $file : base_url($file);
	echo "<script src='$url'></script>".PHP_EOL;
}
?>

<?php // Google Analytics ?>
<?php $this->load->view('_partials/ga') ?>

<script>
	$('#commonDatatable').DataTable({
		/*dom: 'lBfrtip',*/
		dom:"<'row'<'col-sm-12 col-md-5'B><'col-sm-12 col-md-7'f>><'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>", 
		/*dom:"<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>", */
		buttons: [
		'csv', 'excel', 'pdf', 'print'
		],
		lengthChange : false,
		paging      : true,
		searching   : true,
		ordering    : true,
		info        : true,
		autoWidth   : false,
		pageLength  : 10,
		responsive:!0, 
	});

</script>

</body>
</html>