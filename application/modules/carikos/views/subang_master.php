<?=alert_box() ?>
<!-- START TABLE PORTLET-->
<div class="m-portlet m-portlet--mobile">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data Kos
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="subang_master/tambah" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>Tambah Kos</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="commonDatatable">
			<thead>
				<tr>
					<th>Vendor</th>
					<th>Nama Tempat</th>
					<th>Alamat</th>
					<th>Kategori</th>
					<th>Telepon</th>
					<th>Latitude</th>
					<th>Longitude</th>
					<th>Deskripsi</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($subang_master as $key => $row) { ?>
				<tr>
					<td><?=$row->nama_vendor ?></td>
					<td>
						<a href="subang_master/preview/<?=$row->id ?>">
							<?=$row->nama_kos ?>
						</a>
					</td>
					<td><?=$row->alamat ?></td>
					<td><?=$row->harga ?></td>
					<td>
						<?php 
						echo $row->telepon;
						?>
					</td>
					<td><?=$row->latitude ?></td>
					<td><?=$row->longitude ?></td>
					<td><?=character_limiter($row->deskripsi, 20); ?></td>
					<td>
						<?php
						$status = $row->status == 1 ? 'Available' : 'Not Available';
						$badge_type = $row->status == 1 ? 'success' : 'danger';
						?>
						<span style="width: 110px">	
							<span class="m-badge m-badge--<?=$badge_type ?> m-badge--wide"><?=$status ?></span>
						</span>	
					</td>
					<td>
						<a href="subang_master/ubah/<?=$row->id ?>" class="btn btn-warning m-btn m-btn--icon m-btn--icon-only">
							<i class="fa fa-edit"></i>
						</a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!-- END TABLE PORTLET-->