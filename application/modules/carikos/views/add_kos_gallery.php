<?=alert_box() ?>
<div class="row">
	<div class="col-md-6">
		<!--begin::Portlet-->
		<div class="m-portlet m-portlet--tab">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
							<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">
							Foto Kos
						</h3>
					</div>
				</div>
			</div>

			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right">
				<div class="m-portlet__body">
					<div class="form-group m-form__group m--margin-top-10">
						<div class="alert m-alert m-alert--default" role="alert">
							Langkah 2 - Upload foto untuk kelengkapan data (minimal 1 foto)
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12">Foto-1</label>
						<div class="col-md-9 col-sm-12">
							<div class="m-dropzone dropzone" action="kos_master/upload_foto/1/<?=$user->id ?>/<?=$id_kos ?>" id="m-dropzone-one">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12">Foto-2</label>
						<div class="col-md-9 col-sm-12">
							<div class="m-dropzone dropzone" action="kos_master/upload_foto/2/<?=$user->id ?>/<?=$id_kos ?>" id="m-dropzone-two">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12">Foto-3</label>
						<div class="col-md-9 col-sm-12">
							<div class="m-dropzone dropzone" action="kos_master/upload_foto/3/<?=$user->id ?>/<?=$id_kos ?>" id="m-dropzone-three">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12">Foto-4</label>
						<div class="col-md-9 col-sm-12">
							<div class="m-dropzone dropzone" action="kos_master/upload_foto/4/<?=$user->id ?>/<?=$id_kos ?>" id="m-dropzone-four">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12">Foto-5</label>
						<div class="col-md-9 col-sm-12">
							<div class="m-dropzone dropzone" action="kos_master/upload_foto/5/<?=$user->id ?>/<?=$id_kos ?>" id="m-dropzone-five">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions">
						<div class="row">
							<div class="col-lg-9 ml-lg-auto">
								<a href="./kos_master" class="btn btn-brand">Selesai</a>
							</div>
						</div>
					</div>
				</div>
			</form>

			<!--end::Form-->
		</div>
	</div>
</div>

<script>

	var DropzoneDemo= {
		init:function() {
			Dropzone.options.mDropzoneOne= {
				paramName:"file",
				maxFiles:1,
				maxFilesize:5,
				addRemoveLinks:!0,
				acceptedFiles:"image/*",
				accept:function(e, o) {
					"justinbieber.jpg"==e.name?o("Naha, you don't."): o()
				},
				removedfile: function(file) {
					delete_foto(file);
				},
				init: function() {
					var file = <?=json_encode($images_detail[1]) ?>;
					var isAvailable = <?=empty($images_detail[1]) ? 0 : 1 ?>;
					if (isAvailable) {
						var myDropzone = this;
						myDropzone.emit("addedfile", file);
						myDropzone.emit("thumbnail", file, file.url);
						myDropzone.emit("complete", file);  
						myDropzone.files.push(file);
						$('.dz-image img').css('width', '100%');
						$('.dz-image img').css('height', '100%');
					}
				}
			},
			Dropzone.options.mDropzoneTwo= {
				paramName:"file",
				maxFiles:1,
				maxFilesize:5,
				addRemoveLinks:!0,
				acceptedFiles:"image/*",
				accept:function(e, o) {
					"justinbieber.jpg"==e.name?o("Naha, you don't."): o()
				},
				init: function() {
					var file = <?=json_encode($images_detail[2]) ?>;
					var isAvailable = <?=empty($images_detail[2]) ? 0 : 1 ?>;
					if (isAvailable) {
						var myDropzone = this;
						myDropzone.emit("addedfile", file);
						myDropzone.emit("thumbnail", file, file.url);
						myDropzone.emit("complete", file);  
						myDropzone.files.push(file);
						$('.dz-image img').css('width', '100%');
						$('.dz-image img').css('height', '100%');
					}
				}
			},
			Dropzone.options.mDropzoneThree= {
				paramName:"file",
				maxFiles:1,
				maxFilesize:5,
				addRemoveLinks:!0,
				acceptedFiles:"image/*",
				accept:function(e, o) {
					"justinbieber.jpg"==e.name?o("Naha, you don't."): o()
				},
				init: function() {
					var file = <?=json_encode($images_detail[3]) ?>;
					var isAvailable = <?=empty($images_detail[3]) ? 0 : 1 ?>;
					if (isAvailable) {
						var myDropzone = this;
						myDropzone.emit("addedfile", file);
						myDropzone.emit("thumbnail", file, file.url);
						myDropzone.emit("complete", file);  
						myDropzone.files.push(file);
						$('.dz-image img').css('width', '100%');
						$('.dz-image img').css('height', '100%');
					}
				}
			},
			Dropzone.options.mDropzoneFour= {
				paramName:"file",
				maxFiles:1,
				maxFilesize:5,
				addRemoveLinks:!0,
				acceptedFiles:"image/*",
				accept:function(e, o) {
					"justinbieber.jpg"==e.name?o("Naha, you don't."): o()
				},
				init: function() {
					var file = <?=json_encode($images_detail[4]) ?>;
					var isAvailable = <?=empty($images_detail[4]) ? 0 : 1 ?>;
					if (isAvailable) {
						var myDropzone = this;
						myDropzone.emit("addedfile", file);
						myDropzone.emit("thumbnail", file, file.url);
						myDropzone.emit("complete", file);  
						myDropzone.files.push(file);
						$('.dz-image img').css('width', '100%');
						$('.dz-image img').css('height', '100%');
					}
				}
			},
			Dropzone.options.mDropzoneFive= {
				paramName:"file",
				maxFiles:1,
				maxFilesize:5,
				addRemoveLinks:!0,
				acceptedFiles:"image/*",
				accept:function(e, o) {
					"justinbieber.jpg"==e.name?o("Naha, you don't."): o()
				},
				init: function() {
					var file = <?=json_encode($images_detail[5]) ?>;
					var isAvailable = <?=empty($images_detail[5]) ? 0 : 1 ?>;
					if (isAvailable) {
						var myDropzone = this;
						myDropzone.emit("addedfile", file);
						myDropzone.emit("thumbnail", file, file.url);
						myDropzone.emit("complete", file);  
						myDropzone.files.push(file);
						$('.dz-image img').css('width', '100%');
						$('.dz-image img').css('height', '100%');
					}
				}
			}
		}
	};
	DropzoneDemo.init();

	function delete_foto(file) {
		var name = file.name; 
		$.ajax({
			type: 'POST',
			url: 'kos_master/delete_foto',
			data: {name: name},
			success: function(data){
				console.log('success: ' + data);
				var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
			}
		});
		
	}

</script>