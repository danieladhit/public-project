<!-- START TABLE PORTLET-->
<div class="m-portlet m-portlet--mobile">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data Orders
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<!-- <li class="m-portlet__nav-item">
					<a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>New record</span>
						</span>
					</a>
				</li> -->
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="commonDatatable">
			<thead>
				<tr>
					<th>ID</th>
					<th>Customer</th>
					<th>Vendor</th>
					<th>Nama Kos</th>
					<th>Lama Sewa</th>
					<th>Harga</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($orders as $key => $row) { ?>
				<tr>
					<td><?=$row->id ?></td>
					<td><?=$row->nama_customer ?></td>
					<td><?=$row->nama_vendor ?></td>
					<td><?=$row->nama_kos ?></td>
					<td><?=$row->keterangan ?></td>
					<td><?=$row->harga ?></td>
					<td>
						<?php
						$status = $row->status == 1 ? 'Lunas' : 'Belum Lunas';
						$badge_type = $row->status == 1 ? 'success' : 'danger';
						?>
						<span style="width: 110px">	
							<span class="m-badge m-badge--<?=$badge_type ?> m-badge--wide"><?=$status ?></span>
						</span>	
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!-- END TABLE PORTLET-->