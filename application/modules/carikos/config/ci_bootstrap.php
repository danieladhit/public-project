<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CI Bootstrap 3 Configuration
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views 
| when calling MY_Controller's render() function. 
| 
| See example and detailed explanation from:
| 	/application/config/ci_bootstrap_example.php
*/
$template_path = 'themes/metronic/default/dist/demo12/';
$body_class = 'm-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default';

$config['ci_bootstrap'] = array(

	// Site name
	'site_name' => 'Admin',

	// Site Logo
	'site_logo' => '/assets/image/logo.png',

	// Template Path
	'template_path' => $template_path,

	// Default page title prefix
	'page_title_prefix' => '',

	// Default page title
	'page_title' => '',

	// Default meta data
	'meta_data'	=> array(
		'author'		=> '',
		'description'	=> '',
		'keywords'		=> ''
	),
	
	// Default scripts to embed at page head or end
	'scripts' => array(
		'head'	=> array(
			$template_path."assets/vendors/base/vendors.bundle.js",
			$template_path."assets/demo/demo12/base/scripts.bundle.js",
			$template_path."assets/vendors/custom/fullcalendar/fullcalendar.bundle.js",
			$template_path."assets/app/js/dashboard.js",
		),
		'foot'	=> array(
			$template_path."assets/vendors/custom/datatables/datatables.bundle.js",
			'assets/dist/admin/dataTables.buttons.min.js',
			'assets/dist/admin/buttons.flash.min.js',
			'assets/dist/admin/jszip.min.js',
			'assets/dist/admin/pdfmake.min.js',
			'assets/dist/admin/vfs_fonts.js',
			'assets/dist/admin/buttons.html5.min.js',
			'assets/dist/admin/buttons.print.min.js',
		),
	),

	// Default stylesheets to embed at page head
	'stylesheets' => array(
		'screen' => array(
			$template_path."assets/vendors/base/vendors.bundle.css",
			$template_path."assets/demo/demo12/base/style.bundle.css",
			$template_path."assets/vendors/custom/fullcalendar/fullcalendar.bundle.css",
			$template_path."assets/vendors/custom/datatables/datatables.bundle.css",
			'assets/dist/admin/buttons.dataTables.min.css',
		)
	),

	// Default CSS class for <body> tag
	'body_class' => $body_class,
	
	// Multilingual settings
	'languages' => array(
	),

	// Menu items
	'menu' => array(
		/*'home' => array(
			'name'		=> 'Home',
			'url'		=> '',
			'icon'		=> 'fa fa-home',
		),*/
		'home' => array(
			'name'		=> 'Home',
			'url'		=> 'home',
			'icon'		=> 'flaticon-line-graph',
			/*'children'  => array(
				'List'			=> 'report',
			)*/
		),
		'customers' => array(
			'name'		=> 'Customers',
			'url'		=> 'customers',
			'icon'		=> 'fa fa-users',
		),
		'vendors' => array(
			'name'		=> 'Vendors',
			'url'		=> 'vendors',
			'icon'		=> 'fa fa-user-plus',
		),
		'orders' => array(
			'name'		=> 'Orders',
			'url'		=> 'orders',
			'icon'		=> 'fa fa-bars',
		),
		'kos_master' => array(
			'name'		=> 'Data Kos',
			'url'		=> 'kos_master',
			'icon'		=> 'fa fa-home',
		),
		/*'user' => array(
			'name'		=> 'Users',
			'url'		=> 'user',
			'icon'		=> 'fa fa-users',
			'children'  => array(
				'List'			=> 'user',
				'Create'		=> 'user/create',
				'User Groups'	=> 'user/group',
			)
		),
		'panel' => array(
			'name'		=> 'Admin Panel',
			'url'		=> 'panel',
			'icon'		=> 'fa fa-cog',
			'children'  => array(
				'Admin Users'			=> 'panel/admin_user',
				'Create Admin User'		=> 'panel/admin_user_create',
				'Admin User Groups'		=> 'panel/admin_user_group',
			)
		),
		'util' => array(
			'name'		=> 'Utilities',
			'url'		=> 'util',
			'icon'		=> 'fa fa-cogs',
			'children'  => array(
				'Database Versions'		=> 'util/list_db',
			)
		),*/
		'logout' => array(
			'name'		=> 'Sign Out',
			'url'		=> 'panel/logout',
			'icon'		=> 'fa fa-sign-out-alt',
		)
	),

	// Login page
	'login_url' => 'carikos/login',

	// Restricted pages
	'page_auth' => array(
		'vendors'					=> array('admin'),
		'customers'					=> array('admin'),
	),

	// AdminLTE settings
	'adminlte' => array(
		'body_class' => array(
			'webmaster'		=> $body_class,
			'superadmin'	=> $body_class,
			'admin'			=> $body_class,
			'user'			=> $body_class,
			'vendor'		=> $body_class,
		)
	),

	// Useful links to display at bottom of sidemenu
	'useful_links' => array(
		/*array(
			'auth'		=> array('webmaster', 'admin', 'manager', 'staff'),
			'name'		=> 'Frontend Website',
			'url'		=> '',
			'target'	=> '_blank',
			'color'		=> 'text-aqua'
		),
		array(
			'auth'		=> array('webmaster', 'admin'),
			'name'		=> 'API Site',
			'url'		=> 'api',
			'target'	=> '_blank',
			'color'		=> 'text-orange'
		),
		array(
			'auth'		=> array('webmaster', 'admin', 'manager', 'staff'),
			'name'		=> 'Github Repo',
			'url'		=> CI_BOOTSTRAP_REPO,
			'target'	=> '_blank',
			'color'		=> 'text-green'
		),*/
	),

	// Debug tools
	'debug' => array(
		'view_data'	=> FALSE,
		'profiler'	=> FALSE
	),
);

/*
| -------------------------------------------------------------------------
| Override values from /application/config/config.php
| -------------------------------------------------------------------------
*/
$config['sess_cookie_name'] = 'ci_session_admin';
