<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asm extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$crud = $this->generate_crud('gkp_asm');
		$crud->columns('name', 'birth', 'foto', 'gkp_tingkat_id', 'gkp_sekolah_id', 'sd_kelas', 'kehadiran','bintang','ayat_hafalan','note');
		$this->unset_crud_fields('created_at', 'updated_at', 'created_by', 'updated_by');

		$crud->set_relation('gkp_tingkat_id', 'gkp_tingkat', 'name');

		$crud->set_relation('gkp_sekolah_id', 'gkp_sekolah', 'name');

		// disable direct create / delete Frontend User
		//$crud->unset_add();
		$crud->unset_delete();

		$this->mPageTitle = 'Anak Sekolah Minggu';
		$this->render_crud();
	}

	public function tingkat()
	{
		$crud = $this->generate_crud('gkp_tingkat');
		
		$this->mPageTitle = 'Manage Kelas/Tingkat SM';
		$this->render_crud();
	}

	public function sekolah()
	{
		$crud = $this->generate_crud('gkp_sekolah');
		
		$this->mPageTitle = 'Manage Sekolah Umum/Pendidikan';
		$this->render_crud();
	}

	public function bintang()
	{
		$crud = $this->generate_crud('gkp_asm_bintang');

		$crud->columns("gkp_gsm_id", "asm_yang_hadir", "label", "bintang");

		$crud->set_relation('gkp_gsm_id', 'gkp_gsm', 'full_name');

		$crud->set_relation_n_n('asm_yang_hadir', 'gkp_asm_bintang_groups', 'gkp_asm_kehadiran', 'gkp_asm_bintang_id', 'gkp_asm_kehadiran_id', 'gkp_routine_detail_id');

		
		$this->mPageTitle = 'Routine Event Per Week';
		$this->render_crud();
	}

	public function kehadiran()
	{
		$crud = $this->generate_crud('gkp_routine_detail');

		$crud->columns("gkp_schedule_id", "asm_kehadiran");

		$crud->fields("asm_kehadiran");

		$crud->set_relation('gkp_schedule_id', 'gkp_schedule', 'date');

		$crud->set_relation_n_n('asm_kehadiran', 'gkp_asm_kehadiran', 'gkp_asm', 'gkp_routine_detail_id', 'gkp_asm_id', 'name');

		
		$this->mPageTitle = 'Kehadiran Tiap Minggu';
		$this->render_crud();
	}

	
}
