<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gsm extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$crud = $this->generate_crud('gkp_gsm');
		$crud->columns('username', 'email', 'full_name', 'active');
		$this->unset_crud_fields('ip_address', 'last_login');

		// only webmaster and admin can reset user password
		if ($this->ion_auth->in_group(array('webmaster', 'admin')))
		{
			$crud->add_action('Reset Password', '', 'gkpspi-admin/gsm/reset_password', 'fa fa-repeat');
		}

		// disable direct create / delete Frontend User
		$crud->unset_add();
		$crud->unset_delete();

		$this->mPageTitle = 'List GSM';
		$this->render_crud();
	}

	// Create Frontend User
	public function create()
	{
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$identity = empty($username) ? $email : $username;
			$data = array(
				'username'		=> $username,
				'email'			=> $email,
				'password'		=> $this->ion_auth_model->hash_password($password),
				'active'		=> 1,
				'created_on'	=> time(),
				'full_name'	=> $this->input->post('full_name'),
			);
			
			// proceed to create user
			$this->db->insert('gkp_gsm', $data);
			$user_id = $this->db->insert_id();
			if ($user_id)
			{
				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);

				// directly activate user
				$this->ion_auth->activate($user_id);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		$this->mPageTitle = 'Create GSM';

		$this->mViewData['form'] = $form;
		$this->render('gsm/create');
	}

	
	// Frontend User Reset Password
	public function reset_password($user_id)
	{
		// only top-level users can reset user passwords
		$this->verify_auth(array('webmaster', 'admin'));

		$form = $this->form_builder->create_form();
		if ($form->validate())
		{
			// pass validation
			$data = array('password' => $this->input->post('new_password'));
			
			// proceed to change user password
			$this->ion_auth_model->tables['users'] = 'gkp_gsm';
			if ($this->ion_auth->update($user_id, $data))
			{
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
			}
			else
			{
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		$this->load->model('Gsm_model', 'gsm');
		$target = $this->gsm->get($user_id);
		$this->mViewData['target'] = $target;

		$this->mViewData['form'] = $form;
		$this->mPageTitle = 'Reset User Password';
		$this->render('gsm/reset_password');
	}

	public function kehadiran()
	{
		$crud = $this->generate_crud('gkp_routine_detail');

		$crud->columns("gkp_schedule_id", "gsm_kehadiran");

		$crud->fields("gsm_kehadiran");

		$crud->set_relation('gkp_schedule_id', 'gkp_schedule', 'date');

		$crud->set_relation_n_n('gsm_kehadiran', 'gkp_gsm_kehadiran', 'gkp_gsm', 'gkp_routine_detail_id', 'gkp_gsm_id', 'full_name');

		
		$this->mPageTitle = 'Kehadiran Tiap Minggu (GSM)';
		$this->render_crud();
	}

	public function schedule()
	{
		$crud = $this->generate_crud('gkp_gsm_schedule');

		$crud->set_relation('gkp_schedule_id', 'gkp_schedule', 'date');

		$crud->set_relation('mc_id', 'gkp_gsm', 'full_name');
		$crud->set_relation('ft1_id', 'gkp_gsm', 'full_name');
		$crud->set_relation('ft2_id', 'gkp_gsm', 'full_name');
		$crud->set_relation('ft3_id', 'gkp_gsm', 'full_name');
		$crud->set_relation('musik1_id', 'gkp_gsm', 'full_name');
		$crud->set_relation('musik2_id', 'gkp_gsm', 'full_name');
		$crud->set_relation('ontime_id', 'gkp_gsm', 'full_name');

		$this->mPageTitle = 'Jadwal Pelayanan GSM';
		$this->render_crud();
	}

	public function groups()
	{
		$crud = $this->generate_crud('gkp_groups');
		
		$this->mPageTitle = 'GSM Task Group';
		$this->render_crud();
	}

}
