<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends Api_Controller {

	public function index()
	{
		// API Doc page only accessible during development/testing environments
		if (in_array(ENVIRONMENT, array('development', 'testing')))
		{
			$this->mBodyClass = 'swagger-section';
			$this->render('home', 'empty');
		}
		else
		{
			redirect();
		}
	}

	public function register_agent_post() {
		$serial_key = $this->post('serial_key');
		$mac_address = $this->post('mac_address');
		$host_name = $this->post('host_name');

		$agent_master = $this->db->where('agent_serialkey', $serial_key)
								 ->get('agent_master')
								 ->row_array();
	    if (count($agent_master) == 0) {
	    	$data = array(
	    		"agent_servername"	=> "",
	    		"agent_serialkey" 	=> $serial_key,
	    		"agent_type"		=> "",
	    		"agent_status"		=> "0",
	    		"listener_status"	=> 0,
	    		"created_at"		=> date("Y-m-d H:i:s"),
	    		"updated_at"		=> date("Y-m-d H:i:s"),
	    		"updated_by"		=> "",
	    	);
	    	$this->db->insert("agent_master", $data);
	    	$agent_master_id = $this->db->insert_id();
	    	$data['id'] = $agent_master_id;
	    	$agent_master = $data;
	    } else {
	    	$agent_master_id = $agent_master["id"];
	    }

	    // insert to agent_client table
	    $agent_code = random_string('alnum',5);
	    $agent_client = $this->db->where('agent_host_name', $host_name)
								 ->get('agent_client')
								 ->row_array();
	    if (count($agent_client) == 0) {
	    	$data = array(
		    	'agent_master_id' 	=> $agent_master_id,
		    	'agent_code'		=> $agent_code,
		    	'agent_mac_address' => $mac_address,
		    	'agent_host_name'	=> $host_name,
		    	'agent_status'		=> 0,
		    	"created_at"		=> date("Y-m-d H:i:s"),
		    	"updated_at"		=> date("Y-m-d H:i:s"),
		    );
	    	$this->db->insert("agent_client", $data);
	    	$agent_client_id = $this->db->insert_id();
	    	$data['id'] = $agent_client_id;
	    	$agent_client = $data;
	    } 

	    $agent_client["main_master_id"] = $agent_master["id"];
	    $agent_client["main_client_id"] = $agent_client["id"];
	    $agent_client["server_name"] = $agent_master["agent_servername"];
	    $agent_client["agent_serialkey"] = $agent_master["agent_serialkey"];
	    $agent_client["agent_type"] = $agent_master["agent_type"];
	    
		$this->response($agent_client);
	}

	public function sync_agent_post() {
		$agent_data = $this->post();
		$agent_master = $this->db->where('id', $agent_data["main_master_id"])
								 ->get('agent_master')
								 ->row_array();
	    if (count($agent_master) > 0) {
	    	$agent_master_id = $agent_master["id"];

	    	$agent_client = $this->db->where('id', $agent_data["main_client_id"])
								 ->get('agent_client')
								 ->row_array();

			$agent_client_id = $agent_client["id"];
	    } 

	    $agent_client["server_name"] = $agent_master["agent_servername"];
	    $agent_client["agent_serialkey"] = $agent_master["agent_serialkey"];
	    $agent_client["agent_type"] = $agent_master["agent_type"];

	    if ($agent_master["listener_status"] == 1) {
	    	$agent_client["agent_status"] = 1;
	    } else {
	    	$agent_client["agent_status"] = 0;
	    }
	    
		$this->response($agent_client);
	}

	public function process_post()
	{
		$data = $this->post();
		$data["keterangan"] = "ini post";
		$this->response($data);
	}

	public function process_get()
	{
		$data = $this->get();
		$data["keterangan"] = "ini get";
		$this->response($data);
	}
}
