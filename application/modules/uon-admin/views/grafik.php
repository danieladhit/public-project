<?php if ( !empty($crud_note) ) echo "<p>$crud_note</p>"; ?>


<div class="row">
	<div class="col-md-12">
		<!-- LINE CHART -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Grafik Ujian</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="chart">
					<canvas id="lineChart" style="height:250px"></canvas>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

<br>
<br>

<?php if ( !empty($crud_output) ) echo $crud_output; ?>

<script src="https://adminlte.io/themes/AdminLTE/bower_components/chart.js/Chart.js"></script>

<script>

	var areaChartData = {
		labels  : <?php echo json_encode($labels); ?>,
		datasets: [
		{
			label               : 'Benar',
			fillColor           : 'rgba(210, 214, 222, 1)',
			strokeColor         : 'rgba(210, 214, 222, 1)',
			pointColor          : 'rgba(210, 214, 222, 1)',
			pointStrokeColor    : '#c1c7d1',
			pointHighlightFill  : '#fff',
			pointHighlightStroke: 'rgba(220,220,220,1)',
			data                :  <?php echo json_encode($dataset_sedang); ?>
		},
		{
			label               : 'Salah',
			fillColor           : 'rgba(60,141,188,0.9)',
			strokeColor         : 'rgba(60,141,188,0.8)',
			pointColor          : '#3b8bba',
			pointStrokeColor    : 'rgba(60,141,188,1)',
			pointHighlightFill  : '#fff',
			pointHighlightStroke: 'rgba(60,141,188,1)',
			data                : <?php echo json_encode($dataset_mudah); ?>
		},
		/*{
			label               : 'Bobot Sulit',
			fillColor           : '#f39c12',
			strokeColor         : '#f39c12',
			pointColor          : '##f39c12',
			pointStrokeColor    : '#f39c12',
			pointHighlightFill  : '#fff',
			pointHighlightStroke: '#f39c12',
			data                : <?php //echo json_encode($dataset_sulit); ?>
		}*/
		]
	}

	var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : false,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
  }

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
    var lineChart                = new Chart(lineChartCanvas)
    var lineChartOptions         = areaChartOptions
    lineChartOptions.datasetFill = false
    lineChart.Line(areaChartData, lineChartOptions)

    
</script>