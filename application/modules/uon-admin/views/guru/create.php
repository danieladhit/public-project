<?php echo $form->messages(); ?>

<div class="row">

	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">User Info</h3>
			</div>
			<div class="box-body">
				<?php echo $form->open(); ?>

					<?php echo $form->bs3_text('Nama', 'nama'); ?>
					<?php echo $form->bs3_text('NIS', 'nis'); ?>
					<?php echo $form->bs3_text('Jurusan', 'jurusan'); ?>
					<?php echo $form->bs3_text('Kelas', 'kelas'); ?>
					<?php echo $form->bs3_text('Alamat', 'alamat'); ?>
					<?php echo $form->bs3_text('No HP', 'no_hp'); ?>

					<?php echo $form->bs3_text('Username', 'username'); ?>

					<?php echo $form->bs3_password('Password', 'password'); ?>
					<?php echo $form->bs3_password('Retype Password', 'retype_password'); ?>

					<?php echo $form->bs3_submit(); ?>
					
				<?php echo $form->close(); ?>
			</div>
		</div>
	</div>
	
</div>