<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	public function index() {
		redirect(base_url()."uon-admin/guru/master/");
	}

	// Frontend User CRUD
	public function master()
	{
		$crud = $this->generate_crud('data_guru');
		$crud->columns("nama","username","id_matpel","alamat","no_hp");
		$crud->set_relation("id_matpel", "data_mata_pelajaran", "nama");
		$crud->display_as("id_matpel", "Mata Pelajaran");

		// only webmaster and admin can reset user password
		if ($this->ion_auth->in_group(array('webmaster', 'admin')))
		{
			$crud->add_action('Reset Password', '', 'uon-admin/guru/reset_password', 'fa fa-repeat');
		}

		// disable direct create / delete Frontend User
		//$crud->unset_add();
		//$crud->unset_delete();

		$this->mPageTitle = 'Guru';
		$this->render_crud();
	}

	// Create Frontend User
	public function create()
	{
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$identity = $username;
			$data = array(
				'username'		=> $username,
				'password'		=> $this->ion_auth_model->hash_password($password),
				'nama'		=> $this->input->post('nama'),
				'nis'	=> $this->input->post('nis'),
				'jurusan'	=> $this->input->post('jurusan'),
				'kelas'		=> $this->input->post('kelas'),
				'alamat'	=> $this->input->post('alamat'),
				'no_hp'		=> $this->input->post('no_hp')
			);
			
			// proceed to create user
			$this->db->insert('data_siswa', $data);
			$user_id = $this->db->insert_id();
			if ($user_id)
			{
				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);

				// directly activate user
				//$this->ion_auth->activate($user_id);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		$this->mPageTitle = 'Create Siswa';

		$this->mViewData['form'] = $form;
		$this->render('siswa/create');
	}

	
	// Frontend User Reset Password
	public function reset_password($user_id)
	{
		// only top-level users can reset user passwords
		//$this->verify_auth(array('webmaster', 'admin'));

		$form = $this->form_builder->create_form();
		if ($form->validate())
		{
			// pass validation
			$data = array('password' => $this->input->post('new_password'));
			
			// proceed to change user password
			$this->ion_auth_model->tables['users'] = 'data_guru';
			if ($this->ion_auth->update($user_id, $data))
			{
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
			}
			else
			{
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		$this->load->model('Guru_model', 'guru');
		$target = $this->guru->get($user_id);
		$this->mViewData['target'] = $target;

		$this->mViewData['form'] = $form;
		$this->mPageTitle = 'Reset Password Guru';
		$this->render('guru/reset_password');
	}

	public function employees_schedule()
	{
		$crud = $this->generate_crud('employees');
		$crud->columns('username', 'email', 'schedule');
		$crud->fields('username', 'email', 'schedule');
		$crud->unset_add();

		$crud->set_relation_n_n('schedule', 'employees_schedule', 'schedule', 'employee_id', 'schedule_id', 'name');

		$this->mPageTitle = 'Employees Schedule';
		$this->render_crud();
	}

	// Admin Users CRUD
	public function schedule()
	{
		$crud = $this->generate_crud('schedule');
		$crud->columns('places', 'name', 'day', 'note');

		$crud->set_relation_n_n('places', 'schedule_groups', 'places', 'schedule_id', 'place_id', 'name');

		$this->mPageTitle = 'Schedule';
		$this->render_crud();
	}

	public function places() {
		$crud = $this->generate_crud('places');
		$crud->unset_texteditor("address","embed","embed2");
		$this->mPageTitle = 'Manage Places';
		$this->render_crud();
	}

	public function attendance()
	{
		$crud = $this->generate_crud('attendance');
		$crud->columns('username', 'place', 'day', 'checkin_caption', 'checkin_date','checkout_caption','checkout_date','checkin_image','checkout_image', 'rating');
		$crud->fields('place', 'day', 'checkin_caption', 'checkin_date','checkout_caption','checkout_date','checkin_image','checkout_image', 'rating');
		$crud->display_as('checkin_image', 'Before')->display_as('checkout_image','After');
		$crud->unset_add();
		//$crud->unset_edit();
		//$crud->unset_delete();
		$crud->unset_read();
		//$crud->unset_print();
		$crud->unset_export();
		//$crud->unset_operations();

		$crud->callback_column("username", array($this, "_cb_col_username"));
		$crud->callback_column("place", array($this, "_cb_col_place"));
		$crud->callback_column("day", array($this, "_cb_col_day"));
		$crud->callback_field("username", array($this, "_cb_field_username"));
		$crud->callback_field("place", array($this, "_cb_field_place"));
		$crud->callback_field("day", array($this, "_cb_field_day"));
		$crud->callback_column("checkin_date", array($this, "_cb_col_checkin_date"));
		$crud->callback_column("checkout_date", array($this, "_cb_col_checkout_date"));
		$crud->callback_column("checkin_image", array($this, "_cb_col_checkin_image"));
		$crud->callback_column("checkout_image", array($this, "_cb_col_checkout_image"));
		$crud->callback_field("checkin_image", array($this, "_cb_col_checkin_image"));
		$crud->callback_field("checkout_image", array($this, "_cb_col_checkout_image"));

		$this->mPageTitle = 'Attendance';
		$this->render_crud();
	}
 	
	public function _cb_col_username($value, $row){
		$employees_schedule = $this->db->where("employees_schedule.id", $row->employees_schedule_id)
									   ->get("employees_schedule")->row();
		$employee = $this->db->where("employees.id", $employees_schedule->employee_id)
									   ->get("employees")->row();
		return $employee->username;
	}

	public function _cb_col_place($value, $row){
		$schedule_groups = $this->db->where("schedule_groups.id", $row->schedule_groups_id)
									   ->get("schedule_groups")->row();

		$place = $this->db->where("places.id", $schedule_groups->place_id)
									   ->get("places")->row();
		return $place->name;
	}

	public function _cb_col_day($value, $row){
		$schedule_groups = $this->db->where("schedule_groups.id", $row->schedule_groups_id)
									   ->get("schedule_groups")->row();

		$schedule = $this->db->where("schedule.id", $schedule_groups->schedule_id)
									   ->get("schedule")->row();
		return $schedule->name;
	}

	public function _cb_field_place($value, $primary_key){
		$attendance = $this->db->where("id", $primary_key)->get("attendance")->row();
		$schedule_groups = $this->db->where("schedule_groups.id", $attendance->schedule_groups_id)
									   ->get("schedule_groups")->row();

		$place = $this->db->where("places.id", $schedule_groups->place_id)
									   ->get("places")->row();
		//return $place->name;
		return "<input type='text' value='$place->name' readonly='true'>";
	}

	public function _cb_field_day($value, $primary_key){
		$attendance = $this->db->where("id", $primary_key)->get("attendance")->row();
		$schedule_groups = $this->db->where("schedule_groups.id", $attendance->schedule_groups_id)
									   ->get("schedule_groups")->row();

		$schedule = $this->db->where("schedule.id", $schedule_groups->schedule_id)
									   ->get("schedule")->row();
		//return $schedule->name;
	    return "<input type='text' value='$schedule->name' readonly='true'>";
	}

	public function _cb_col_checkin_date($value, $row) {
		return $row->checkin_date. " " . $row->checkin_time;
	}

	public function _cb_col_checkout_date($value, $row) {
		return $row->checkout_date. " " . $row->checkout_time;
	}

	public function _cb_col_checkin_image($value, $row) {
		return "<a href='data:image/jpeg;base64, $value' target='_blank'><img class='img-responsive' src='data:image/jpeg;base64, $value'></a>";
	}

	public function _cb_col_checkout_image($value, $row) {
		return "<a href='data:image/jpeg;base64, $value' target='_blank'><img class='img-responsive' src='data:image/jpeg;base64, $value'></a>";
	}
}
