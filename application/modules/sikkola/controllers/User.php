<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin User management, includes: 
 * 	- Admin Users CRUD
 * 	- Admin User Groups CRUD
 * 	- Admin User Reset Password
 * 	- Account Settings (for login user)
 */
class User extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	public function localadmin()
	{	
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			$this->localadmin_process();
			refresh();
		}

		$localadmin = $this->db->select('tbl_admin.id as admin_id, tbl_admin.user_id, tbl_admin.nama, tbl_sekolah.*') 
		->join('tbl_sekolah', 'tbl_sekolah.id_sekolah = tbl_admin.id_sekolah')
		->get('tbl_admin')->result();

		$sekolah = $this->db->get('tbl_sekolah')->result();

		$this->mViewData['form'] = $form;
		$this->mViewData['localadmin'] = $localadmin;
		$this->mViewData['sekolah'] = $sekolah;
		$this->mPageTitle = 'Local Admin Manage Users Page';
		$this->render('localadmin');
	}

	public function localadmin_edit()
	{
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			$this->localadmin_process();
			redirect_referrer();
		}

	}

	public function localadmin_process()
	{
		// passed validation from before function
		$process = $this->input->post('process');
		$username = $this->input->post('user_id');
		$password = $this->input->post('password');
		$profile_data = array(
			'user_id' => $username,
			'nama'	=> $this->input->post('nama'),
			'id_sekolah'		=> $this->input->post('id_sekolah'),
		);

		if ($process == 'add')
		{
			$this->db->insert('tbl_admin', $profile_data);
			$user_login_id = $this->db->insert_id();
		}
		else if ($process == 'edit')
		{
			$admin_id = $this->input->post('admin_id');
			$this->db->update('tbl_admin', $profile_data, ['tbl_admin.id' => $admin_id]);
			$user_login_id = $admin_id;
		}
		
		$password = $password == '' ? '123456' : $password;
		$admin_data = array(
			'user_login_id'	=> $user_login_id,
			'ip_address' => $this->input->ip_address(),
			'username' => $username,
			'password' => hash_password($password),
			'first_name' => $this->input->post('nama'),
			'active'	=> 1,
		);

		$admin_users = $this->db->get_where('admin_users', ['user_login_id' => $user_login_id])->row();

		if ($process == 'add' || count($admin_users) == 0)
		{
			$this->db->insert('admin_users', $admin_data);
			$user_id = $this->db->insert_id();

			$users_groups_data = array(
				'user_id' => $user_id,
				'group_id' => 2 // localadmin
			);

			$this->db->insert('admin_users_groups', $users_groups_data);
			$process_message = "Berhasil menambah data !!";
		}
		else if ($process == 'edit')
		{
			$admin_data = elements(array('ip_address', 'username', 'first_name'), $admin_data);
			$this->db->update('admin_users', $admin_data, ['user_login_id' => $user_login_id]);
			$process_message = "Berhasil Edit data !!";
		}

		$this->system_message->set_success($process_message);
	}

	public function change_password_process()
	{
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			$process = $this->input->post('process');
			$username = $this->input->post('user_id');
			$admin_id = $this->input->post('admin_id');
			$group_id = $this->input->post('group_id');
			$password = $this->input->post('password');

			$user_login_id = $admin_id;
			$admin_data = array(
				'user_login_id'	=> $user_login_id,
				'ip_address' => $this->input->ip_address(),
				'username' => $username,
				'password' => hash_password($password),
				'active'	=> 1,
			);

			$admin_users = $this->db->get_where('admin_users', ['user_login_id' => $user_login_id])->row();

			if (count($admin_users) == 0)
			{
				$this->db->insert('admin_users', $admin_data);
				$user_id = $this->db->insert_id();

				$users_groups_data = array(
					'user_id' => $user_id,
					'group_id' => $group_id // 2 for localadmin
				);

				$this->db->insert('admin_users_groups', $users_groups_data);
				$process_message = "Berhasil Ubah Password !!";
			}
			else 
			{
				$admin_data = elements(array('password'), $admin_data);
				$this->db->update('admin_users', $admin_data, ['user_login_id' => $user_login_id]);
				$process_message = "Berhasil Ubah Password !!";
			}

			$this->system_message->set_success($process_message);

			redirect_referrer();
		}

	}

	public function guru()
	{	
		$guru = $this->db->select('tbl_guru.*, tbl_sekolah.*') 
		->join('tbl_sekolah', 'tbl_sekolah.id_sekolah = tbl_guru.id_sekolah')
		->get('tbl_guru')->result();

		$this->mViewData['guru'] = $guru;
		$this->mPageTitle = 'Guru Manage Users Page';
		$this->render('guru');
	}

	// Admin Users CRUD
	public function admin_user($userdata = array())
	{
		$page_title = '';
		if(!empty($userdata)) 
		{
			$page_title = $userdata['page_title'];
		}

		$crud = $this->generate_crud('admin_users');
		$crud->columns('groups', 'username', 'first_name', 'last_name', 'active');
		$this->unset_crud_fields('ip_address', 'last_login');

		// cannot change Admin User groups once created
		if ($crud->getState()=='list')
		{
			if (!empty($userdata))
			{
				$where_admin_groups = $userdata['where_admin_groups'];
				$crud->set_relation_n_n('groups', 'admin_users_groups', 'admin_groups', 'user_id', 'group_id', 'name', null, $where_admin_groups);
			}
			else
			{
				$crud->set_relation_n_n('groups', 'admin_users_groups', 'admin_groups', 'user_id', 'group_id', 'name');
			}
		}

		// only webmaster can reset Admin User password
		if ( $this->ion_auth->in_group(array('webmaster', 'admin')) )
		{
			$crud->add_action('Reset Password', '', $this->mModule.'/panel/admin_user_reset_password', 'fa fa-repeat');
		}
		
		// disable direct create / delete Admin User
		$crud->unset_add();
		$crud->unset_delete();

		$this->mPageTitle = $page_title;
		$this->render_crud();
	}

	// Create Admin User
	public function admin_user_create()
	{
		// (optional) only top-level admin user groups can create Admin User
		//$this->verify_auth(array('webmaster'));

		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$additional_data = array(
				'first_name'	=> $this->input->post('first_name'),
				'last_name'		=> $this->input->post('last_name'),
			);
			$groups = $this->input->post('groups');

			// create user (default group as "members")
			$user = $this->ion_auth->register($username, $password, $email, $additional_data, $groups);
			if ($user)
			{
				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		$groups = $this->ion_auth->groups()->result();
		unset($groups[0]);	// disable creation of "webmaster" account
		$this->mViewData['groups'] = $groups;
		$this->mPageTitle = 'Create Admin User';

		$this->mViewData['form'] = $form;
		$this->render('panel/admin_user_create');
	}

	// Admin User Groups CRUD
	public function admin_user_group()
	{
		$crud = $this->generate_crud('admin_groups');
		$this->mPageTitle = 'Admin User Groups';
		$this->render_crud();
	}

	// Admin User Reset password
	public function admin_user_reset_password($user_id)
	{
		// only top-level users can reset Admin User passwords
		$this->verify_auth(array('webmaster'));

		$form = $this->form_builder->create_form();
		if ($form->validate())
		{
			// pass validation
			$data = array('password' => $this->input->post('new_password'));
			if ($this->ion_auth->update($user_id, $data))
			{
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
			}
			else
			{
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		$this->load->model('admin_user_model', 'admin_users');
		$target = $this->admin_users->get($user_id);
		$this->mViewData['target'] = $target;

		$this->mViewData['form'] = $form;
		$this->mPageTitle = 'Reset Admin User Password';
		$this->render('panel/admin_user_reset_password');
	}

	// Account Settings
	public function account()
	{
		// Update Info form
		$form1 = $this->form_builder->create_form($this->mModule.'/panel/account_update_info');
		$form1->set_rule_group('panel/account_update_info');
		$this->mViewData['form1'] = $form1;

		// Change Password form
		$form2 = $this->form_builder->create_form($this->mModule.'/panel/account_change_password');
		$form1->set_rule_group('panel/account_change_password');
		$this->mViewData['form2'] = $form2;

		$this->mPageTitle = "Account Settings";
		$this->render('panel/account');
	}

	// Submission of Update Info form
	public function account_update_info()
	{
		$data = $this->input->post();
		if ($this->ion_auth->update($this->mUser->id, $data))
		{
			$messages = $this->ion_auth->messages();
			$this->system_message->set_success($messages);
		}
		else
		{
			$errors = $this->ion_auth->errors();
			$this->system_message->set_error($errors);
		}

		redirect($this->mModule.'/panel/account');
	}

	// Submission of Change Password form
	public function account_change_password()
	{
		$data = array('password' => $this->input->post('new_password'));
		if ($this->ion_auth->update($this->mUser->id, $data))
		{
			$messages = $this->ion_auth->messages();
			$this->system_message->set_success($messages);
		}
		else
		{
			$errors = $this->ion_auth->errors();
			$this->system_message->set_error($errors);
		}

		redirect($this->mModule.'/panel/account');
	}
	
	/**
	 * Logout user
	 */
	public function logout()
	{
		$this->ion_auth->logout();
		redirect($this->mConfig['login_url']);
	}
}
