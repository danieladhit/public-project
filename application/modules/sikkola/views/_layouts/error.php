
<?php $this->load->view('_partials/navbar'); ?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<!-- INCLUDE SIDEBAR -->
<?php $this->load->view('_partials/sidemenu'); ?>

<!-- INCLUDE BODY HERE -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	
	<div class="m-content">
		<!--Begin::Section-->
		<!-- <div class="row">
			
		</div> -->
		<?php $this->load->view($inner_view); ?>
	</div>
</div>

</div>
<!-- End::Body -->


