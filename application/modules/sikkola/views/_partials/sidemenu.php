<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
	<i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
	<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" 
	class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
	m-menu-vertical="1"
	m-menu-scrollable="1" m-menu-dropdown-timeout="500"  
	style="position: relative;">

	<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

		<?php foreach ($menu as $parent => $parent_params): ?>

			<?php if ( empty($page_auth[$parent_params['url']]) || $this->ion_auth->in_group($page_auth[$parent_params['url']]) ): ?>

				<?php if ( empty($parent_params['children']) ): ?>
					<?php $active = ($current_uri==$parent_params['url'] || $ctrler==$parent); ?>
					<li class="m-menu__item <?php if ($active) echo 'm-menu__item--active'; ?>" aria-haspopup="true" >
						<a href='<?php echo $parent_params['url']; ?>' class="m-menu__link ">
							<i class="m-menu__link-icon <?php echo $parent_params['icon']; ?>"></i>
							<span class="m-menu__link-title">
								<span class="m-menu__link-wrap">
									<span class="m-menu__link-text">
										<?php echo $parent_params['name']; ?>
									</span>
							<!-- <span class="m-menu__link-badge">
								<span class="m-badge m-badge--danger">
									2
								</span>
							</span> -->
						</span>
					</span>
				</a>
			</li>

		<?php else: ?>

			<?php $parent_active = ($ctrler==$parent); ?>
			
			<!-- Sample Dropdown -->

			<li class="m-menu__item  m-menu__item--submenu <?php if ($parent_active) echo 'm-menu__item--active m-menu__item--open'; ?>" aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a  href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon <?php echo $parent_params['icon']; ?>"></i>
					<span class="m-menu__link-text">
						<?php echo $parent_params['name']; ?>
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu ">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
							<span class="m-menu__link">
								<span class="m-menu__link-text">
									<?php echo $parent_params['name']; ?>
								</span>
							</span>
						</li>
						<?php foreach ($parent_params['children'] as $name => $url): ?>
							<?php if ( empty($page_auth[$url]) || $this->ion_auth->in_group($page_auth[$url]) ): ?>
								<?php $child_active = ($current_uri==$url); ?>
								<li class="m-menu__item <?php if ($child_active) echo 'm-menu__item--active'; ?>" aria-haspopup="true" >
									<a  href='<?php echo $url; ?>' class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											<?php echo $name; ?></a>
										</span>
									</a>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul>
				</div>
			</li>

		<?php endif; ?>
	<?php endif; ?>
<?php endforeach; ?>

</ul>

</div>
<!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->


