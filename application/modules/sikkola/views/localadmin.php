<?=$form->messages() ?>
<div class="m-portlet m-portlet--mobile">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data Admin Sekolah
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
						<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
							<i class="la la-ellipsis-h m--font-brand"></i>
						</a>
						<div class="m-dropdown__wrapper">
							<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
							<div class="m-dropdown__inner">
								<div class="m-dropdown__body">
									<div class="m-dropdown__content">
										<ul class="m-nav">
											<li class="m-nav__section m-nav__section--first">
												<span class="m-nav__section-text">
													Quick Actions
												</span>
											</li>
											<li class="m-nav__item">
												<a href="" class="m-nav__link">
													<i class="m-nav__link-icon flaticon-share"></i>
													<span class="m-nav__link-text">
														Create Post
													</span>
												</a>
											</li>
											<li class="m-nav__item">
												<a href="" class="m-nav__link">
													<i class="m-nav__link-icon flaticon-chat-1"></i>
													<span class="m-nav__link-text">
														Send Messages
													</span>
												</a>
											</li>
											<li class="m-nav__item">
												<a href="" class="m-nav__link">
													<i class="m-nav__link-icon flaticon-multimedia-2"></i>
													<span class="m-nav__link-text">
														Upload File
													</span>
												</a>
											</li>
											<li class="m-nav__section">
												<span class="m-nav__section-text">
													Useful Links
												</span>
											</li>
											<li class="m-nav__item">
												<a href="" class="m-nav__link">
													<i class="m-nav__link-icon flaticon-info"></i>
													<span class="m-nav__link-text">
														FAQ
													</span>
												</a>
											</li>
											<li class="m-nav__item">
												<a href="" class="m-nav__link">
													<i class="m-nav__link-icon flaticon-lifebuoy"></i>
													<span class="m-nav__link-text">
														Support
													</span>
												</a>
											</li>
											<li class="m-nav__separator m-nav__separator--fit m--hide"></li>
											<li class="m-nav__item m--hide">
												<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
													Submit
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">
		<!--begin: Search Form -->
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-8 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center">
						<div class="col-md-8">
							<div class="m-form__group m-form__group--inline">
								<div class="m-form__label">
									<label>
										Pilih Sekolah:
									</label>
								</div>
								<div class="m-form__control">
									<select class="form-control m-bootstrap-select" id="m_form_sekolah">
										<option value="">All</option>
										<?php foreach ($sekolah as $key => $row) { ?>
										<option value="<?=$row->nama_sekolah ?>">
											<?=$row->nama_sekolah ?>
										</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="d-md-none m--margin-bottom-10"></div>
						</div>
						
						<div class="col-md-4">
							<div class="m-input-icon m-input-icon--left">
								<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-search"></i>
									</span>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 order-1 order-xl-2 m--align-right">
					<a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" data-toggle="modal" data-target="#modal_add">
						<span>
							<i class="la la-user"></i>
							<span>
								Tambah Data
							</span>
						</span>
					</a>
					<div class="m-separator m-separator--dashed d-xl-none"></div>
				</div>
			</div>
		</div>
		<!--end: Search Form -->
		<!--begin: Datatable -->
		<table class="m-datatable" id="html_table" width="100%">
			<thead>
				<tr>
					<th title="Field #1">
						User ID
					</th>
					<th title="Field #2">
						Nama
					</th>
					<th title="Field #3">
						ID Sekolah
					</th>
					<th title="Field #4">
						Nama Sekolah
					</th>
					<th title="Field #5">
						Action
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($localadmin as $key => $row){ ?>
				<tr>
					<td>
						<?=$row->user_id ?>
					</td>
					<td>
						<?=$row->nama ?>
					</td>
					<td>
						<?=$row->id_sekolah ?>
					</td>
					<td>
						<?=$row->nama_sekolah ?>
					</td>
					<td>
						<button class="btn btn-success m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-tooltip" data-container="body" data-placement="top" title="Edit Data" data-toggle="modal" data-target="#modal_edit_id_<?=$row->user_id ?>">
							<i class="la la-edit"></i>
						</button>
						<button class="btn btn-accent m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-tooltip" data-container="body" data-placement="top" title="Ubah Password" data-toggle="modal" data-target="#modal_password_id_<?=$row->user_id ?>">
							<i class="la la-unlock"></i>
						</button>
					</td>
				</tr>

				<!-- Modal Edit -->
				<div class="modal fade" id="modal_edit_id_<?=$row->user_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Edit Data Admin Sekolah
								</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">
										&times;
									</span>
								</button>
							</div>
							<div class="modal-body">
								<form class="editForm" method="POST" action="user/localadmin_edit">
									<input type="hidden" name="process" value="edit">
									<input type="hidden" name="admin_id" value="<?=$row->admin_id ?>">
									<div class="form-group">
										<label for="nama" class="form-control-label">
											Username :
										</label>
										<input type="text" class="form-control" name="user_id" value="<?=$row->user_id ?>">
									</div>
									<div class="form-group">
										<label for="nama" class="form-control-label">
											Nama :
										</label>
										<input type="text" class="form-control" name="nama" value="<?=$row->nama ?>">
									</div>
									<div class="form-group">
										<label for="sekolah" class="form-control-label">
											Sekolah :
										</label>
										<select name="id_sekolah" class="form-control">
											<?php foreach ($sekolah as $key => $row_sk) { ?>
											<option value="<?=$row_sk->id_sekolah ?>" <?=$row->id_sekolah == $row_sk->id_sekolah ? 'selected' : '' ?> >
												<?=$row_sk->nama_sekolah ?>
											</option>
											<?php } ?>
										</select>
									</div>
									
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Close
								</button>
								<button type="button" class="btn btn-primary btnEditForm">
									Simpan
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--end::Modal Edit-->


				<!-- Modal Change Password -->
				<div class="modal fade" id="modal_password_id_<?=$row->user_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Ubah Password
								</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">
										&times;
									</span>
								</button>
							</div>
							<div class="modal-body">
								<form class="passwordForm" method="POST" action="user/change_password_process">
									<input type="hidden" name="process" value="gantipassword">
									<input type="hidden" name="admin_id" value="<?=$row->admin_id ?>">
									<input type="hidden" name="user_id" value="<?=$row->user_id ?>">
									<input type="hidden" name="group_id" value="2"> <!-- 2 for localadmin -->
									<div class="form-group">
										<label for="nama" class="form-control-label">
											Nama :
										</label>
										<input type="text" class="form-control" value="<?=$row->nama ?>" readonly="readonly">
									</div>
									<div class="form-group">
										<label for="sekolah" class="form-control-label">
											Sekolah :
										</label>
										<input type="text" class="form-control" value="<?=$row->nama_sekolah ?>" readonly="readonly">
									</div>
									<div class="form-group">
										<label for="nama" class="form-control-label">
											Password :
										</label>
										<input type="text" class="form-control" name="password">
									</div>
									<div class="form-group">
										<label for="nama" class="form-control-label">
											Retype Password :
										</label>
										<input type="text" class="form-control" name="retype_password">
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Close
								</button>
								<button type="button" class="btn btn-primary btnPasswordForm">
									Simpan
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--end::Modal-->

				<?php } ?>
				<!-- end: Table Row Looping -->
			</tbody>
		</table>
		<!--end: Datatable -->
	</div>
</div>
</div>
</div>
</div>
<!-- end:: Body -->


<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Tambah Data Admin Sekolah
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="addForm" method="POST" action="">
					<input type="hidden" name="process" value="add">
					<div class="form-group">
						<label for="nama" class="form-control-label">
							Username :
						</label>
						<input type="text" class="form-control" name="user_id">
					</div>
					<div class="form-group">
						<label for="nama" class="form-control-label">
							Nama :
						</label>
						<input type="text" class="form-control" name="nama">
					</div>
					<div class="form-group">
						<label for="sekolah" class="form-control-label">
							Sekolah :
						</label>
						<select name="id_sekolah" class="form-control">
							<?php foreach ($sekolah as $key => $row) { ?>
							<option value="<?=$row->id_sekolah ?>">
								<?=$row->nama_sekolah ?>
							</option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label for="nama" class="form-control-label">
							Password :
						</label>
						<input type="text" class="form-control" name="password">
					</div>
					<div class="form-group">
						<label for="nama" class="form-control-label">
							Retype Password :
						</label>
						<input type="text" class="form-control" name="retype_password">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
				</button>
				<button type="button" class="btn btn-primary btnAddForm">
					Simpan
				</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<script>
	$(".btnAddForm").click(function(){
		$(".addForm").submit();
	});
	$(".btnEditForm").click(function(){
		$(".editForm").submit();
	});
	$(".btnPasswordForm").click(function(){
		$(".passwordForm").submit();
	});
</script>