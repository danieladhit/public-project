<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CI Bootstrap 3 Configuration
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views 
| when calling MY_Controller's render() function. 
| 
| See example and detailed explanation from:
| 	/application/config/ci_bootstrap_example.php
*/

$config['ci_bootstrap'] = array(

	// Site name
	'site_name' => 'Admin Metronic Panel',

	// Default page title prefix
	'page_title_prefix' => '',

	// Default page title
	'page_title' => '',

	// Default meta data
	'meta_data'	=> array(
		'author'		=> '',
		'description'	=> '',
		'keywords'		=> ''
	),
	
	// Default scripts to embed at page head or end
	'scripts' => array(
		'head'	=> array(
			'assets/theme/metronic/default/assets/vendors/base/vendors.bundle.js',
			'assets/theme/metronic/default/assets/demo/default/base/scripts.bundle.js',
			'assets/theme/metronic/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js',
		),
		'foot'	=> array(
			'assets/theme/metronic/default/assets/app/js/dashboard.js',
			'assets/dist/admin/html-table.js'
		),
	),

	// Default stylesheets to embed at page head
	'stylesheets' => array(
		'screen' => array(
			'assets/theme/metronic/default/assets/vendors/base/vendors.bundle.css',
			'assets/theme/metronic/default/assets/demo/default/base/style.bundle.css',
			'assets/theme/metronic/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css',
		)
	),

	// Default CSS class for <body> tag
	'body_class' => 'm--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default',
	
	// Multilingual settings
	'languages' => array(
	),

	// Menu items
	'menu' => array(
		'home' => array(
			'name'		=> 'Home',
			'url'		=> '',
			'icon'		=> 'fa fa-home',
		),
		'user' => array(
			'name'		=> 'Manage Users',
			'url'		=> 'user',
			'icon'		=> 'fa fa-users',
			'children'  => array(
				'Admin Local'		=> 'user/localadmin',
				'Guru'				=> 'user/guru',
				'Ortu'				=> 'user/ortu',
				'Siswa'			    => 'user/siswa',
			)
		),
		'manage_data' => array(
			'name'		=> 'Manage Data',
			'url'		=> 'manaage_data',
			'icon'		=> 'fa fa-cog',
			'children'  => array(
				'Sekolah'			=> 'manage_data/sekolah',
				'Absen'				=> 'manage_data/absen',
				'Sekolah'			=> 'manage_data/sekolah',
				'Surat Izin'		=> 'manage_data/surat_izin',
				'Kalender'			=> 'manage_data/kalender',
				'Absen'				=> 'manage_data/mading',
			)
		),
		'logout' => array(
			'name'		=> 'Sign Out',
			'url'		=> 'panel/logout',
			'icon'		=> 'fa fa-sign-out-alt',
		)
	),

	// Login page
	'login_url' => 'sikkola/login',

	// Restricted pages
	'page_auth' => array(
		'user'		=> array('superadmin', 'localadmin', 'guru'),
		'user/localadmin'			=> array('superadmin'),
		'user/guru'				=> array('superadmin', 'localadmin'),
		'user/ortu'				=> array('superadmin', 'localadmin', 'guru'),
		'user/siswa'			=> array('superadmin', 'localadmin', 'guru'),
		'manage_data/sekolah'	=> array('superadmin', 'localadmin', 'guru'),
		'manage_data/absen'						=> array('superadmin', 'localadmin', 'guru'),
		'manage_data/sekolah'				=> array('superadmin', 'localadmin', 'guru'),
		'manage_data/surat_izin'			=> array('superadmin', 'localadmin', 'ortu', 'siswa'),
		'manage_data/kalender'			=> array('superadmin', 'localadmin','ortu', 'siswa'),
		'manage_data/mading'			=> array('superadmin', 'localadmin','ortu', 'siswa'),
	),

	// AdminLTE settings
	'adminlte' => array(
		'body_class' => array(
			'superadmin'	=> 'm--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default',
			'localadmin'		=> 'm--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default',
			'guru'	=> 'm--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default',
			'ortu'		=> 'm--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default',
			'siswa'		=> 'm--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default',
		)
	),

	// Useful links to display at bottom of sidemenu
	'useful_links' => array(
		/*array(
			'auth'		=> array('webmaster', 'admin', 'manager', 'staff'),
			'name'		=> 'Frontend Website',
			'url'		=> '',
			'target'	=> '_blank',
			'color'		=> 'text-aqua'
		),
		array(
			'auth'		=> array('webmaster', 'admin'),
			'name'		=> 'API Site',
			'url'		=> 'api',
			'target'	=> '_blank',
			'color'		=> 'text-orange'
		),
		array(
			'auth'		=> array('webmaster', 'admin', 'manager', 'staff'),
			'name'		=> 'Github Repo',
			'url'		=> CI_BOOTSTRAP_REPO,
			'target'	=> '_blank',
			'color'		=> 'text-green'
		),*/
	),

	// Debug tools
	'debug' => array(
		'view_data'	=> FALSE,
		'profiler'	=> FALSE
	),
);

/*
| -------------------------------------------------------------------------
| Override values from /application/config/config.php
| -------------------------------------------------------------------------
*/
$config['sess_cookie_name'] = 'ci_session_admin';