<!-- Product Catagories Area Start -->
<div class="products-catagories-area clearfix overflow-container">

	<?php $this->load->view('_partials/home_search'); ?>

	<?php if(!empty($hotel)) { ?>

		<?php if(!empty($keyword)) { ?>
			<h5 style="color: #6d6d6d;" >Ditemukan pencarian hotel dengan keyword "<?=$keyword ?>"</h5>
		<?php } ?>

		<div class="amado-pro-catagory clearfix below-fixed-container">
			<?php foreach($hotel as $key => $row) { ?>
				<!-- Single Catagory -->
				<div class="single-products-catagory clearfix" style="min-height: 200px;">
					<a href="<?=base_url('hotel/detail/index/') ?><?=$row->id.$query_booking_date ?>">
						<!-- <img src="<?//=$template_path ?>img/bg-img/1.jpg" alt=""> -->
						<img src="<?=base_url($row->foto) ?>" alt="" style="min-height: 200px">
						<!-- Hover Content -->
						<div class="hover-content" style="top: 5px; left: 5px;">
							<div class="line"></div>
							<p>From Rp. <?=number_format($row->harga) ?> <small>/ room / night</small></p>
							<h4><?=$row->nama_hotel ?> - <small><?=$row->nama_kota ?></small></h4>

							<hr style="margin-bottom: 5px;margin-top: 5px">
							<p>Fasilitas Kamar</p>
							<div class="row">
								<?php if($row->ex_ac) { ?>
									<div class="col-md-6">
										<p style="font-size: 12px; color: #000" class="avaibility"><i style="color: #20d34a" class="fa fa-circle"></i> AC</p>
									</div>
								<?php } ?>
								<?php if($row->ex_wifi) { ?>
									<div class="col-md-6">
										<p style="font-size: 12px; color: #000" class="avaibility"><i style="color: #20d34a" class="fa fa-circle"></i> Wifi</p>
									</div>
								<?php } ?>
								<?php if($row->ex_tv) { ?>
									<div class="col-md-6">
										<p style="font-size: 12px; color: #000" class="avaibility"><i style="color: #20d34a" class="fa fa-circle"></i> Television</p>
									</div>
								<?php } ?>
								<?php if($row->ex_coffee_maker) { ?>
									<div class="col-md-6">
										<p style="font-size: 12px; color: #000" class="avaibility"><i style="color: #20d34a" class="fa fa-circle"></i> Coffee Maker</p>
									</div>
								<?php } ?>
								<?php if($row->ex_parking) { ?>
									<div class="col-md-6">
										<p style="font-size: 12px; color: #000" class="avaibility"><i style="color: #20d34a" class="fa fa-circle"></i> Parking</p>
									</div>
								<?php } ?>
								<?php if($row->ex_swimming) { ?>
									<div class="col-md-6">
										<p style="font-size: 12px; color: #000" class="avaibility"><i style="color: #20d34a" class="fa fa-circle"></i> Swimming Pool</p>
									</div>
								<?php } ?>
								<?php if($row->ex_elevator) { ?>
									<div class="col-md-6">
										<p style="font-size: 12px; color: #000" class="avaibility"><i style="color: #20d34a" class="fa fa-circle"></i> Elevator</p>
									</div>
								<?php } ?>
								<?php if($row->ex_restaurant) { ?>
									<div class="col-md-6">
										<p style="font-size: 12px; color: #000" class="avaibility"><i style="color: #20d34a" class="fa fa-circle"></i> Restaurant</p>
									</div>
								<?php } ?>
								<?php if($row->ex_24_hour) { ?>
									<div class="col-md-6">
										<p style="font-size: 12px; color: #000" class="avaibility"><i style="color: #20d34a" class="fa fa-circle"></i> 24-Hour Front Desk</p>
									</div>
								<?php } ?>
							</div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>
	<?php } else { ?>
		<br>
		<h3 style="color: #6d6d6d; text-align: center;">Ups!! Pencarian dengan keyword "<?=$keyword ?>" tidak ditemukan</h3>
	<?php } ?>

</div>
<!-- Product Catagories Area End -->