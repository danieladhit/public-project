<!DOCTYPE html>
<html lang="en" style="height: 100%">
<head>
  <meta charset="utf-8">
  <title>Hotel Invoices</title>
  <link rel="stylesheet" href="/themes/invoice1/style.css" media="all" />
</head>
<body style="height: 100%"> 
  <header class="clearfix">
    <div id="logo">
      <img src="/image_upload/icon/hotel-icon.png">
      <h2 style="display: inline-block;margin: 0px;line-height: 30px;vertical-align: bottom;">.com</h2>
    </div>
    <h1>KODE BOOKING : <?=$booking_code ?></h1>
    <div id="company" class="clearfix">
      <div>HOTEL.COM,<br /> JAKARTA, INDONESIA</div>
        <!-- <div>0812 8232 1234</div>
          <div><a href="mailto:company@example.com">admin@hotel.com</a></div> -->
          <div>
            <a href="<?=$qr_link ?>" target="_blank">
              <img src="<?=$qr_code ?>" alt="QR Code" style="width: 80px; height: auto;">
            </a>
          </div>
        </div>
        <div id="project">
          <div><span>HOTEL</span> <?=$hotel->nama_hotel ?></div>
          <div><span>CLIENT</span> <?=$booking->nama_lengkap ?></div>
          <div><span>KTP</span> <?=$booking->no_ktp ?></div>
          <div><span>ADDRESS</span> <?=$booking->alamat ?></div>
          <div><span>EMAIL</span> <a href="mailto:<?=$booking->email ?>"><?=$booking->email ?></a></div>
          <div><span>DATE</span> <?=date("d M Y") ?></div>
          <div><span>BOOKING</span> <?=date("d M Y", strtotime($booking->checkin)) . " - " . date("d M Y", strtotime($booking->checkout)) ?></div>
        </div>
      </header>
      <main>
        <table>
          <thead>
            <tr>
              <!-- <th class="service">SERVICE</th> -->
              <th class="desc">DESCRIPTION</th>
              <th>PRICE</th>
              <th>DAYS</th>
              <th>TOTAL</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <!-- <td class="service">Design</td> -->
              <td class="desc"><?=$hotel->nama_hotel ." - ". $booking->keterangan ?></td>
              <td class="unit">Rp. <?=number_format($hotel->harga) ?></td>
              <td class="qty"><?=$booking->days ?></td>
              <td class="total">Rp. <?=number_format($hotel->harga*$booking->days) ?></td>
            </tr>
            <tr>
              <td colspan="3">SUBTOTAL</td>
              <td class="total">Rp. <?=number_format($hotel->harga*$booking->days) ?></td>
            </tr>
          <!-- <tr>
            <td colspan="3">TAX 25%</td>
            <td class="total">$1,300.00</td>
          </tr> -->
          <tr>
            <td colspan="3" class="grand total">GRAND TOTAL</td>
            <td class="grand total">Rp. <?=number_format($hotel->harga*$booking->days) ?></td>
          </tr>
        </tbody>
      </table>
      <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">Tunjukan bukti ini pada resepsionis tempat Anda memesan Hotel.</div>
      </div>
    </main>
    <footer>
      Invoice was created on a system and is valid without the signature and seal.
    </footer>
  </body>
  </html>