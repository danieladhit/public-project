<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<base href="<?php echo $base_url; ?>" />
	<!--end::Global Theme Styles -->
	<!-- Favicon  -->
	<link rel="icon" href="<?=$template_path ?>img/core-img/favicon.ico">

	<title><?php echo $page_title; ?></title>

	<?php
	foreach ($meta_data as $name => $content)
	{
		if (!empty($content))
			echo "<meta name='$name' content='$content'>".PHP_EOL;
	}

	foreach ($stylesheets as $media => $files)
	{
		foreach ($files as $file)
		{
			$url = starts_with($file, 'http') ? $file : base_url($file);
			echo "<link href='$url' rel='stylesheet' media='$media'>".PHP_EOL;	
		}
	}

	foreach ($scripts['head'] as $file)
	{
		$url = starts_with($file, 'http') ? $file : base_url($file);
		echo "<script src='$url'></script>".PHP_EOL;
	}
	?>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<style>
		.hover-content {
			background-color: rgba(255,255,255,.7);
			padding: 10px;
			max-width: 95%;
		}

		.overflow-container {
			height: 100%;
			overflow: auto;
		}
		
		
		.fixed-container {
			position: absolute;
			width: calc(100% - 305px);
			background-color: #ffffff;
			z-index: 99;
		}

		.below-fixed-container {
			top: 50px;
		}

		.mb-20 {
			margin-bottom: 0px;
		}

		@media screen and (max-width:768px) { 
			.fixed-container {
				width: 104%;
				padding-left: 10px;
				margin-top: 5px;
			}

			.btn-search-home {
				width: 100%;
				border-radius: 0px;
			}

			.below-fixed-container {
				top: 175px;
			}

			.mb-20 {
				margin-bottom: 20px;
			}
		}

	</style>
</head>
<body class="<?php echo $body_class; ?>">