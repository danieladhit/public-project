<!-- Mobile Nav (max width 767px)-->
<div class="mobile-nav">
	<!-- Navbar Brand -->
	<div class="amado-navbar-brand">
		<!-- <a href="index.html"><img src="<?=$template_path ?>img/core-img/logo.png" alt=""></a> -->
		<a href="<?=base_url('hotel') ?>"><?php echo $page_title; ?></a>
	</div>
	<!-- Navbar Toggler -->
	<div class="amado-navbar-toggler">
		<span></span><span></span><span></span>
	</div>
</div>

<!-- Header Area Start -->
<header class="header-area clearfix">
	<!-- Close Icon -->
	<div class="nav-close">
		<i class="fa fa-close" aria-hidden="true"></i>
	</div>
	<!-- Logo -->
	<div class="logo" style="margin-bottom: 50px;">
		<!-- <a href="index.html"><img src="<?//=$template_path ?>img/core-img/logo.png" alt=""></a> -->
		<a href="<?=base_url('hotel') ?>">
			<img style="height: 50px; width: auto" class="img-responsive" src="/image_upload/icon/hotel-icon.png" alt="">
			<h2 style="display: inline-block;margin: 0px;line-height: 30px;vertical-align: bottom;">.com</h2>
		</a>
	</div>
	<!-- Amado Nav -->
	<nav class="amado-nav">
		<?php 
			$menu = $this->uri->segment(2);
		 ?>
		<ul>
			<li class="<?=$menu == '' ? 'active' : '' ?>"><a href="<?=base_url('hotel') ?>">Home</a></li>
			<!-- <li class="<?=$menu == 'home' ? 'active' : '' ?>"><a href="<?=base_url('hotel/home/versi2') ?>">Home-2</a></li> -->
			<li class="<?=$menu == 'gallery' ? 'active' : '' ?>"><a href="<?=base_url('hotel/gallery') ?>">Gallery</a></li>
			<!-- <li><a href="product-details.html">Product</a></li>
			<li><a href="cart.html">Cart</a></li>
			<li><a href="checkout.html">Checkout</a></li> -->
		</ul>
	</nav>
	<!-- Button Group -->
	<!-- <div class="amado-btn-group mt-30 mb-100">
		<a href="#" class="btn amado-btn mb-15">%Discount%</a>
		<a href="#" class="btn amado-btn active">New this week</a>
	</div> -->
	<!-- Cart Menu -->
	<div class="cart-fav-search mb-100">
		<!-- <a href="cart.html" class="cart-nav"><img src="<?=$template_path ?>img/core-img/cart.png" alt=""> Cart <span>(0)</span></a>
			<a href="#" class="fav-nav"><img src="<?=$template_path ?>img/core-img/favorites.png" alt=""> Favourite</a> -->
			<a href="#" class="search-nav"><img src="<?=$template_path ?>img/core-img/search.png" alt=""> Search</a>
		</div>
		<!-- Social Button -->

		<div class="footer" style="position: absolute; width: 50%; bottom: 40px">
			<div class="social-info d-flex justify-content-between">
				<a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
				<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			</div>
			<br>
			<div class="single_widget_area">
				<!-- Copywrite Text -->
				<p class="copywrite" style="font-size: 12px; text-align: center;"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</p>
			</div>
		</div>

	</header>
        <!-- Header Area End -->