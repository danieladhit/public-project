<form method="GET" action="">
	<div class="row fixed-container" style="padding-top: 5px; padding-bottom: 5px;">
		<div class="col-md-5 mb-20" style="font-family: sans-serif !important; font-size: 13px; color: #495057 !important; ">
			<select class="form-control" id="select_kota" name="kota" style="width: 100%;">
				<option value=""></option>
				<?php 
				$kota_selected = $this->input->get("kota");
				foreach($kota as $key => $row){ 
					?>
					<option <?=$kota_selected == $row->nama ? 'selected' : '' ?> value="<?=$row->nama ?>"><?=$row->nama ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-5 mb-20" style="">
			<div class='input-group' id='m_daterangepicker_2'>
				<input name="booking_date" value="<?=$this->input->get('booking_date') ?>" type='text' class="form-control m-input" placeholder="Select Booking Date"/>
				<div class="input-group-append">
					<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<button type="submit" class="btn-search-home btn btn-lg m-btn--pill m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" style="margin-top: 2px;">Cari</button>
		</div>
	</div>
</form>