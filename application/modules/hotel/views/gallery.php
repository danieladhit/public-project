<div class="shop_sidebar_area">

	<!-- ##### Single Widget ##### -->
	<div class="widget catagory mb-50">
		<!-- Widget Title -->
		<h6 class="widget-title mb-30">Pilih Kategori</h6>

		<!--  Catagories  -->
		<div class="catagories-menu">
			<?php 
				$url_kategori = $this->uri->segment(4);
			 ?>
			<ul>
				<li class="<?=$kategori == "" ? 'active' : '' ?>"><a href="gallery">Semua Kategori</a></li>
				<?php foreach($kategori as $key => $row){ ?>
					<li class="<?=$url_kategori == "$row->nama" ? 'active' : '' ?>"><a href="gallery/index/<?=$row->nama ?>"><?=$row->nama ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<!-- ##### Single Widget ##### -->
	<div class="widget price mb-50">
		<!-- Widget Title -->
		<!-- <h6 class="widget-title mb-30">Price</h6>

		<div class="widget-desc">
			<div class="slider-range">
				<div data-min="500000" data-max="1000000" data-unit="Rp " class="slider-range-price ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" data-value-min="500000" data-value-max="1000000" data-label-result="">
					<div class="ui-slider-range ui-widget-header ui-corner-all"></div>
					<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span>
					<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span>
				</div>
				<div class="range-price" style="font-size: 12px">Rp 500.000 - Rp 1.000.000</div>
			</div>
		</div> -->
	</div>
</div>

<div class="amado_product_area section-padding-100 overflow-container" style="bottom: 20px">
	<div class="container-fluid">

		<div class="row">
			<div class="col-12">
				<div class="product-topbar d-xl-flex align-items-end justify-content-between" style="margin-bottom: 20px;">
					<!-- Total Products -->
					<div class="total-products">
						<?php if($total_records > 0){ ?>
						<p>Showing <?=($per_page+1)."-".($limit_per_page*($page+1) > $total_records ? $total_records : $limit_per_page*($page+1)). " Of " . $total_records ?></p> 
					<?php } else { ?>
						<p>Hotel di <em><?=$url_kategori ?></em> tidak ditemukan</p>
					<?php } ?>
						<!-- <div class="view d-flex">
							<a href="#"><i class="fa fa-th-large" aria-hidden="true"></i></a>
							<a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
						</div> -->
					</div>
					<!-- Sorting -->
					<!-- <div class="product-sorting d-flex">
						<div class="sort-by-date d-flex align-items-center mr-15">
							<p>Sort by</p>
							<form action="#" method="get">
								<select name="select" id="sortBydate">
									<option value="value">Date</option>
									<option value="value">Newest</option>
									<option value="value">Popular</option>
								</select>
							</form>
						</div>
						<div class="view-product d-flex align-items-center">
							<p>View</p>
							<form action="#" method="get">
								<select name="select" id="viewProduct">
									<option value="value">12</option>
									<option value="value">24</option>
									<option value="value">48</option>
									<option value="value">96</option>
								</select>
							</form>
						</div>
					</div> -->
				</div>
			</div>
		</div>

		<div class="row">
			<?php foreach ($hotel as $key => $row) { ?>
				<!-- Single Product Area -->
				<div class="col-12 col-sm-6 col-md-12 col-xl-6">
					<div class="single-product-wrapper">
						<!-- Product Image -->
						<div class="product-img" style="height: 280px; overflow: hidden; background-color: #a49999">
							<img src="<?=base_url($row->foto) ?>" alt="">
							<!-- Hover Thumb -->
							<!-- <img class="hover-img" src="img/product-img/product2.jpg" alt=""> -->
						</div>

						<!-- Product Description -->
						<div class="product-description d-flex align-items-center justify-content-between">
							<!-- Product Meta Data -->
							<div class="product-meta-data">
								<div class="line"></div>
								<p class="product-price"><?=number_format($row->harga) ?></p>
								<a href="<?=base_url('hotel/detail/index/') ?><?=$row->id ?>">
									<h6><?=$row->nama_hotel . " - " . $row->nama_kota ?></h6>
								</a>
							</div>
							<!-- Ratings & Cart -->
							<div class="ratings-cart text-right">
								<div class="ratings">
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
								</div>
								<!-- <div class="cart">
									<a href="cart.html" data-toggle="tooltip" data-placement="left" title="Booking"><img src="img/core-img/cart.png" alt=""></a>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="col-12">
				<!-- Pagination -->
				<nav aria-label="navigation">
					<!-- <ul class="pagination justify-content-end mt-50">
						<li class="page-item active"><a class="page-link" href="#">01.</a></li>
						<li class="page-item"><a class="page-link" href="#">02.</a></li>
						<li class="page-item"><a class="page-link" href="#">03.</a></li>
						<li class="page-item"><a class="page-link" href="#">04.</a></li>
					</ul> -->
					<?=$links ?>
				</nav>
			</div>
		</div>
	</div>
</div>