<!-- Search Wrapper Area Start -->
<div class="search-wrapper section-padding-100">
	<div class="search-close">
		<i class="fa fa-close" aria-hidden="true"></i>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="search-content">
					<form action="" method="get">
						<input type="search" name="search" id="search" placeholder="Type your keyword..." value="<?=$this->input->get('search') ?>">
						<button type="submit"><img src="<?=$template_path ?>img/core-img/search.png" alt=""></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Search Wrapper Area End -->

<!-- ##### Main Content Wrapper Start ##### -->
<div class="main-content-wrapper d-flex clearfix" style="height: 100%">
	<?php $this->load->view('_partials/navbar'); ?>

	<?php $this->load->view($inner_view); ?>
</div>
<!-- ##### Main Content Wrapper End ##### -->

<?php //$this->load->view('_partials/newsletter'); ?>

<?php //$this->load->view('_partials/footer'); ?>