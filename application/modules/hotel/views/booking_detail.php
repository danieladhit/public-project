
<div class="cart-table-area section-padding-100 overflow-container" style="padding-top: 20px;padding-bottom: 20px">
    <div class="container-fluid">
        <form action="<?=base_url('hotel/detail/invoice') ?>" method="post">
            <input type="hidden" name="hotel_id" value="<?=$hotel->id ?>">
            <input type="hidden" name="checkin" value="<?=$checkin ?>">
            <input type="hidden" name="checkout" value="<?=$checkout ?>">
            <input type="hidden" name="days" value="<?=$days ?>">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="checkout_details_area mt-50 clearfix">

                        <div class="cart-title">
                            <h2>Customer Form</h2>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <input type="text" class="form-control" name="nama_lengkap" value="" placeholder="Nama Lengkap" required>
                            </div>
                            <div class="col-md-12 mb-3">
                                <input type="text" class="form-control" name="no_ktp" value="" placeholder="Nomor KTP" required>
                            </div>
                            <div class="col-12 mb-3">
                                <input type="email" class="form-control" name="email" placeholder="Email" value="" required>
                            </div>
                            <div class="col-12 mb-3">
                                <input type="text" class="form-control mb-3" name="alamat" placeholder="Alamat" value="" required>
                            </div>
                            <!-- <div class="col-12 mb-3">
                                <input type="text" class="form-control" id="city" placeholder="Town" value="">
                            </div>
                            <div class="col-md-6 mb-3">
                                <input type="text" class="form-control" id="zipCode" placeholder="Zip Code" value="">
                            </div> -->
                            <div class="col-md-12 mb-3">
                                <input type="text" class="form-control" name="no_hp" placeholder="Nomor Handphone" required>
                            </div>
                            <div class="col-12 mb-3">
                                <textarea name="keterangan" class="form-control w-100" id="comment" cols="30" rows="10" placeholder="Keterangan tambahan untuk booking Anda"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="cart-title mt-50">
                        <h2>Booking Detail</h2>
                    </div>

                    <div class="cart-table clearfix">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th style="flex: 0 0 50%;width: 50%;max-width: 50%;">Booking Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="cart_product_img">
                                        <a href="#"><img src="<?=base_url($hotel->foto) ?>" alt="Product"></a>
                                    </td>
                                    <td class="cart_product_desc">
                                        <h5><?=$hotel->nama_hotel ?></h5>
                                    </td>
                                    <td class="price" style="flex: 0 0 50%;width: 50%;max-width: 50%;">
                                        <span><?=$this->input->post("booking_date") ?></span>
                                    </td>
                                <!-- <td class="qty">
                                    <div class="qty-btn d-flex">
                                        <p>Qty</p>
                                        <div class="quantity">
                                            <span class="qty-minus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                            <input type="number" class="qty-text" id="qty" step="1" min="1" max="300" name="quantity" value="1">
                                            <span class="qty-plus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </td> -->
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="cart-summary" style="position: fixed;width: 25%;">
                    <h5>Cart Total</h5>
                    <ul class="summary-table">
                        <li><span>subtotal:</span> <span>Rp. <?=number_format($hotel->harga) ?></span></li>
                        <li><span>how long:</span> <span><?=$days ?> days</span></li>
                        <li><span>total:</span> <span>Rp. <?=number_format($hotel->harga * $days) ?></span></li>
                    </ul>
                    <div class="cart-btn mt-100">
                        <button type="submit" class="btn amado-btn w-100">Booking</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div>