<!-- Product Details Area Start -->
<div class="single-product-area section-padding-100 clearfix overflow-container">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Kategori</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?=$hotel->nama_hotel ?></li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-7">
                <div class="single_product_thumb">
                    <div id="product_details_slider" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li class="active" data-target="#product_details_slider" data-slide-to="0" style="
                            background-image: url(<?=base_url($hotel->foto) ?>);
                            -webkit-box-flex: 0;
                            -ms-flex: 0 0 20%;
                            flex: 0 0 20%;
                            width: 20%;
                            min-width: 20%;
                            height: 120px;">
                        </li>
                        <?php 
                        $i = 1;
                        while ($i < 5) { 
                            if(isset($hotel->gallery[$i])) {
                                $thumbnail = base_url($hotel->gallery[$i]->foto);
                            } else {
                                $thumbnail = "http://via.placeholder.com/650x400";
                            }
                            ?>
                            <li data-target="#product_details_slider" data-slide-to="<?=$i ?>" style="
                            background-image: url(<?=$thumbnail ?>); -webkit-box-flex: 0;
                            -ms-flex: 0 0 20%;
                            flex: 0 0 20%;
                            width: 20%;
                            min-width: 20%;
                            height: 120px;">
                        </li>
                        <?php $i++; } ?>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <a class="gallery_img" href="<?=base_url($hotel->foto) ?>">
                                <img class="d-block w-100" src="<?=base_url($hotel->foto) ?>" alt="First slide">
                            </a>
                        </div>
                        <?php 
                        $i = 1;
                        while ($i < 5) { 
                            if(isset($hotel->gallery[$i])) {
                                $thumbnail = base_url($hotel->gallery[$i]->foto);
                            } else {
                                $thumbnail = "http://via.placeholder.com/650x400";
                            }
                            ?>
                            <div class="carousel-item">
                                <a class="gallery_img" href="<?=$thumbnail ?>">
                                    <img class="d-block w-100" src="<?=$thumbnail ?>" alt="<?=$i ?> slide">
                                </a>
                            </div>
                            <?php $i++; } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-5">
                <div class="single_product_desc">
                    <!-- Product Meta Data -->
                    <div class="product-meta-data">
                        <div class="line"></div>
                        <p class="product-price">Rp. <?=number_format($hotel->harga) ?></p>
                        <a href="product-details.html">
                            <h6><?=$hotel->nama_hotel ?></h6>
                        </a>
                        <!-- Ratings & Review -->
                        <div class="ratings-review mb-15 d-flex align-items-center justify-content-between">
                            <div class="ratings">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                                    <!-- <div class="review">
                                        <a href="#">Write A Review</a>
                                    </div> -->
                                </div>
                                <!-- Avaiable -->
                                <p class="avaibility"><i class="fa fa-circle"></i> Kamar Tersedia</p>
                            </div>

                            <div class="short_overview my-5">
                                <p><?=$hotel->deskripsi ?></p>
                            </div>

                            <!-- Add to Cart Form -->
                            <form action="<?=base_url('hotel/detail/booking_detail') ?>" class="cart clearfix" method="POST">
                                <input type="hidden" name="hotel_id" value="<?=$hotel->id ?>">
                                <?php if($this->input->get('booking_date')) { ?>
                                    <input type="hidden" name="booking_date" value="<?=$this->input->get('booking_date') ?>">
                                <?php } ?>
                                <div class="date-container mb-3" style="width: 75%;">
                                    <div class='input-group' id='m_daterangepicker_2'>
                                        <input name="booking_date" value="<?=$this->input->get('booking_date') ?>" type='text' class="form-control m-input" placeholder="Select Booking Date" <?=$this->input->get('booking_date') == "" ? "" : "disabled" ?> required />
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- <small><em>* 1 kamar untuk 2 orang</em></small>
                                <div class="cart-btn d-flex mb-50">
                                    <p>Jumlah Kamar</p>
                                    <div class="quantity">
                                        <span class="qty-minus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                        <input type="number" class="qty-text" id="qty" step="1" min="1" max="300" name="quantity" value="1">
                                        <span class="qty-plus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-caret-up" aria-hidden="true"></i></span>
                                    </div>
                                </div> -->

                                <button type="submit" name="addtocart" value="5" class="btn amado-btn">Booking</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product Details Area End -->