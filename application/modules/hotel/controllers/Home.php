<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home page
 */
class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index_backup()
	{	
		$keyword = "";

		$this->db->select('hotel_master.*, foto, kota.nama as nama_kota')
		->join('hotel_gallery', 'hotel_master.id = hotel_gallery.id_hotel_master', 'left')
		->join('orders', 'hotel_master.id = orders.id_hotel_master', 'left')
		->where('hotel_gallery.urutan', 1)
		->order_by('hotel_master.id', 'DESC');

		if ($this->input->get("search")) {
			$keyword = $this->input->get("search");
			$this->db->group_start();
			$this->db->like('nama_hotel', $keyword);
			$this->db->or_like('alamat', $keyword);
			$this->db->or_like('harga', $keyword);
			$this->db->group_end();
		} 

		$query_booking_date = "?booking_date=";
		if ($this->input->get('booking_date')) {
			$query_booking_date = "?booking_date=".$this->input->get('booking_date');
			$start_n_end = explode(' / ', $this->input->get('booking_date'));
			$checkin = $start_n_end[0];
			$checkout = $start_n_end[1];
			//exit($checkout);
			$this->db->group_start();
			$this->db->where('checkin NOT BETWEEN "'. date('Y-m-d', strtotime($checkin)). '" and "'. date('Y-m-d', strtotime($checkout)).'"');
			$this->db->group_end();
		}

		$this->db->group_by('hotel_master.id');
		$this->db->limit(8);

		$hotel = $this->db->get('hotel_master')->result();

		$kota = $this->db->get('kota')->result();
		//echo $ctrler; exit($ctrler);
		//echo "<pre>", print_r($hotel); exit();
		$this->mViewData["kota"] = $kota;
		$this->mViewData["hotel"] = $hotel;
		$this->mViewData["keyword"] = $keyword;
		$this->mViewData["query_booking_date"] = $query_booking_date;
		$this->render('home');
	}

	public function home_1()
	{
		$query = "SELECT hotel_master.*, foto, checkin, checkout, kota.nama as nama_kota FROM hotel_master 
		LEFT JOIN hotel_gallery ON hotel_master.id = hotel_gallery.id_hotel_master
		LEFT JOIN orders ON hotel_master.id = orders.id_hotel_master
		LEFT JOIN kota ON id_kota = kota.id
		WHERE hotel_gallery.urutan = 1 ";

		$keyword = "";
		if ($this->input->get("search")) {
			$keyword = $this->input->get("search");
			$query .= "AND (nama_hotel LIKE '$keyword' OR alamat LIKE '$keyword' OR harga LIKE '$keyword') ";
		} 

		// kota filtered
		if ($this->input->get("kota")) {
			$nama_kota = $this->input->get("kota");
			$kota = $this->db->get_where('kota', ['nama' => $nama_kota])->row();

			if (!empty($kota)) {
				$id_kota = $kota->id;
				$query .= "AND id_kota = $id_kota ";
			}
		}

		$today = date("Y-m-d");
		$checkin = $today;
		$checkout = date("Y-m-d", strtotime($today) + "1 day");

		$query_booking_date = "?booking_date=";
		if ($this->input->get('booking_date')) {
			$params   = $_SERVER['QUERY_STRING'];
			$query_booking_date = "?".$params;
			$start_n_end = explode(' / ', $this->input->get('booking_date'));
			$checkin = $start_n_end[0];
			$checkout = $start_n_end[1];
			//exit($checkout);
		}

		// date filtered
		$query .= "AND (checkin NOT BETWEEN '$checkin' AND '$checkout' OR checkin IS NULL ) AND (checkout NOT BETWEEN '$checkin' AND '$checkout' OR checkin IS NULL) ";

		$query .= "GROUP BY hotel_master.id ORDER BY hotel_master.id DESC LIMIT 8";

		$hotel = $this->db->query($query)->result();

		$kota = $this->db->get('kota')->result();
		//echo $ctrler; exit($ctrler);
		//echo "<pre>", print_r($hotel); exit();
		$this->mViewData["kota"] = $kota;
		$this->mViewData["hotel"] = $hotel;
		$this->mViewData["keyword"] = $keyword;
		$this->mViewData["query_booking_date"] = $query_booking_date;
		$this->render('home');
	}

	public function index()
	{	
		$query = "SELECT hotel_master.*, foto, checkin, checkout, kota.nama as nama_kota FROM hotel_master 
		LEFT JOIN hotel_gallery ON hotel_master.id = hotel_gallery.id_hotel_master
		LEFT JOIN orders ON hotel_master.id = orders.id_hotel_master
		LEFT JOIN kota ON id_kota = kota.id
		WHERE hotel_gallery.urutan = 1 ";

		$keyword = "";
		if ($this->input->get("search")) {
			$keyword = $this->input->get("search");
			$query .= "AND (nama_hotel LIKE '$keyword' OR alamat LIKE '$keyword' OR harga LIKE '$keyword') ";
		} 

		// kota filtered
		if ($this->input->get("kota")) {
			$nama_kota = $this->input->get("kota");
			$kota = $this->db->get_where('kota', ['nama' => $nama_kota])->row();

			if (!empty($kota)) {
				$id_kota = $kota->id;
				$query .= "AND id_kota = $id_kota ";
			}
		}

		$today = date("Y-m-d");
		$checkin = $today;
		$checkout = date("Y-m-d", strtotime($today) + "1 day");

		$query_booking_date = "?booking_date=";
		if ($this->input->get('booking_date')) {
			$params   = $_SERVER['QUERY_STRING'];
			$query_booking_date = "?".$params;
			$start_n_end = explode(' / ', $this->input->get('booking_date'));
			$checkin = $start_n_end[0];
			$checkout = $start_n_end[1];
			//exit($checkout);
		}

		// date filtered
		$query .= "AND (checkin NOT BETWEEN '$checkin' AND '$checkout' OR checkin IS NULL ) AND (checkout NOT BETWEEN '$checkin' AND '$checkout' OR checkin IS NULL) ";

		$query .= "GROUP BY hotel_master.id ORDER BY hotel_master.id DESC LIMIT 12";

		$hotel = $this->db->query($query)->result();

		$kota = $this->db->get('kota')->result();
		//echo $ctrler; exit($ctrler);
		//echo "<pre>", print_r($hotel); exit();
		$this->mViewData["kota"] = $kota;
		$this->mViewData["hotel"] = $hotel;
		$this->mViewData["keyword"] = $keyword;
		$this->mViewData["query_booking_date"] = $query_booking_date;
		$this->render('home2');
	}
}
