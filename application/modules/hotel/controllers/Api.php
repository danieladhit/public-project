<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller {

	public function index()
	{
		$this->db->select('vendors.nama as nama_vendor, vendors.telepon as telepon_vendor, vendors.email as email_vendor, vendors.alamat as alamat_vendor, kos_master.*, kos_gallery.foto as foto_kos,
			(SELECT GROUP_CONCAT(foto SEPARATOR ",") FROM kos_gallery WHERE id_kos_master = kos_master.id) as slider_kos')
		->join('vendors', 'vendors.id = kos_master.id_vendor', 'left')
		->join('kos_gallery', 'kos_gallery.id_kos_master = kos_master.id', 'left')
		->group_by('kos_master.id');
		$kos_master = $this->db->get('kos_master')->result();

		foreach ($kos_master as $key => $kos) {
			$kos_master[$key]->harga = number_format($kos->harga);
			$kos_master[$key]->alamat = trim($kos->alamat);
		}
		echo json_encode($kos_master);
		exit();
	}

	public function getUserLogin()
	{
		$email = $this->input->post("email");
		$plain_password = $this->input->post("password");

		$this->db->select('email, password');
		$this->db->where("email", $email);
		$userLogin = $this->db->get("customers")->result();

		$result = array();
		if(count($userLogin) > 0) {
			if (verify_pw($plain_password, $userLogin[0]->password)) {
				$userLogin[0]->password = $plain_password;
				$result = $userLogin;
			} 
			else {
				$result = $userLogin;
			}
		} 
		
		echo json_encode($result);
		exit();
	}

	public function registerProcess()
	{
		$nama = $this->input->post("nama");
		$email = $this->input->post("email");
		$plain_password = $this->input->post("password");
		$mobile_number = $this->input->post("mobile_number");
		$otp_number = $this->input->post("otp_number");
		$register_is_success = $this->input->post("register_is_success");
		$register_status = false;

		if (!$otp_number) {
			$otp_number = mt_rand(100000, 999999);
		}
		
		$data = array(
			"nama"		=>	$nama,
			"email"		=>	$email,
			"password"	=>	hash_pw($plain_password),
			"telepon"	=>	$mobile_number,
			"otp_number"=>	$otp_number
		);

		if($register_is_success == "true") {
			$updated_data["otp_number"] = null;
			$updated_data["status"] = 1;
			$this->db->update("customers", $updated_data, ["email" => $email, "otp_number" => $otp_number]);
			$register_status = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
		} else {
			$data["status"] = 0;
			$this->insertSmsHistory($mobile_number, "Carikos - " . $otp_number . " gunakan nomor ini untuk register");
			$register_status = $this->db->insert("customers", $data);
		}

		$data["register_status"] = $register_status;
		if ($register_status) {
			echo json_encode($data);
		} else {
			echo null;
		}
		exit();
	}

	public function getOTPNumber()
	{
		$nama = $this->input->post("nama");
		$email = $this->input->post("email");
		$plain_password = $this->input->post("password");
		$mobile_number = $this->input->post("mobile_number");

		$customer = $this->db->get_where("customers", ["email" => $email])->row();
		$nama_customer = $customer->nama;
		$mobile_number = $customer->telepon;
		$otp_number = $customer->otp_number;
		
		if(!empty($customer)) {
			$this->insertSmsHistory($mobile_number, "Carikos - " . $otp_number . " gunakan nomor ini untuk register");
		}

		// $data["register_status"] = $register_status;
		// if ($register_status) {
		// 	echo json_encode($data);
		// } else {
		// 	echo null;
		// }
		echo null;
		exit();
	}

	public function checkoutProcess() {
		$email = $this->input->post("email");
		$id_kos = $this->input->post("id_kos");
		$id_vendor = $this->input->post("id_vendor");
		$account_number = $this->input->post("account_number");
		$amount = $this->input->post("amount");
		$bukti_pembayaran = $this->input->post("bukti_pembayaran");
		$checkout_status = false;

		$customer = $this->db->get_where("customers", ["email" => $email])->row();
		$nama_customer = $customer->nama;
		$mobile_number = $customer->telepon;

		$data_order = array(
			"id_customer"	=> $customer->id,
			"id_kos_master"	=> $id_kos,
			"id_kos_periode" => 1,
			"waktu_order" => date("Y-m-d H:i:s"),
			"no_rekening" => $account_number,
			"jumlah_transfer" => $amount,
			"bukti_pembayaran" => $bukti_pembayaran,
			"status" => 0
		);

		if(!empty($bukti_pembayaran)) {
			$data_order["status"] = 1;
		}

		$checkout_status = $this->db->insert("orders", $data_order);
		if ($checkout_status) {
			// send message
			if(!empty($bukti_pembayaran)) {
				$message = "Hai $nama_customer, Terimakasih telah order di Carikos. Silahkan selesaikan pembayaran Anda untuk booking kos";
			} else {
				$message = "Hai $nama_customer, Terimakasih telah order di Carikos. Konfirmasi pembayaran Anda telah kami terima";
			}
			$this->insertSmsHistory($mobile_number, $message);
			echo json_encode($data_order);
		} else {
			echo null;
		}
		exit();
	}

	public function konfirmasiPembayaran() {
		$order_id = $this->input->post("order_id");
		$email = $this->input->post("email");
		$id_kos = $this->input->post("id_kos");
		$id_vendor = $this->input->post("id_vendor");
		$account_number = $this->input->post("account_number");
		$amount = $this->input->post("amount");
		$bukti_pembayaran = $this->input->post("bukti_pembayaran");
		$waktu_order = $this->input->post("waktu_order");
		$waktu_order = date("Y-m-d H:i:s", strtotime($waktu_order));
		$checkout_status = false;

		$customer = $this->db->get_where("customers", ["email" => $email])->row();
		
		$data_order = array(
			"no_rekening" => $account_number,
			"jumlah_transfer" => $amount,
			"bukti_pembayaran" => $bukti_pembayaran,
			"status" => 1
		);

		$this->db->update("orders", $data_order, ["id" => $order_id]);
		$checkout_status = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
		if ($checkout_status) {
			echo json_encode($data_order);
		} else {
			echo null;
		}
		exit();
	}

	public function getOrderHistory()
	{
		$email = $this->input->post("email");
		$customer = $this->db->get_where("customers", ["email" => $email])->row();

		$this->db->select('vendors.nama as nama_vendor, vendors.telepon as telepon_vendor, vendors.email as email_vendor, vendors.alamat as alamat_vendor, kos_master.*, kos_gallery.foto as foto_kos, orders.id as order_id, orders.waktu_order, orders.harga_sewa, orders.no_rekening, orders.jumlah_transfer, orders.bukti_pembayaran, orders.status as status_order, kos_periode.keterangan as periode')
		->join('kos_periode', 'kos_periode.id = orders.id_kos_periode', 'left')
		->join('kos_master', 'kos_master.id = orders.id_kos_master', 'left')
		->join('vendors', 'vendors.id = kos_master.id_vendor', 'left')
		->join('kos_gallery', 'kos_gallery.id_kos_master = kos_master.id', 'left')
		->where('orders.id_customer', $customer->id)
		->group_by('orders.id')
		->order_by('orders.id', 'DESC');

		$kos_master = $this->db->get('orders')->result();

		foreach ($kos_master as $key => $kos) {
			$kos_master[$key]->waktu_order = date("d F Y H:i:s", strtotime($kos->waktu_order));
			$kos_master[$key]->harga = number_format($kos->harga);

			if ($kos->status_order == 0) {
				$kos_master[$key]->status_order = "Belum Bayar";
			} else if ($kos->status_order == 1) {
				$kos_master[$key]->status_order = "Sudah Konfirmasi";
			} else if ($kos->status_order == 2) {
				$kos_master[$key]->status_order = "Sudah Lunas";
			} else if ($kos->status_order == 3) {
				$kos_master[$key]->status_order = "Konfirmasi Pembayaran Ditolak";
			} else {
				$kos_master[$key]->status_order = "Belum Bayar";
			}
		}

		echo json_encode($kos_master);
		exit();
	}

	public function getSmsHistory() {
		$sms_history = $this->db->get_where("sms_history", ["status" => 0])->result();
		echo json_encode($sms_history);
		exit();
	}

	public function insertSmsHistory($receiver="", $message="") {
		if(empty($receiver)) {
			$receiver = $this->input->post("receiver");
			$message = $this->input->post("message");
		}
		
		$sms_data = array(
			"receiver" => $receiver,
			"message"  => $message,
			"status"   => 0
		);
		$this->db->insert("sms_history", $sms_data);
		$sms_data["id"] = $this->db->insert_id();

		if(empty($receiver)) {
			echo json_encode($sms_data);
			exit();
		} else {
			return true;
		}

	}

	public function updateSmsHistory() {
		$id = $this->input->post("id");
		$sender = "089690698915";
		$sms_history = $this->db->get_where("sms_history", ["id" => $id])->row();

		$this->db->update("sms_history", ["sender" => $sender,"status" => 1], ["id" => $id]);
		echo json_encode($sms_history);
		exit();
	}

	public function tambah()
	{
		$this->mViewData = array(
			'nama_kos' => '',
			'id_vendor' => '',
			'alamat' => '',
			'harga' => '',
			'jumlah' => '',
			'deskripsi' => '',
			'ex_kasur' => '',
			'ex_lemari' => '',
			'ex_ac' => '',
			'ex_kipas' => '',
			'ex_wifi' => '',
			'ex_tv' => ''
		);

		if ($this->input->post('nama_kos')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_kasur','ex_lemari','ex_ac','ex_kipas','ex_wifi','ex_tv');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->insert('kos_master', $data);
			$id_kos = $this->db->insert_id();

			set_alert('success', 'Informasi kos berhasil disimpan');
			redirect($this->mModule.'/kos_master/manage_gallery/'.$id_kos);
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_vendors = $this->db->get('vendors');
		$vendors = $get_vendors->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['vendors'] = $vendors;
		$this->mPageTitle = 'Tambah Kos';
		$this->render('add_kos');
	}

	public function ubah($id_kos)
	{
		// Params Validation
		$get_kos = $this->db->where('id', $id_kos)
		->get('kos_master');
		$kos = $get_kos->row();

		if (empty($kos)) {
			set_alert('danger', 'Data yang anda cari tidak ditemukan');
			redirect($this->mModule.'/kos_master/');
		}

		foreach ($kos as $key => $value) {
			$this->mViewData[$key] = $value;
		}

		if ($this->input->post('nama_kos')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_kasur','ex_lemari','ex_ac','ex_kipas','ex_wifi','ex_tv');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->update('kos_master', $data, ['id' => $id_kos]);

			set_alert('success', 'Informasi kos berhasil diubah');
			redirect($this->mModule.'/kos_master/manage_gallery/'.$id_kos);
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_vendors = $this->db->get('vendors');
		$vendors = $get_vendors->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['vendors'] = $vendors;
		$this->mPageTitle = 'Tambah Kos';
		$this->render('add_kos');
	}

	public function preview($id_kos)
	{
		// ---- KOS DATA -----
		// Params Validation
		$get_kos = $this->db->where('id', $id_kos)
		->get('kos_master');
		$kos = $get_kos->row();

		if (empty($kos)) {
			set_alert('danger', 'Data yang anda cari tidak ditemukan');
			redirect($this->mModule.'/kos_master/');
		}

		foreach ($kos as $key => $value) {
			$this->mViewData[$key] = $value;
		}

		if ($this->input->post('nama_kos')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_kasur','ex_lemari','ex_ac','ex_kipas','ex_wifi','ex_tv');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->update('kos_master', $data, ['id' => $id_kos]);

			set_alert('success', 'Informasi kos berhasil diubah');
			refresh();
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_vendors = $this->db->get('vendors');
		$vendors = $get_vendors->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['vendors'] = $vendors;


		// --- KOS GALLERY ----
		$get_kos_gallery = $this->db->where('id_kos_master', $id_kos)
		->limit(5)
		->order_by('urutan', 'ASC')
		->get('kos_gallery');
		$kos_gallery = $get_kos_gallery->result();

		$images_detail = array();

		// get images detail
		foreach ($kos_gallery as $key => $row) {
			$name = str_replace('./image_upload/kos/', '', $row->foto);
			$size = filesize($row->foto);
			$url = base_url($row->foto);
			$images_detail[$row->urutan] = array('name' => $name, 'size' => $size, 'url' => $url);
		}

		// validation images detail must have 5 files
		$i=1;
		while ($i <= 5) {
			if(!isset($images_detail[$i])) {
				$images_detail[$i] = array();
			}
			$i++;
		}
		$this->mViewData['id_kos'] = $id_kos;
		$this->mViewData['images_detail'] = $images_detail;

		$this->mPageTitle = 'Preview Kos';
		$this->render('preview_kos');
	}

	public function manage_gallery($id_kos = 0) {
		$get_kos_gallery = $this->db->where('id_kos_master', $id_kos)
		->limit(5)
		->order_by('urutan', 'ASC')
		->get('kos_gallery');
		$kos_gallery = $get_kos_gallery->result();

		$images_detail = array();

		// get images detail
		foreach ($kos_gallery as $key => $row) {
			$name = str_replace('./image_upload/kos/', '', $row->foto);
			$size = filesize($row->foto);
			$url = base_url($row->foto);
			$images_detail[$row->urutan] = array('name' => $name, 'size' => $size, 'url' => $url);
		}

		// validation images detail must have 5 files
		$i=1;
		while ($i <= 5) {
			if(!isset($images_detail[$i])) {
				$images_detail[$i] = array();
			}
			$i++;
		}

		//print_r($images_detail); exit();

		$this->mPageTitle = 'Upload Foto Kos';
		$this->mViewData['id_kos'] = $id_kos;
		$this->mViewData['images_detail'] = $images_detail;
		$this->render('add_kos_gallery');
	}

	public function upload_foto($urutan=0, $id_user=0, $id_kos=0)
	{
		// Only Vendor Can Be Upload -- Validation
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		// Params Validation
		$get_kos = $this->db->where('id', $id_kos)
		->get('kos_master');
		$kos = $get_kos->row();

		if (empty($kos)) {
			$this->output->set_status_header('401', 'Kos Data is Missing');
			exit('Kos Data is Missing');
		}

		if ($id_user != $user->id) {
			$this->output->set_status_header('401', 'User Not Found');
			exit('User Not Found');
		}

		$this->load->helper('file_upload_helper');
		$upload_path = './image_upload/kos/';
		$response = image_upload('file', $upload_path);

		if ($response['status']) {
			$data['id_kos_master'] = $id_kos;
			$data['foto'] = $response['value'];
			$data['urutan'] = $urutan;

			$get_kos_gallery = $this->db->where('id_kos_master', $id_kos)
			->where('urutan', $urutan)
			->get('kos_gallery');
			$kos_gallery = $get_kos_gallery->row();

			if (empty($kos_gallery)) {
				$this->db->insert('kos_gallery', $data);
			} else {
				$this->db->update('kos_gallery', $data, ['id' => $kos_gallery->id]);
			}

			exit($response['value']);
		} else {
			$this->output->set_status_header('401', $response['value']);
			exit($response['value']);
		}
	}

	public function delete_foto()
	{
		$filename = $this->input->post('name');
		$upload_path = './image_upload/kos/';
		$full_path = $upload_path . $filename;

		// Delete DB
		$this->db->delete('kos_gallery',['foto' => $full_path]);

		// Delete File
		unlink($full_path);

		exit('Deleted Succesfully');
	}
}
