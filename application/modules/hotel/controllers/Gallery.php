<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Gallery page
 */
class Gallery extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mCtrler = 'gallery';
		// load Pagination library
        $this->load->library('pagination');
         
        // load URL helper
        $this->load->helper('url');
	}

	public function index($kategori = '')
	{	
		$kategori = $this->db->get_where('hotel_kategori', ['nama' => $kategori])->row();

		$this->db->select("hotel_master.*")
		->order_by("hotel_master.id", "DESC");

        $segment = 4;
		if (!empty($kategori)) {
			$id_kategori = $kategori->id;
			$this->db->where('id_kategori', $id_kategori);
            $segment = 5;
		}

		$total_hotel = $this->db->get("hotel_master")->result();
		$this->mViewData["total_hotel"] = $total_hotel;

		// init params
        $params = array();
        
        $limit_per_page = 8;
        $page = ($this->uri->segment($segment)) ? ($this->uri->segment($segment) - 1) : 0;
        $per_page = $limit_per_page * $page;
        $total_records = count($total_hotel);
        //echo $page; exit($page);
     
        $this->db->select("hotel_master.*, foto, hotel_kategori.nama as nama_kategori, kota.nama as nama_kota")
		->join("hotel_gallery", "hotel_master.id = id_hotel_master", "left")
		->join("hotel_kategori", "hotel_master.id_kategori = hotel_kategori.id", "left")
        ->join("kota", "hotel_master.id_kota = kota.id", "left")
		->where("hotel_gallery.urutan", 1)
		->order_by("hotel_master.id", "DESC")
		->limit($limit_per_page, $per_page);

		if (!empty($kategori)) {
			$id_kategori = $kategori->id;
			$this->db->where('id_kategori', $id_kategori);
		}

		$hotel = $this->db->get("hotel_master")->result();
		$this->mViewData["hotel"] = $hotel;

		$this->gallery_pagination($total_records, $hotel, $limit_per_page, $segment);

		$kategori = $this->db->get('hotel_kategori')->result();
		$this->mViewData["kategori"] = $kategori;

		$this->mViewData['limit_per_page'] = $limit_per_page;
		$this->mViewData['page'] = $page;
		$this->mViewData['per_page'] = $per_page;
		$this->mViewData['total_records'] = $total_records;
		$this->render('gallery');
	}

	public function gallery_pagination($total_records, $results, $limit_per_page, $segment)
	{
        if ($total_records > 0)
        {
            // get current page records
            $params["results"] = $results;
                 
            $config['base_url'] = base_url() . 'hotel/gallery/index';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = $segment;
             
            // custom paging configuration
            $config['num_links'] = $total_records;
            $config['use_page_numbers'] = TRUE;
            // $config['reuse_query_string'] = FALSE;
            $config['page_query_string'] = FALSE;
             
            $config['full_tag_open'] = '<ul class="pagination justify-content-end mt-50">';
            $config['full_tag_close'] = '</ul>';
             
            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
             
            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';
             
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';
 
            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
 
            $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
 
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';

            $config['attributes'] = array('class' => 'page-link');
             
            $this->pagination->initialize($config);
                 
            // build paging links
            $this->mViewData["links"] = $this->pagination->create_links();
        } else {
            $this->mViewData["links"] = "";
        }
	}
}
