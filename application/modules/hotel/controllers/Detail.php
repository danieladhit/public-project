<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Gallery page
 */
class Detail extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mCtrler = 'gallery';
		$this->load->library('encrypt');
	}

	public function index($hotel_id = 1)
	{	
		$this->db->select("hotel_master.*, foto")
		->join("hotel_gallery", "hotel_master.id = id_hotel_master", "left")
		->where("hotel_gallery.urutan", 1)
		->where("hotel_master.id", $hotel_id);
		$hotel = $this->db->get("hotel_master")->row();

		$gallery = $this->db->order_by("urutan", "ASC")
		->get_where("hotel_gallery", ["id_hotel_master" => $hotel->id]);
		$hotel->gallery = $gallery->result();
		//echo $ctrler; exit($ctrler);
		//echo "<pre>", print_r($hotel); exit();
		$this->mViewData["hotel"] = $hotel;
		$this->render('hotel_detail');
	}

	public function booking_detail()
	{
		$hotel_id = $this->input->post('hotel_id');
		$booking_date = $this->input->post('booking_date');

		$start_n_end = explode(' / ', $booking_date);
		$checkin = $start_n_end[0];
		$checkout = $start_n_end[1];

		// cek orders
		$booking_query = "SELECT * FROM orders WHERE (checkin NOT BETWEEN '$checkin' AND '$checkout' OR checkin IS NULL ) AND (checkout NOT BETWEEN '$checkin' AND '$checkout' OR checkin IS NULL)";

		$room_available = $this->db->query($booking_query);

		if ($room_available->num_rows() == 0) {
			exit('Room Not Found');
		} 

		$this->db->select("hotel_master.*, foto")
		->join("hotel_gallery", "hotel_master.id = id_hotel_master", "left")
		->where("hotel_gallery.urutan", 1)
		->where("hotel_master.id", $hotel_id);
		$hotel = $this->db->get("hotel_master")->row();
		//echo $ctrler; exit($ctrler);
		//echo "<pre>", print_r($hotel); exit();
		$this->mViewData["hotel"] = $hotel;
		$this->mViewData["checkin"] = $checkin;
		$this->mViewData["checkout"] = $checkout;

		$earlier = new DateTime($checkin);
		$later = new DateTime($checkout);

		$this->mViewData["days"] = $later->diff($earlier)->format("%a");
		$this->render('booking_detail');
	}

	public function invoice(){
		$hotel_id = $this->input->post('hotel_id');
		$booking = $this->input->post();

		// generate qr code
		$this->load->library('ciqrcode');

		$hotel_id_w_checkin_w_customer = $hotel_id . "=+=" . $booking["checkin"] . "=+=" . $booking["nama_lengkap"];
		$ciphertext = $this->encrypt->encode($hotel_id_w_checkin_w_customer);
		$params['data'] = base_url('hotel/detail/download_invoice/'.$ciphertext);
		//exit($params['data']);
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'image_upload/qrcode/qr_hotel_'.$hotel_id.date("dmy", strtotime($booking['checkin'])).'.png';
		$this->ciqrcode->generate($params);

		// generate booking_code
		$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$booking_code = substr(str_shuffle($permitted_chars), 0, 10);

		$this->db->select("hotel_master.*, foto")
		->join("hotel_gallery", "hotel_master.id = id_hotel_master", "left")
		->where("hotel_gallery.urutan", 1)
		->where("hotel_master.id", $hotel_id);
		$hotel = $this->db->get("hotel_master")->row();
		
		$data = array(
			'hotel' => $hotel,
			'booking' => (object) $booking,
			'qr_code' => base_url().'image_upload/qrcode/qr_hotel_'.$hotel_id.date("dmy", strtotime($booking['checkin'])).'.png',
			'qr_link' => $params['data'],
			'booking_code' => $booking_code
		);

		// save to order
		$this->save_order($data);

		// generate pdf
		// $filename = 'invoice_'.$hotel_id.date("dmy", strtotime($booking['checkin'])).'.pdf';
		// $this->load->library('pdf');
		// $this->pdf->load_view('invoice_pdf', $data);
		// $this->pdf->render();
		// Output the generated PDF (1 = download and 0 = preview)
		// $this->pdf->stream($filename, array("Attachment"=>0));

		$this->load->view("invoice", $data);
	}

	public function save_order($data = array()) {
		$hotel = $data["hotel"];
		$booking = $data["booking"];
		$qrcode = $data["qr_code"];
		$booking_code = $data["booking_code"];

		$orders_data = array(
			"id_hotel_master" => $hotel->id,
			"booking_code" => $booking_code,
			"checkin" => $booking->checkin,
			"checkout" => $booking->checkout,
			"waktu_order" => date("Y-m-d H:i:s"),
			"total_bayar" => $hotel->harga * $booking->days,
			"customer" => $booking->nama_lengkap,
			"email" => $booking->email,
			"no_ktp" => $booking->no_ktp,
			"no_hp" => $booking->no_hp,
			"alamat" => $booking->alamat,
			"keterangan" => $booking->keterangan,
			"qrcode" => $qrcode,
			"status" => 0,
		);

		$get_order = $this->db->get_where("orders", ["id_hotel_master" => $hotel->id, "checkin" => $booking->checkin]);

		// save orders if available
		if ($get_order->num_rows() == 0 && !empty($orders_data["id_hotel_master"])) {
			$this->db->insert("orders", $orders_data);
		}
	}

	public function download_invoice($ciphertext){
		$plain_code = $this->encrypt->decode($ciphertext);
		$partial_code = explode("=+=", $plain_code);

		$hotel_id = $partial_code[0];
		$checkin = $partial_code[1];
		$customer = $partial_code[2];

		//print_r($partial_code); exit();

		$this->db->select("hotel_master.*, foto")
		->join("hotel_gallery", "hotel_master.id = id_hotel_master", "left")
		->where("hotel_gallery.urutan", 1)
		->where("hotel_master.id", $hotel_id);
		$hotel = $this->db->get("hotel_master")->row();

		$get_order = $this->db->get_where("orders", ["id_hotel_master" => $hotel_id, "checkin" => $checkin, "customer" => $customer]);
		$booking = $get_order->row();

		if (empty($booking)) {
			exit("Data not Found !!!");
		}

		$booking->nama_lengkap = $booking->customer;

		$earlier = new DateTime($booking->checkin);
		$later = new DateTime($booking->checkout);
		$booking->days = $later->diff($earlier)->format("%a");
		
		$data = array(
			'hotel' => $hotel,
			'booking' => $booking,
			'qr_code' => $booking->qrcode,
			'booking_code' => $booking->booking_code
		);

		// generate pdf
		$filename = 'invoice_'.$hotel_id.date("dmy", strtotime($booking->checkin)).'.pdf';
		$this->load->library('pdf');
		$this->pdf->load_view('invoice_pdf', $data);
		$this->pdf->render();
		// Output the generated PDF (1 = download and 0 = preview)
		$this->pdf->stream($filename, array("Attachment"=>0));
	}

	public function validate_invoice($order_id, $hotel_id){
		$this->db->select("hotel_master.*, foto")
		->join("hotel_gallery", "hotel_master.id = id_hotel_master", "left")
		->where("hotel_gallery.urutan", 1)
		->where("hotel_master.id", $hotel_id);
		$hotel = $this->db->get("hotel_master")->row();

		$get_order = $this->db->get_where("orders", ["id" => $order_id]);
		$booking = $get_order->row();

		if (empty($booking)) {
			exit("Data not Found !!!");
		}
		
		$booking->nama_lengkap = $booking->customer;

		$earlier = new DateTime($booking->checkin);
		$later = new DateTime($booking->checkout);
		$booking->days = $later->diff($earlier)->format("%a");

		$hotel_id_w_checkin_w_customer = $hotel_id . "=+=" . $booking->checkin . "=+=" . $booking->nama_lengkap;
		$ciphertext = $this->encrypt->encode($hotel_id_w_checkin_w_customer);
		$qr_link = base_url('hotel/detail/download_invoice/'.$ciphertext);
		
		$data = array(
			'hotel' => $hotel,
			'booking' => $booking,
			'qr_code' => $booking->qrcode,
			'qr_link' => $qr_link,
			'booking_code' => $booking->booking_code
		);

		$this->load->view("invoice", $data);
	}
}
