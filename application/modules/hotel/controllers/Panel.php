<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Panel management, includes: 
 * 	- Admin Users CRUD
 * 	- Admin User Groups CRUD
 * 	- Admin User Reset Password
 * 	- Account Settings (for login user)
 */
class Panel extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Admin Users CRUD
	public function admin_user()
	{
		$crud = $this->generate_crud('admin_users');
		$crud->columns('groups', 'username', 'full_name', 'active');
		$crud->display_as('groups', 'Level');
		$this->unset_crud_fields('ip_address', 'last_login');

		// cannot change Admin User groups once created
		if ($crud->getState()=='list' || $crud->getState()=='success')
		{
			$crud->set_relation_n_n('groups', 'admin_users_groups', 'admin_groups', 'user_id', 'group_id', 'name');
		}

		// only webmaster can reset Admin User password
		if ( $this->ion_auth->in_group(array('webmaster', 'admin')) )
		{
			$crud->add_action('Reset Password', '', $this->mModule.'/panel/admin_user_reset_password', 'fa fa-repeat');
		}
		
		// disable direct create / delete Admin User
		$crud->unset_add();
		$crud->unset_delete();

		$this->mPageTitle = 'Admin Users';
		$this->render_crud();
	}

	// Create Admin User
	public function admin_user_create()
	{
		// (optional) only top-level admin user groups can create Admin User
		//$this->verify_auth(array('webmaster'));

		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$additional_data = array(
				'full_name'	=> $this->input->post('full_name'),
			);
			$groups = $this->input->post('groups');

			// create user (default group as "members")
			$user = $this->ion_auth->register($username, $password, $email, $additional_data, $groups);
			if ($user)
			{
				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		$groups = $this->ion_auth->groups()->result();
		unset($groups[0]);	// disable creation of "webmaster" account
		$this->mViewData['groups'] = $groups;
		$this->mPageTitle = 'Create Admin User';

		$this->mViewData['form'] = $form;
		$this->render('panel/admin_user_create');
	}

	// Admin User Groups CRUD
	public function admin_user_group()
	{
		$crud = $this->generate_crud('admin_groups');
		$this->mPageTitle = 'Admin User Groups';
		$this->render_crud();
	}

	// Admin User Reset password
	public function admin_user_reset_password($user_id)
	{
		// only top-level users can reset Admin User passwords
		$this->verify_auth(array('webmaster'));

		$form = $this->form_builder->create_form();
		if ($form->validate())
		{
			// pass validation
			$data = array('password' => $this->input->post('new_password'));
			if ($this->ion_auth->update($user_id, $data))
			{
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
			}
			else
			{
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		$this->load->model('admin_user_model', 'admin_users');
		$target = $this->admin_users->get($user_id);
		$this->mViewData['target'] = $target;

		$this->mViewData['form'] = $form;
		$this->mPageTitle = 'Reset Admin User Password';
		$this->render('panel/admin_user_reset_password');
	}

	// Account Settings
	public function account()
	{
		// Update Info form
		$form1 = $this->form_builder->create_form($this->mModule.'/panel/account_update_info');
		$form1->set_rule_group('panel/account_update_info');
		$this->mViewData['form1'] = $form1;

		// Change Password form
		$form2 = $this->form_builder->create_form($this->mModule.'/panel/account_change_password');
		$form1->set_rule_group('panel/account_change_password');
		$this->mViewData['form2'] = $form2;

		$this->mPageTitle = "Account Settings";
		$this->render('panel/account');
	}

	// Submission of Update Info form
	public function account_update_info()
	{
		$data = $this->input->post();
		if ($this->ion_auth->update($this->mUser->id, $data))
		{
			$messages = $this->ion_auth->messages();
			$this->system_message->set_success($messages);
		}
		else
		{
			$errors = $this->ion_auth->errors();
			$this->system_message->set_error($errors);
		}

		redirect($this->mModule.'/panel/account');
	}

	// Submission of Change Password form
	public function account_change_password()
	{
		$data = array('password' => $this->input->post('new_password'));
		if ($this->ion_auth->update($this->mUser->id, $data))
		{
			$messages = $this->ion_auth->messages();
			$this->system_message->set_success($messages);
		}
		else
		{
			$errors = $this->ion_auth->errors();
			$this->system_message->set_error($errors);
		}

		redirect($this->mModule.'/panel/account');
	}

	// Submission of Change Password form
	public function setting()
	{
		$site_data = $this->db->get('site_customisation')->row();
		$form = $this->form_builder->create_form();
		$selected_form = $this->input->post('selected_form');
		if ($selected_form == 'profile') {
			$this->save_profile($form);
		} else if ($selected_form == 'color_scheme') {
			$this->save_color_scheme($form);
		} else if ($selected_form == 'site_header') {
			$this->site_header($form);
		}

		$this->mViewData['form'] = $form;
		$this->mViewData['site_data'] = $site_data;
		$this->mPageTitle = "Customisation";
		$this->render('panel/setting');
	}

	public function save_profile($form) {
		$form->set_rule_group('panel/save_profile');
		if ($form->validate()) {
			$data = elements(array('company_name', 'email', 'telephone', 'address'), $this->input->post());
			$this->db->update('site_customisation', $data);
			$this->system_message->set_success("Data updated successfully");
			refresh();
		}
	}

	public function change_color_scheme($theme) {
		// load session
		$site_customisation = get_userdata('site_customisation');

		// set value on session
		$site_customisation['color_scheme'] = $theme;

		// update session
		set_userdata('site_customisation',$site_customisation);

		// save to database
		$this->db->update('site_customisation', ['color_scheme' => $theme]);

		// response
		echo direct_alert("success", "Color Scheme Updated to $theme");
		exit();
	}

	public function save_site_header() {
		$data = array();
		$data['site_title'] = $this->input->post('site_title');
		

		$this->load->helper('file_upload_helper');
		$upload_path = './image_upload/logo/';
		$response = image_upload('logo', $upload_path);

		if ($response['status']) {
			$data['logo'] = $response['value'];
			// load session
			$site_customisation = get_userdata('site_customisation');

			// set value on session
			$site_customisation['logo'] = $data['logo'];
			$site_customisation['site_title'] = $data['site_title'];

			// update session
			set_userdata('site_customisation',$site_customisation);
			$this->db->update('site_customisation', $data);
			echo direct_alert('success', 'Site Header Updated!');
		} else {
			echo direct_alert('danger', $response['value']);
		}
		//redirect($refer, 'refresh');
	}

	public function mouse_entered() {
		echo "hoho";
	}
	
	/**
	 * Logout user
	 */
	public function logout()
	{
		$this->ion_auth->logout();
		redirect($this->mConfig['login_url']);
	}
}
