<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends Admin_Controller {

	public function index()
	{
		$this->mViewData['customers'] = $this->db->get('customers')->result();
		$this->render('customers');
	}
}
