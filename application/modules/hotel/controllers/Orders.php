<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Admin_Controller {

	public function index()
	{
		$this->db->select('customers.nama as nama_customer, customers.email as email_customer, customers.alamat as alamat_customer, customers.telepon as telepon_customer, kos_master.nama_kos, kos_master.alamat as alamat_kos_master, kos_master.harga, kos_master.deskripsi, kos_periode.keterangan, vendors.nama as nama_vendor, vendors.email as email_vendor, vendors.telepon as telepon_vendor, orders.*')
				->join('customers', 'customers.id = orders.id_customer', 'left')
				->join('kos_master', 'kos_master.id = orders.id_kos_master', 'left')
				->join('kos_periode', 'kos_periode.id = orders.id_kos_periode', 'left')
				->join('vendors', 'vendors.id = kos_master.id_vendor', 'left');
		$this->mViewData['orders'] = $this->db->get('orders')->result();
		
		$this->render('orders');
	}
}
