<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CI Bootstrap 3 Configuration
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views 
| when calling MY_Controller's render() function. 
| 
| See example and detailed explanation from:
| 	/application/config/ci_bootstrap_example.php
*/

$template_path = '/themes/amado/';
$body_class = '';

$config['ci_bootstrap'] = array(

	// Site name
	'site_name' => 'Hotel',

	// Site Logo
	'site_logo' => '/assets/image/logo.png',

	// Template Path
	'template_path' => $template_path,

	// Default page title prefix
	'page_title_prefix' => '',

	// Default page title
	'page_title' => '',

	// Default meta data
	'meta_data'	=> array(
		'author'		=> 'Simpelin Dev',
		'description'	=> 'Cari Hotel gk pake ribet.',
		'keywords'		=> 'Hotel,Booking,Murah,Nyaman'
	),

	// Default scripts to embed at page head or end
	'scripts' => array(
		'head'	=> array(
		),
		'foot'	=> array(
			//'assets/dist/frontend/lib.min.js',
			//'assets/dist/frontend/app.min.js',
			"assets/dist/vendors/base/vendors.bundle.js",
			"assets/dist/demo/default/base/scripts.bundle.js",
			//$template_path."js/jquery/jquery-2.2.4.min.js",
			$template_path."js/popper.min.js",
			//$template_path."js/bootstrap.min.js",
			$template_path."js/plugins.js",
			$template_path."js/active.js",
			"assets/dist/forms/widgets/select2.js",
		),
	),

	// Default stylesheets to embed at page head
	'stylesheets' => array(
		'screen' => array(
			//'assets/dist/frontend/lib.min.css',
			//'assets/dist/frontend/app.min.css',
			"assets/dist/vendors/base/vendors.bundle.css",
			"assets/dist/demo/default/base/style.bundle.css",
			$template_path."css/core-style.css",
			$template_path."style.css",
		)
	),

	// Default CSS class for <body> tag
	'body_class' => $body_class,
	
	// Multilingual settings
	'languages' => array(
		// 'default'		=> '',
		// 'autoload'		=> array('general'),
		// 'available'		=> array(
		// 	'en' => array(
		// 		'label'	=> 'English',
		// 		'value'	=> 'english'
		// 	),
		// 	'zh' => array(
		// 		'label'	=> '繁體中文',
		// 		'value'	=> 'traditional-chinese'
		// 	),
		// 	'cn' => array(
		// 		'label'	=> '简体中文',
		// 		'value'	=> 'simplified-chinese'
		// 	),
		// 	'es' => array(
		// 		'label'	=> 'Español',
		// 		'value' => 'spanish'
		// 	)
		// )
	),

	// Google Analytics User ID
	'ga_id' => '',

	// Menu items
	'menu' => array(
		'home' => array(
			'name'		=> 'Home',
			'url'		=> '',
		),
	),

	// Login page
	'login_url' => 'hotel',

	// Restricted pages
	'page_auth' => array(
	),

	// Email config
	'email' => array(
		'from_email'		=> '',
		'from_name'			=> '',
		'subject_prefix'	=> '',
		
		// Mailgun HTTP API
		'mailgun_api'		=> array(
			'domain'			=> '',
			'private_api_key'	=> ''
		),
	),

	// Debug tools
	'debug' => array(
		'view_data'	=> FALSE,
		'profiler'	=> FALSE
	),
);

/*
| -------------------------------------------------------------------------
| Override values from /application/config/config.php
| -------------------------------------------------------------------------
*/
$config['sess_cookie_name'] = 'hotel_frontend';