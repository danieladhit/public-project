<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Admin_Controller {

	public function index()
	{
		$this->db->select('hotel_master.nama_hotel, hotel_master.alamat as alamat_hotel_master, hotel_master.harga, hotel_master.deskripsi, orders.*')
		->join('hotel_master', 'hotel_master.id = orders.id_hotel_master', 'left');
		$this->mViewData['orders'] = $this->db->get('orders')->result();
		
		$this->render('orders');
	}

	public function validation($order_id, $hotel_id){

		if($this->input->post('status')) {
			$this->validation_process($order_id, $hotel_id);
		}

		$this->load->library('encrypt');

		$this->db->select("hotel_master.*, foto")
		->join("hotel_gallery", "hotel_master.id = id_hotel_master", "left")
		->where("hotel_gallery.urutan", 1)
		->where("hotel_master.id", $hotel_id);
		$hotel = $this->db->get("hotel_master")->row();

		$get_order = $this->db->get_where("orders", ["id" => $order_id]);
		$booking = $get_order->row();

		if (empty($booking)) {
			exit("Data not Found !!!");
		}
		
		$booking->nama_lengkap = $booking->customer;

		$earlier = new DateTime($booking->checkin);
		$later = new DateTime($booking->checkout);
		$booking->days = $later->diff($earlier)->format("%a");

		$hotel_id_w_checkin_w_customer = $hotel_id . "=+=" . $booking->checkin . "=+=" . $booking->nama_lengkap;
		$ciphertext = $this->encrypt->encode($hotel_id_w_checkin_w_customer);
		$qr_link = base_url('hotel/detail/download_invoice/'.$ciphertext);
		
		$this->mViewData = array(
			'hotel' => $hotel,
			'booking' => $booking,
			'qr_code' => $booking->qrcode,
			'qr_link' => $qr_link,
			'booking_code' => $booking->booking_code,
		);

		$this->render("invoice");
	}

	public function validation_process($order_id, $hotel_id){
		$status = $this->input->post('status');

		$get_order = $this->db->get_where("orders", ["id" => $order_id]);
		$booking = $get_order->row();

		if ($status == 'null') {
			set_alert('danger', 'Pilih Action terlebih dahulu !!');
			refresh();
		}

		if (empty($booking)) {
			set_alert('danger', 'Maaf data tidak ditemukan');
			redirect(base_url('hotel-backend/orders'));
		} else {
			$this->db->update("orders", ["status" => $status], ["id" => $order_id]);
			set_alert('success', 'Update Status Berhasil !!!');
			redirect(base_url('hotel-backend/orders'));
		}
	}

}
