<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		redirect($this->mModule.'/setting/kategori_hotel');
	}

	public function kategori_hotel() 
	{
		$crud = $this->generate_crud('hotel_kategori');
		$crud->set_subject('Kategori Hotel');
		// disable direct create / delete Frontend User
		$crud->unset_delete();

		$this->mPageTitle = 'Kategori Hotel';
		$this->render_crud();
	}

	public function kota() 
	{
		$crud = $this->generate_crud('kota');
		$crud->set_subject('Kota');
		// disable direct create / delete Frontend User
		$crud->unset_delete();

		$this->mPageTitle = 'Kota';
		$this->render_crud();
	}
}
