<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel_master extends Admin_Controller {

	public function index()
	{
		$this->db->select('kota.nama as kota, hotel_master.*')
		->join('kota', 'kota.id = hotel_master.id_kota', 'left');
		$this->mViewData['hotel_master'] = $this->db->get('hotel_master')->result();
		$this->render('hotel_master');
	}

	public function tambah()
	{
		$this->mViewData = array(
			'nama_hotel' => '',
			'id_kota' => '',
			'alamat' => '',
			'harga' => '',
			'jumlah' => '',
			'deskripsi' => '',
			'ex_24_hour' => '',
			'ex_elevator' => '',
			'ex_ac' => '',
			'ex_coffee_maker' => '',
			'ex_wifi' => '',
			'ex_tv' => '',
			'ex_parking' => '',
			'ex_restaurant' => '',
			'ex_swimming' => '',
		);

		if ($this->input->post('nama_hotel')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_24_hour','ex_elevator','ex_ac','ex_coffee_maker','ex_wifi','ex_tv','ex_parking','ex_restaurant','ex_swimming');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->insert('hotel_master', $data);
			$id_hotel = $this->db->insert_id();

			set_alert('success', 'Informasi hotel berhasil disimpan');
			redirect($this->mModule.'/hotel_master/manage_gallery/'.$id_hotel);
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_vendors = $this->db->get('vendors');
		$vendors = $get_vendors->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['vendors'] = $vendors;
		$this->mPageTitle = 'Tambah hotel';
		$this->render('add_hotel');
	}

	public function ubah($id_hotel)
	{
		// Params Validation
		$get_hotel = $this->db->where('id', $id_hotel)
		->get('hotel_master');
		$hotel = $get_hotel->row();

		if (empty($hotel)) {
			set_alert('danger', 'Data yang anda cari tidak ditemukan');
			redirect($this->mModule.'/hotel_master/');
		}

		foreach ($hotel as $key => $value) {
			$this->mViewData[$key] = $value;
		}

		if ($this->input->post('nama_hotel')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_24_hour','ex_elevator','ex_ac','ex_coffee_maker','ex_wifi','ex_tv','ex_parking','ex_restaurant','ex_swimming');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->update('hotel_master', $data, ['id' => $id_hotel]);

			set_alert('success', 'Informasi hotel berhasil diubah');
			redirect($this->mModule.'/hotel_master/manage_gallery/'.$id_hotel);
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_kota = $this->db->get('kota');
		$kota = $get_kota->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['kota'] = $kota;
		$this->mPageTitle = 'Tambah Hotel';
		$this->render('add_hotel');
	}

	public function preview($id_hotel)
	{
		// ---- hotel DATA -----
		// Params Validation
		$get_hotel = $this->db->where('id', $id_hotel)
		->get('hotel_master');
		$hotel = $get_hotel->row();

		if (empty($hotel)) {
			set_alert('danger', 'Data yang anda cari tidak ditemukan');
			redirect($this->mModule.'/hotel_master/');
		}

		foreach ($hotel as $key => $value) {
			$this->mViewData[$key] = $value;
		}

		if ($this->input->post('nama_hotel')) {
			$data = $this->input->post();
			$data['status'] = 1;

			// get checkbox value
			$extra = array('ex_24_hour','ex_elevator','ex_ac','ex_coffee_maker','ex_wifi','ex_tv','ex_parking','ex_restaurant','ex_swimming');
			foreach ($extra as $ex) {
				if (isset($data[$ex])) {
					$data[$ex] = 1;
				} else {
					$data[$ex] = 0;
				}
			}

			$this->db->update('hotel_master', $data, ['id' => $id_hotel]);

			set_alert('success', 'Informasi hotel berhasil diubah');
			refresh();
		}

		// Get User Admin Role
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		$get_kota = $this->db->get('kota');
		$kota = $get_kota->result();

		$this->mViewData['user_role'] = $user_role;
		$this->mViewData['kota'] = $kota;


		// --- hotel GALLERY ----
		$get_hotel_gallery = $this->db->where('id_hotel_master', $id_hotel)
		->limit(5)
		->order_by('urutan', 'ASC')
		->get('hotel_gallery');
		$hotel_gallery = $get_hotel_gallery->result();

		$images_detail = array();

		// get images detail
		foreach ($hotel_gallery as $key => $row) {
			$name = str_replace('./image_upload/hotel/', '', $row->foto);
			$size = filesize($row->foto);
			$url = base_url($row->foto);
			$images_detail[$row->urutan] = array('name' => $name, 'size' => $size, 'url' => $url);
		}

		// validation images detail must have 5 files
		$i=1;
		while ($i <= 5) {
			if(!isset($images_detail[$i])) {
				$images_detail[$i] = array();
			}
			$i++;
		}
		$this->mViewData['id_hotel'] = $id_hotel;
		$this->mViewData['images_detail'] = $images_detail;

		$this->mPageTitle = 'Preview hotel';
		$this->render('preview_hotel');
	}

	public function manage_gallery($id_hotel = 0) {
		$get_hotel_gallery = $this->db->where('id_hotel_master', $id_hotel)
		->limit(5)
		->order_by('urutan', 'ASC')
		->get('hotel_gallery');
		$hotel_gallery = $get_hotel_gallery->result();

		$images_detail = array();

		// get images detail
		foreach ($hotel_gallery as $key => $row) {
			$name = str_replace('./image_upload/hotel/', '', $row->foto);
			$size = filesize($row->foto);
			$url = base_url($row->foto);
			$images_detail[$row->urutan] = array('name' => $name, 'size' => $size, 'url' => $url);
		}

		// validation images detail must have 5 files
		$i=1;
		while ($i <= 5) {
			if(!isset($images_detail[$i])) {
				$images_detail[$i] = array();
			}
			$i++;
		}

		//print_r($images_detail); exit();

		$this->mPageTitle = 'Upload Foto hotel';
		$this->mViewData['id_hotel'] = $id_hotel;
		$this->mViewData['images_detail'] = $images_detail;
		$this->render('add_hotel_gallery');
	}

	public function upload_foto($urutan=0, $id_user=0, $id_hotel=0)
	{
		// Only Vendor Can Be Upload -- Validation
		$user = $this->mUser;
		$get_role = $this->db->select('admin_groups.name as role_name, admin_groups.description as role_description, admin_users_groups.*')
		->join('admin_users_groups', 'group_id = admin_groups.id')
		->where('user_id', $user->id)
		->get('admin_groups');

		$user_role = $get_role->row();

		// Params Validation
		$get_hotel = $this->db->where('id', $id_hotel)
		->get('hotel_master');
		$hotel = $get_hotel->row();

		if (empty($hotel)) {
			$this->output->set_status_header('401', 'hotel Data is Missing');
			exit('hotel Data is Missing');
		}

		if ($id_user != $user->id) {
			$this->output->set_status_header('401', 'User Not Found');
			exit('User Not Found');
		}

		$this->load->helper('file_upload_helper');
		$upload_path = './image_upload/hotel/';
		$response = image_upload('file', $upload_path);

		if ($response['status']) {
			$data['id_hotel_master'] = $id_hotel;
			$data['foto'] = $response['value'];
			$data['urutan'] = $urutan;

			$get_hotel_gallery = $this->db->where('id_hotel_master', $id_hotel)
			->where('urutan', $urutan)
			->get('hotel_gallery');
			$hotel_gallery = $get_hotel_gallery->row();

			if (empty($hotel_gallery)) {
				$this->db->insert('hotel_gallery', $data);
			} else {
				$this->db->update('hotel_gallery', $data, ['id' => $hotel_gallery->id]);
			}

			exit($response['value']);
		} else {
			$this->output->set_status_header('401', $response['value']);
			exit($response['value']);
		}
	}

	public function delete_foto()
	{
		$filename = $this->input->post('name');
		$upload_path = './image_upload/hotel/';
		//$full_path = $upload_path . $filename;
		$full_path = $filename;

		// Delete DB
		$this->db->delete('hotel_gallery',['foto' => $full_path]);

		// Delete File
		unlink($full_path);

		exit('Deleted Succesfully');
	}
}
