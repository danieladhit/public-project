<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

	public function index()
	{
		//$this->load->model('user_model', 'users');
		/*$this->mViewData['count'] = array(
			'users' => $this->users->count_all(),
		);*/
		$hotel = $this->db->get("hotel_master")->result();
		$orders = $this->db->get("orders")->result();

		$this->mViewData["hotel"] = $hotel;
		$this->mViewData["orders"] = $orders;
		$this->render('home');
	}
}
