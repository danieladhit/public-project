<!-- START TABLE PORTLET-->
<div class="m-portlet m-portlet--mobile">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data Vendors
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="vendors/tambah" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>New record</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="commonDatatable">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nama</th>
					<th>Email</th>
					<th>Alamat</th>
					<th>Telepon</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($vendors as $key => $row) { ?>
				<tr>
					<td><?=$row->id ?></td>
					<td><?=$row->nama ?></td>
					<td><?=$row->email ?></td>
					<td><?=$row->alamat ?></td>
					<td><?=$row->telepon ?></td>
					<td>
						<?php
						$status = $row->status == 1 ? 'Active' : 'Not Active';
						$badge_type = $row->status == 1 ? 'success' : 'danger';
						?>
						<span style="width: 110px">	
							<span class="m-badge m-badge--<?=$badge_type ?> m-badge--wide"><?=$status ?></span>
						</span>	
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!-- END TABLE PORTLET-->