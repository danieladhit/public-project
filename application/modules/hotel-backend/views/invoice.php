<style>
    .clearfix:after {
      content: "";
      display: table;
      clear: both;
    }

    a {
      color: #5D6975;
      text-decoration: underline;
    }

    .body {
      width: 100%;  
      height: auto; 
      margin: 0 auto; 
      color: #001028;
      /*background: #FFFFFF; */
      font-family: Arial, sans-serif; 
      font-size: 12px; 
      font-family: Arial;
    }

    header {
      padding: 10px 0;
      margin-bottom: 30px;
    }

    #logo {
      text-align: center;
      margin-bottom: 10px;
    }

    #logo img {
      width: 90px;
    }

    h1 {
      border-top: 1px solid  #5D6975;
      border-bottom: 1px solid  #5D6975;
      color: #5D6975;
      font-size: 2.4em;
      line-height: 1.4em;
      font-weight: normal;
      text-align: center;
      margin: 0 0 20px 0;
      background: url(dimension.png);
    }

    #project {
      float: left;
    }

    #project span {
      color: #5D6975;
      text-align: right;
      width: 52px;
      margin-right: 10px;
      display: inline-block;
      font-size: 0.8em;
    }

    #company {
      float: right;
      text-align: right;
    }

    #project div,
    #company div {
      white-space: nowrap;        
    }

    table {
      width: 100%;
      border-collapse: collapse;
      border-spacing: 0;
      margin-bottom: 20px;
    }

    table tr:nth-child(2n-1) td {
      background: #F5F5F5;
    }

    table th,
    table td {
      text-align: center;
    }

    table th {
      padding: 5px 20px;
      color: #5D6975;
      border-bottom: 1px solid #C1CED9;
      white-space: nowrap;        
      font-weight: normal;
    }

    table .service,
    table .desc {
      text-align: left;
    }

    table td {
      padding: 20px;
      text-align: right;
    }

    table td.service,
    table td.desc {
      vertical-align: top;
    }

    table td.unit,
    table td.qty,
    table td.total {
      font-size: 1.2em;
    }

    table td.grand {
      border-top: 1px solid #5D6975;;
    }

    #notices .notice {
      color: #5D6975;
      font-size: 1.2em;
    }
  </style>
<div class="body">
<?=alert_box() ?>
<header class="clearfix">
  <div id="logo">
    <img src="/image_upload/icon/hotel-icon.png">
    <h2 style="display: inline-block;margin: 0px;line-height: 30px;vertical-align: bottom;">.com</h2>
  </div>
  <h1>KODE BOOKING : <?=$booking_code ?></h1>
  <div id="company" class="clearfix">
    <div>HOTEL.COM,<br /> JAKARTA, INDONESIA</div>
        <!-- <div>0812 8232 1234</div>
          <div><a href="mailto:company@example.com">admin@hotel.com</a></div> -->
          <div>
            <a href="<?=$qr_link ?>" target="_blank">
              <img src="<?=$qr_code ?>" alt="QR Code" style="width: 80px; height: auto;">
            </a>
          </div>
        </div>
        <div id="project">
          <div><span>HOTEL</span> <?=$hotel->nama_hotel ?></div>
          <div><span>CLIENT</span> <?=$booking->nama_lengkap ?></div>
          <div><span>KTP</span> <?=$booking->no_ktp ?></div>
          <div><span>ADDRESS</span> <?=$booking->alamat ?></div>
          <div><span>EMAIL</span> <a href="mailto:<?=$booking->email ?>"><?=$booking->email ?></a></div>
          <div><span>DATE</span> <?=date("d M Y") ?></div>
          <div><span>BOOKING</span> <?=date("d M Y", strtotime($booking->checkin)) . " - " . date("d M Y", strtotime($booking->checkout)) ?></div>
        </div>
      </header>
      <main>
        <table>
          <thead>
            <tr>
              <!-- <th class="service">SERVICE</th> -->
              <th class="desc">DESCRIPTION</th>
              <th>PRICE</th>
              <th>DAYS</th>
              <th>TOTAL</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <!-- <td class="service">Design</td> -->
              <td class="desc"><?=$hotel->nama_hotel ." - ". $booking->keterangan ?></td>
              <td class="unit">Rp. <?=number_format($hotel->harga) ?></td>
              <td class="qty"><?=$booking->days ?></td>
              <td class="total">Rp. <?=number_format($hotel->harga*$booking->days) ?></td>
            </tr>
            <tr>
              <td colspan="3">SUBTOTAL</td>
              <td class="total">Rp. <?=number_format($hotel->harga*$booking->days) ?></td>
            </tr>
          <!-- <tr>
            <td colspan="3">TAX 25%</td>
            <td class="total">$1,300.00</td>
          </tr> -->
          <tr>
            <td colspan="3" class="grand total">GRAND TOTAL</td>
            <td class="grand total">Rp. <?=number_format($hotel->harga*$booking->days) ?></td>
          </tr>
        </tbody>
      </table>
      <div id="notices">
        <div>ACTION:</div>
        <div class="notice">
          <form action="" class="form form-inline" method="post">
            <div class="form-group">
              <select name="status" id="" class="form-control">
                <option value="null">-- Pilih Aksi --</option>
                <option value="1">Valid</option>
                <option value="2">Not Valid</option>
                <option value="3">Cancel</option>
              </select>
            </div>
            <div class="form-group">
              <button class="btn btn-info" type="submit" style="margin-left: 20px">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </main>
</div>