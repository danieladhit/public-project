<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">

	<?php $this->load->view('_partials/navbar'); ?>

	<!-- begin::Body -->
	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

		<?php $this->load->view('_partials/sidemenu'); ?>

		<div class="m-grid__item m-grid__item--fluid m-wrapper">

			<!-- BEGIN: Subheader -->
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title "><?php echo $page_title; ?></h3>
					</div>
				</div>
			</div>

			<!-- END: Subheader -->
			<div class="m-content">
				<?php $this->load->view($inner_view); ?>
			</div>
		</div>
		<!-- end:: Body -->
	</div>

	<?php // Footer ?>
	<?php $this->load->view('_partials/footer'); ?>

</div>
<!-- end:: Page -->



<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
	<i class="la la-arrow-up"></i>
</div>