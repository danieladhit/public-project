<?=alert_box() ?>
<!-- START TABLE PORTLET-->
<div class="m-portlet m-portlet--mobile">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data Hotel
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="hotel_master/tambah" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>Tambah Hotel</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="commonDatatable">
			<thead>
				<tr>
					<th>Nama Hotel</th>
					<th>Kota</th>
					<th>Alamat</th>
					<th>Harga</th>
					<th>Fasilitas</th>
					<th>Deskripsi</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($hotel_master as $key => $row) { ?>
				<tr>
					<td>
						<a href="hotel_master/preview/<?=$row->id ?>">
							<?=$row->nama_hotel ?>
						</a>
					</td>
					<td><?=$row->kota ?></td>
					<td><?=$row->alamat ?></td>
					<td><?=$row->harga ?></td>
					<td>
						<?php 
						$fasilitas = array();
						$extra = array('ex_24_hour', 'ex_elevator', 'ex_ac', 'ex_coffee_maker', 'ex_wifi', 'ex_tv', 'ex_parking', 'ex_restaurant', 'ex_swimming');
						foreach ($extra as $ex) {
							if ($row->$ex == 1) {
								$fasilitas[] = str_replace('ex_', '', $ex);
							}
						}

						echo implode($fasilitas, ',');
						?>
					</td>
					<td><?=$row->deskripsi ?></td>
					<td>
						<?php
						$status = $row->status == 1 ? 'Available' : 'Not Available';
						$badge_type = $row->status == 1 ? 'success' : 'danger';
						?>
						<span style="width: 110px">	
							<span class="m-badge m-badge--<?=$badge_type ?> m-badge--wide"><?=$status ?></span>
						</span>	
					</td>
					<td>
						<a href="hotel_master/ubah/<?=$row->id ?>" class="btn btn-warning m-btn m-btn--icon m-btn--icon-only">
							<i class="fa fa-edit"></i>
						</a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!-- END TABLE PORTLET-->