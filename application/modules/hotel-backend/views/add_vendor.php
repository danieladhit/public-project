<?=alert_box() ?>
<div class="row">
	<div class="col-md-6">
		<!--begin::Portlet-->
		<div class="m-portlet m-portlet--tab">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
							<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">
							Informasi Vendor
						</h3>
					</div>
				</div>
			</div>

			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="">
				<div class="m-portlet__body">
					<div class="form-group m-form__group">
						<label for="nama">*Nama</label>
						<input type="text" class="form-control m-input" id="full_name" aria-describedby="full_name" name="full_name" value="<?=$full_name ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="alamat">*Alamat</label>
						<input type="text" class="form-control m-input" id="alamat" aria-describedby="alamat" name="alamat" value="<?=$alamat ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="telepon">*Telepon</label>
						<input type="number" class="form-control m-input" id="telepon" aria-describedby="telepon" name="telepon" value="<?=$telepon ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="username">*Username</label>
						<input type="text" class="form-control m-input" id="username" aria-describedby="username" name="username" value="<?=$username ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="email">*Email</label>
						<input type="email" class="form-control m-input" id="email" aria-describedby="email" name="email" value="<?=$email ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="password">*Password</label>
						<input type="password" class="form-control m-input" id="password" aria-describedby="password" name="password" required="required">
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="./kos_master" class="btn btn-secondary">Back</a>
					</div>
				</div>
			</form>

			<!--end::Form-->
		</div>

		<!--end::Portlet-->

	</div>
</div>
