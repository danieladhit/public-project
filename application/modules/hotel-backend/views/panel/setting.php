<?php echo $form->messages(); ?>
<div id="messages"></div>

<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#profile" data-toggle="tab" aria-expanded="false">Profile</a></li>
				<li class=""><a href="#color-scheme" data-toggle="tab" aria-expanded="false">Color Scheme</a></li>
				<li class=""><a href="#settings" data-toggle="tab" aria-expanded="true">Site Header</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="profile">
					<form class="form-horizontal" method="POST">
						<input type="hidden" name="selected_form" value="profile">
						<div class="form-group">
							<label for="inputName" class="col-sm-2 control-label">Company Name</label>

							<div class="col-sm-10">
								<input type="text" name="company_name" class="form-control" id="inputName" placeholder="Name" value="<?=$site_data->company_name?>">
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail" class="col-sm-2 control-label">Email</label>

							<div class="col-sm-10">
								<input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" value="<?=$site_data->email?>">
							</div>
						</div>
						<div class="form-group">
							<label for="inputName" class="col-sm-2 control-label">Telephone</label>

							<div class="col-sm-10">
								<input type="text" name="telephone" class="form-control" id="inputName" placeholder="Telephone" value="<?=$site_data->telephone?>">
							</div>
						</div>
						<div class="form-group">
							<label for="inputExperience" class="col-sm-2 control-label">Address</label>

							<div class="col-sm-10">
								<textarea class="form-control" name="address" id="inputExperience" placeholder="Address"><?=$site_data->address?></textarea>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-danger">Submit</button>
							</div>
						</div>
					</form>
				</div>
				<!-- /.tab-pane -->
				<div class="tab-pane" id="color-scheme">
					<input type="hidden" name="selected_form" value="color_scheme">
					<ul class="list-unstyled clearfix">
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-blue" href="javascript:void(0)" data-skin="skin-blue" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px">Blue</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-black" href="javascript:void(0)" data-skin="skin-black" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="unblockUI clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px">Black</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-purple" href="javascript:void(0)" data-skin="skin-purple" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple unblockUI" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px">Purple</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-green" href="javascript:void(0)" data-skin="skin-green" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px">Green</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-red" href="javascript:void(0)" data-skin="skin-red" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px">Red</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-yellow" href="javascript:void(0)" data-skin="skin-yellow" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px">Yellow</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-blue-light" href="javascript:void(0)" data-skin="skin-blue-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px" style="font-size: 12px">Blue Light</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-black-light" href="javascript:void(0)" data-skin="skin-black-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="unblockUI clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px" style="font-size: 12px">Black Light</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-purple-light" href="javascript:void(0)" data-skin="skin-purple-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px" style="font-size: 12px">Purple Light</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-green-light" href="javascript:void(0)" data-skin="skin-green-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px" style="font-size: 12px">Green Light</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-red-light" href="javascript:void(0)" data-skin="skin-red-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>
							</a>
							<p class="text-center" style="margin-bottom:20px" style="font-size: 12px">Red Light</p>
						</li>
						<li class="col-md-3">
							<a ic-target="#messages" ic-post-to="panel/change_color_scheme/skin-yellow-light" href="javascript:void(0)" data-skin="skin-yellow-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="unblockUI clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a>
							<p class="text-center" style="margin-bottom:20px" style="font-size: 12px">Yellow Light</p>
						</li></ul>
					</div>
					<!-- /.tab-pane -->

					<div class="tab-pane" id="settings">
						<form id="formSiteHeader" class="form-horizontal" method="POST" ic-target="#messages" ic-post-to="panel/save_site_header/" enctype='multipart/form-data'>
							<input type="hidden" name="selected_form" value="site_header">
							<div class="form-group">
								<label for="inputName" class="col-sm-2 control-label">Logo</label>

								<div class="col-sm-10">
									<img id="logoImage" height="80px" width="auto" src="<?=site_url($site_data->logo)?>" alt="your image" />
									<br><br>
									<input type='file' name="logo" onchange="readURL(this);" />
								</div>
							</div>
							<div class="form-group">
								<label for="siteTitle" class="col-sm-2 control-label">Site Title</label>

								<div class="col-sm-10">
									<input type="text" name="site_title" class="form-control" id="siteTitle" placeholder="Site Title" value="<?=$site_data->site_title?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-danger">Submit</button>
								</div>
							</div>
						</form>
					</div>
					<!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			</div>
			<!-- /.nav-tabs-custom -->
		</div>

	</div>

	<script>
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#logoImage')
					.attr('src', e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}

		$(function(){
          $('.unblockUI').on('beforeSend.ic', function(){
            $.blockUI();
          }).on('complete.ic', function(){
            $.unblockUI();
          });
        })	

        $(function(){
          $('#formSiteHeader').on('beforeSend.ic', function(){
            $.blockUI();
          }).on('complete.ic', function(evt, elt, data){
          	console.log(data);
            $.unblockUI();
            var siteTitle = $("#siteTitle").val();
            $('.logo').html('<b>' + siteTitle + '</b>');
          });
        })		
	</script>