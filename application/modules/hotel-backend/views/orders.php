<?=alert_box() ?>
<!-- START TABLE PORTLET-->
<div class="m-portlet m-portlet--mobile">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data Orders
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<!-- <li class="m-portlet__nav-item">
					<a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>New record</span>
						</span>
					</a>
				</li> -->
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="commonDatatable">
			<thead>
				<tr>
					<th>Customer</th>
					<th>Kode Booking</th>
					<th>Booking Date</th>
					<th>Nama Hotel</th>
					<th>Harga</th>
					<th>Status</th>
					<th>Invoice</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($orders as $key => $row) { ?>
					<tr>
						<td><?=$row->customer ?></td>
						<td><strong><?=$row->booking_code ?></strong></td>
						<td><?=date("d M Y", strtotime($row->checkin)) ." s/d " . date("d M Y", strtotime($row->checkout)) ?></td>
						<td><?=$row->nama_hotel ?></td>
						<td><?=number_format($row->harga) ?></td>
						<td>
							<?php
							$status = $row->status == 1 ? 'Sudah di verifikasi' : 'Belum di verifikasi';
							$badge_type = $row->status == 1 ? 'success' : 'danger';
							?>
							<span style="width: 110px">	
								<span class="m-badge m-badge--<?=$badge_type ?> m-badge--wide"><?=$status ?></span>
							</span>	
						</td>
						<td>
							<a href="<?=base_url('hotel/detail/validate_invoice/'.$row->id.'/'.$row->id_hotel_master) ?>">
								<img src="<?=$row->qrcode ?>" alt="QR Code" style="width: 80px; height: auto">
							</a>
						</td>
						<td>
						<a href="orders/validation/<?=$row->id.'/'.$row->id_hotel_master ?>" class="btn btn-warning m-btn m-btn--icon m-btn--icon-only">
							<i class="fa fa-edit"></i>
						</a>
					</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!-- END TABLE PORTLET-->