<?=alert_box() ?>
<div class="row">
	<div class="col-md-6">
		<!--begin::Portlet-->
		<div class="m-portlet m-portlet--tab">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
							<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">
							Informasi Hotel
						</h3>
					</div>
				</div>
			</div>

			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="">
				<div class="m-portlet__body">
					<div class="form-group m-form__group m--margin-top-10">
						<div class="alert m-alert m-alert--default" role="alert">
							Langkah 1 - Isi detail dan informasi tentang hotel
						</div>
					</div>
					<?php if($user_role->role_name == 'admin'){ ?>
					<div class="form-group m-form__group">
						<label for="id_kota">*Pilih Kota</label>
						<select class="form-control m-input" id="id_kota"  name="id_kota" value="<?=$id_kota ?>" required="required">
							<?php foreach($kota as $key => $row){ ?>
								<option <?=$row->id == $id_kota ? 'selected' : ''  ?> value="<?=$row->id ?>"><?=$row->nama ?></option>
							<?php } ?>
						</select>
					</div>
					<?php } ?>
					<div class="form-group m-form__group">
						<label for="nama_hotel">*Nama hotel</label>
						<input type="text" class="form-control m-input" id="nama_hotel" aria-describedby="nama_hotel" name="nama_hotel" value="<?=$nama_hotel ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="alamat">*Alamat hotel</label>
						<input type="text" class="form-control m-input" id="alamat" aria-describedby="alamat" name="alamat" value="<?=$alamat ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="harga">*Harga hotel</label>
						<input type="number" class="form-control m-input" id="harga" aria-describedby="harga" name="harga" value="<?=$harga ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="jumlah">*Jumlah</label>
						<input type="number" class="form-control m-input" id="jumlah" aria-describedby="jumlah" name="jumlah" value="<?=$jumlah ?>" required="required">
					</div>
					<div class="form-group m-form__group">
						<label for="deskripsi">*Deskripsi</label>
						<textarea class="form-control m-input" id="deskripsi" rows="3" name="deskripsi" required="required"><?=$deskripsi ?></textarea>
					</div> 
					<div class="m-form__group form-group">
						<label>Fasilitas</label>
						<div class="m-checkbox-list">
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_ac == 1 ? 'checked' : '' ?> name="ex_ac"> Air Conditioner (AC)
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_coffee_maker == 1 ? 'checked' : '' ?> name="ex_coffee_maker"> Coffee Maker
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_wifi == 1 ? 'checked' : '' ?> name="ex_wifi"> Wifi
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_tv == 1 ? 'checked' : '' ?> name="ex_tv"> TV
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_24_hour == 1 ? 'checked' : '' ?> name="ex_24_hour"> 24-Hour Front Desk
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_parking == 1 ? 'checked' : '' ?> name="ex_parking"> Parking
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_restaurant == 1 ? 'checked' : '' ?> name="ex_restaurant"> Restaurant
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_swimming == 1 ? 'checked' : '' ?> name="ex_swimming"> Swimming Pool
								<span></span>
							</label>
							<label class="m-checkbox m-checkbox--brand">
								<input type="checkbox" <?=$ex_elevator == 1 ? 'checked' : '' ?> name="ex_elevator"> Elevator
								<span></span>
							</label>
						</div>
						<span class="m-form__help">Pilih fasilitas yang ada di hotel ini</span>
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="./hotel_master" class="btn btn-secondary">Back</a>
					</div>
				</div>
			</form>

			<!--end::Form-->
		</div>

		<!--end::Portlet-->

	</div>
</div>
