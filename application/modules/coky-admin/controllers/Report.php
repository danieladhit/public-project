<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	public function index() {
		redirect(base_url()."uon-admin/Report/ujian/");
	}

	// Frontend User CRUD
	public function ujian()
	{
		$crud = $this->generate_crud('data_ujian');
		$crud->where("selesai", "1");
		$state = $crud->getState();

		if ($state == "read") {
			$info = $crud->getStateInfo();
			redirect(base_url()."uon-admin/Report/ujian_detail/$info->primary_key");
		}

		$crud->columns('id_siswa', 'id_matpel', 'tanggal', 'waktu', 'total_score');
		$crud->display_as('id_siswa', 'Nama Siswa')
		->display_as('id_matpel', 'Mata Pelajaran')
		->display_as('total_score', 'Nilai');
		$crud->set_relation('id_siswa', 'data_siswa', 'nama');
		$crud->set_relation('id_matpel', 'data_mata_pelajaran', 'nama');
		
		// disable direct create / delete Frontend User
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_edit();
		//$crud->unset_read();

		$this->mPageTitle = 'Laporan Ujian';
		$this->render_crud();
	}

	public function ujian_detail($id_ujian) {
		$ujian_by_id = $this->db->where('id', $id_ujian)
		->get('data_ujian')->row();

		$ujian_by_siswa = $this->db->where('id_siswa', $ujian_by_id->id_siswa)
		->get('data_ujian')->result();

		
		//exit('hihihi');
		$crud = $this->generate_crud('data_ujian_detail');
		$crud->where("id_ujian", $id_ujian);
		$crud->columns("id_soal", "score");
		$crud->display_as("id_soal", "Soal")
		->display_as("score", "Jawaban");

		// Example by referring to a database id (e.g.12,13, 14... e.t.c.)
		$crud->callback_column("score", array($this, "cb_col_score"));

		$crud->set_relation('id_soal', 'data_soal', 'teks');
		
		$crud->unset_operations();
		
		$this->mPageTitle = 'Laporan Ujian';

		$labels = array();
		$dataset_mudah = array();
		$dataset_sedang = array();
		$dataset_sulit = array();

		foreach ($ujian_by_siswa as $key => $ubs) {

			if ($ujian_by_id->tanggal == $ubs->tanggal) {
				$labels[] = $ubs->tanggal . " " . $ubs->waktu;
			} else {
				$labels[] = $ubs->tanggal . " " . $ubs->waktu;
			}
			
			$ujian_detail = $this->db->select("data_ujian_detail.*, bobot")
			->where('id_ujian', $ubs->id)
			->join('data_soal', 'data_soal.id = id_soal')
			->get("data_ujian_detail")->result();

			$mudah = 0; $sedang = 0; $sulit = 0;
			foreach ($ujian_detail as $key => $ub) {
				if($ub->bobot == "mudah") {
					$mudah += ($ub->score * 1);
				} else if($ub->bobot == "sedang") {
					$sedang += ($ub->score * 2);
				} else if($ub->bobot == "sulit") {
					$sulit += ($ub->score * 3);
				}
			}

			$dataset_mudah[] = $mudah;
			$dataset_sedang[] = $sedang;
			$dataset_sulit[] = $sulit;
		}


		$this->mViewData['labels'] = $labels;
		$this->mViewData['dataset_mudah'] = $dataset_mudah;
		$this->mViewData['dataset_sedang'] = $dataset_sedang;
		$this->mViewData['dataset_sulit'] = $dataset_sulit;

		$this->render_grafik("grafik");
	}

	public function cb_col_score($value, $row){
		if ($value == 0) {
			return "Salah";
		} else {
			return "Benar";
		}
		
	}

	// Create Frontend User
	public function create()
	{
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$identity = $username;
			$data = array(
				'username'		=> $username,
				'password'		=> $this->ion_auth_model->hash_password($password),
				'nama'		=> $this->input->post('nama'),
				'nis'	=> $this->input->post('nis'),
				'jurusan'	=> $this->input->post('jurusan'),
				'kelas'		=> $this->input->post('kelas'),
				'alamat'	=> $this->input->post('alamat'),
				'no_hp'		=> $this->input->post('no_hp')
			);
			
			// proceed to create user
			$this->db->insert('data_siswa', $data);
			$user_id = $this->db->insert_id();
			if ($user_id)
			{
				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);

				// directly activate user
				//$this->ion_auth->activate($user_id);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		$this->mPageTitle = 'Create Siswa';

		$this->mViewData['form'] = $form;
		$this->render('siswa/create');
	}

	
	// Frontend User Reset Password
	public function reset_password($user_id)
	{
		// only top-level users can reset user passwords
		//$this->verify_auth(array('webmaster', 'admin'));

		$form = $this->form_builder->create_form();
		if ($form->validate())
		{
			// pass validation
			$data = array('password' => $this->input->post('new_password'));
			
			// proceed to change user password
			$this->ion_auth_model->tables['users'] = 'data_siswa';
			if ($this->ion_auth->update($user_id, $data))
			{
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
			}
			else
			{
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		$this->load->model('Siswa_model', 'siswa');
		$target = $this->siswa->get($user_id);
		$this->mViewData['target'] = $target;

		$this->mViewData['form'] = $form;
		$this->mPageTitle = 'Reset Password Siswa';
		$this->render('siswa/reset_password');
	}

	public function employees_schedule()
	{
		$crud = $this->generate_crud('employees');
		$crud->columns('username', 'email', 'schedule');
		$crud->fields('username', 'email', 'schedule');
		$crud->unset_add();

		$crud->set_relation_n_n('schedule', 'employees_schedule', 'schedule', 'employee_id', 'schedule_id', 'name');

		$this->mPageTitle = 'Employees Schedule';
		$this->render_crud();
	}

	// Admin Users CRUD
	public function schedule()
	{
		$crud = $this->generate_crud('schedule');
		$crud->columns('places', 'name', 'day', 'note');

		$crud->set_relation_n_n('places', 'schedule_groups', 'places', 'schedule_id', 'place_id', 'name');

		$this->mPageTitle = 'Schedule';
		$this->render_crud();
	}

	public function places() {
		$crud = $this->generate_crud('places');
		$crud->unset_texteditor("address","embed","embed2");
		$this->mPageTitle = 'Manage Places';
		$this->render_crud();
	}

	public function attendance()
	{
		$crud = $this->generate_crud('attendance');
		$crud->columns('username', 'place', 'day', 'checkin_caption', 'checkin_date','checkout_caption','checkout_date','checkin_image','checkout_image', 'rating');
		$crud->fields('place', 'day', 'checkin_caption', 'checkin_date','checkout_caption','checkout_date','checkin_image','checkout_image', 'rating');
		$crud->display_as('checkin_image', 'Before')->display_as('checkout_image','After');
		$crud->unset_add();
		//$crud->unset_edit();
		//$crud->unset_delete();
		$crud->unset_read();
		//$crud->unset_print();
		$crud->unset_export();
		//$crud->unset_operations();

		$crud->callback_column("username", array($this, "_cb_col_username"));
		$crud->callback_column("place", array($this, "_cb_col_place"));
		$crud->callback_column("day", array($this, "_cb_col_day"));
		$crud->callback_field("username", array($this, "_cb_field_username"));
		$crud->callback_field("place", array($this, "_cb_field_place"));
		$crud->callback_field("day", array($this, "_cb_field_day"));
		$crud->callback_column("checkin_date", array($this, "_cb_col_checkin_date"));
		$crud->callback_column("checkout_date", array($this, "_cb_col_checkout_date"));
		$crud->callback_column("checkin_image", array($this, "_cb_col_checkin_image"));
		$crud->callback_column("checkout_image", array($this, "_cb_col_checkout_image"));
		$crud->callback_field("checkin_image", array($this, "_cb_col_checkin_image"));
		$crud->callback_field("checkout_image", array($this, "_cb_col_checkout_image"));

		$this->mPageTitle = 'Attendance';
		$this->render_crud();
	}

	public function _cb_col_username($value, $row){
		$employees_schedule = $this->db->where("employees_schedule.id", $row->employees_schedule_id)
		->get("employees_schedule")->row();
		$employee = $this->db->where("employees.id", $employees_schedule->employee_id)
		->get("employees")->row();
		return $employee->username;
	}

	public function _cb_col_place($value, $row){
		$schedule_groups = $this->db->where("schedule_groups.id", $row->schedule_groups_id)
		->get("schedule_groups")->row();

		$place = $this->db->where("places.id", $schedule_groups->place_id)
		->get("places")->row();
		return $place->name;
	}

	public function _cb_col_day($value, $row){
		$schedule_groups = $this->db->where("schedule_groups.id", $row->schedule_groups_id)
		->get("schedule_groups")->row();

		$schedule = $this->db->where("schedule.id", $schedule_groups->schedule_id)
		->get("schedule")->row();
		return $schedule->name;
	}

	public function _cb_field_place($value, $primary_key){
		$attendance = $this->db->where("id", $primary_key)->get("attendance")->row();
		$schedule_groups = $this->db->where("schedule_groups.id", $attendance->schedule_groups_id)
		->get("schedule_groups")->row();

		$place = $this->db->where("places.id", $schedule_groups->place_id)
		->get("places")->row();
		//return $place->name;
		return "<input type='text' value='$place->name' readonly='true'>";
	}

	public function _cb_field_day($value, $primary_key){
		$attendance = $this->db->where("id", $primary_key)->get("attendance")->row();
		$schedule_groups = $this->db->where("schedule_groups.id", $attendance->schedule_groups_id)
		->get("schedule_groups")->row();

		$schedule = $this->db->where("schedule.id", $schedule_groups->schedule_id)
		->get("schedule")->row();
		//return $schedule->name;
		return "<input type='text' value='$schedule->name' readonly='true'>";
	}

	public function _cb_col_checkin_date($value, $row) {
		return $row->checkin_date. " " . $row->checkin_time;
	}

	public function _cb_col_checkout_date($value, $row) {
		return $row->checkout_date. " " . $row->checkout_time;
	}

	public function _cb_col_checkin_image($value, $row) {
		return "<a href='data:image/jpeg;base64, $value' target='_blank'><img class='img-responsive' src='data:image/jpeg;base64, $value'></a>";
	}

	public function _cb_col_checkout_image($value, $row) {
		return "<a href='data:image/jpeg;base64, $value' target='_blank'><img class='img-responsive' src='data:image/jpeg;base64, $value'></a>";
	}
}
