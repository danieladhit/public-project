<?php

/**
 * Helper class to handle authentication
 */

// Generate random password (e.g. for newly created user)
if (!function_exists('image_upload')) {
	function image_upload($name='image_upload', $image_path, $config=array(), $url='', $redirect=FALSE)
	{
		$config_var = array(
			'allowed_types'	=> 'gif|jpg|png|jpeg', 
			'max_size'		=> 5024, 
			'max_width'		=> 2600, 
			'max_height'	=> 2600, 
			'maintain_ratio'=> TRUE, 
			'remove_spaces'	=> TRUE,
		);

		foreach ($config_var as $key => $conf) {
			if (!isset($config[$key])) {
				$config[$key] = $conf;
			}
		}

		$ci =& get_instance();

		$config['upload_path'] = $image_path;
		/*$config['allowed_types'] = ;
		$config['max_size'] = ;
		$config['max_width'] = ;
		$config['max_height'] = ;
		$config['maintain_ratio'] = ;
		$config['remove_spaces'] = ;*/

		$ci->load->library('upload', $config);
		$ci->upload->initialize($config);

		if (!$ci->upload->do_upload($name)) {
			$error = array('error' => $ci->upload->display_errors());

			if ($redirect) {
				$ci->session->set_flashdata('error', $error['error']);
				redirect($url, 'refresh');
			} else {
				return array('status' => 0, 'value' => $error['error']);
			}

		} else {
			$image = $ci->upload->data();
			return array('status' => 1, 'value' => $config['upload_path'] . $image['file_name']);
		}

		return FALSE;

	}

}