<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Password Helper
 *
 * Uses PHP standard password hashing
 * @see http://php.net/manual/en/ref.password.php
 *
 * @author Sujeet <sujeetkv90@gmail.com>
 * @link https://github.com/sujeet-kumar/ci-password-helper
 */

if (!is_php('5.3.7')) {
    trigger_error('Password Helper requires PHP 5.3.7 or greater.', E_USER_ERROR);
    return;
}

if (version_compare(CI_VERSION, '3.0.0', '<') and ! is_php('5.5')) {
    trigger_error('Password Helper requires CodeIgniter 3.x or PHP 5.5.x', E_USER_ERROR);
    return;
}

/**
 * Set password hash cost here
 * @see http://php.net/manual/en/function.password-hash.php#example-982
 */
defined('PASSWORD_HASH_COST') or define('PASSWORD_HASH_COST', 10);


if ( ! function_exists('hash_password'))
{
    /**
     * Generates password hash
     * Returns the hashed password, or FALSE on failure.
     *
     * @param	string	$password
     * @return	mixed
     */
    function hash_password($password) {
        return password_hash($password, PASSWORD_DEFAULT, array('cost' => PASSWORD_HASH_COST));
    }
    
}


if ( ! function_exists('verify_password'))
{
    /**
     * Verifies that a password matches a hash
     *
     * If a valid hash_update_callback provided, a regenerated hash will be passed to this callback 
     * in case of PHP version update or PASSWORD_HASH_COST change.
     * This callback should be intended to replace old hash of password.
     * 
     * Usage Example:
     * --------------
     * if (verify_password('mypassword', $user['password'])) {
     *     // password valid
     * } else {
     *     // password invalid
     * }
     * 
     * Usage Example with Hash Update Callback:
     * ----------------------------------------
     * $user_id = $user['id'];
     * 
     * if (verify_password('mypassword', $user['password'], function ($new_hash) use ($user_id) {
     *     if ( ! empty($new_hash)) {
     *         get_instance()->db->where('id', $user_id)->update('users', array('password' => $new_hash));
     *     }
     * })) {
     *     // password valid
     * } else {
     *     // password invalid
     * }
     *
     *
     * @param	string		$password
     * @param	string		$hash
     * @param	callable	$hash_update_callback
     * @return	bool
     */
    function verify_password($password, $hash, $hash_update_callback = NULL) {
        if (password_verify($password, $hash)) {
            
            if ($hash_update_callback !== NULL) {
                if (!is_callable($hash_update_callback, false, $callback_name)) {
                    trigger_error('Invalid hash_update_callback \'' . $callback_name . '\' for verify_password()', E_USER_ERROR);
                    return false;
                } elseif (password_needs_rehash($hash, PASSWORD_DEFAULT, array('cost' => PASSWORD_HASH_COST))) {
                    if ($new_hash = hash_password($password)) {
                        call_user_func($hash_update_callback, $new_hash);
                    }
                }
            }
            
            return true;
        } else {
            return false;
        }
    }
    
}
