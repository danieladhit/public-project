<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home page
 */
class Email extends MY_Controller {

	public function index()
	{
		if($this->input->post("request_type") == "email") {
			$this->load->helper("email");
			
			$to = $this->input->post("to");
			$name = $this->input->post("to");
			$subject = $this->input->post("subject");
			$view = "general";
			$message = $this->input->post("message");

			$view_data["data"] = array(
				"message" => $message
			);

			send_email($to, $name, $subject, $view, $view_data);
			exit("send email success");
		}
		$this->render('email', 'full_width');
	}
}
