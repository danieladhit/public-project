#
# TABLE STRUCTURE FOR: admin_groups
#

DROP TABLE IF EXISTS `admin_groups`;

CREATE TABLE `admin_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `admin_groups` (`id`, `name`, `description`) VALUES ('1', 'admin', 'Administrator');
INSERT INTO `admin_groups` (`id`, `name`, `description`) VALUES ('2', 'vendor', 'Pemilik Kos');


#
# TABLE STRUCTURE FOR: admin_users
#

DROP TABLE IF EXISTS `admin_users`;

CREATE TABLE `admin_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `admin_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `full_name`) VALUES ('1', '127.0.0.1', 'admin', '$2y$08$7Bkco6JXtC3Hu6g9ngLZDuHsFLvT7cyAxiz1FzxlX5vwccvRT7nKW', NULL, NULL, NULL, NULL, NULL, NULL, '1451900228', '1565419644', '1', 'Admin');
INSERT INTO `admin_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `full_name`) VALUES ('6', '::1', 'adit', '$2y$08$JHV55CYaSYvlRsBNBajSTeglvNvoX2FOF7GlBuNp/PgfWh8W8y0Ji', NULL, 'adhityakristy@gmail.com', NULL, NULL, NULL, NULL, '1554794554', '1554795378', '1', 'Daniel Adhitya K');


#
# TABLE STRUCTURE FOR: admin_users_groups
#

DROP TABLE IF EXISTS `admin_users_groups`;

CREATE TABLE `admin_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `admin_users_groups` (`id`, `user_id`, `group_id`) VALUES ('1', '1', '1');
INSERT INTO `admin_users_groups` (`id`, `user_id`, `group_id`) VALUES ('5', '6', '2');


#
# TABLE STRUCTURE FOR: customers
#

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text,
  `telepon` varchar(20) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `waktu_registrasi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `customers` (`id`, `nama`, `email`, `alamat`, `telepon`, `password`, `waktu_registrasi`, `status`) VALUES ('2', 'Masbro', 'masbro@gmail.com', 'bekasi', '0123456', '$2y$10$SFcyLj4BFAWgqdWk/HC4uuh1WjF1Oi6tRtKtYj9jzO7Hquy7iE3ly', '2018-10-03 12:09:00', '1');
INSERT INTO `customers` (`id`, `nama`, `email`, `alamat`, `telepon`, `password`, `waktu_registrasi`, `status`) VALUES ('3', 'Jhedhot', 'jhedhot@gmail.com', 'jln testing', '0842325616', '$2y$10$lGbVcLF/E11W.TwvtfZRSu.rqP6VKKMp2Pif8H4PkexAjj6h1ha5u', '2018-10-11 09:13:06', '0');


#
# TABLE STRUCTURE FOR: kos_gallery
#

DROP TABLE IF EXISTS `kos_gallery`;

CREATE TABLE `kos_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kos_master` int(11) NOT NULL,
  `foto` varchar(150) NOT NULL,
  `urutan` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('3', '5', './image_upload/kos/placeimg_640_480_arch1.jpg', '2');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('4', '5', './image_upload/kos/favicon1.png', '3');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('5', '4', './image_upload/kos/kos21.jpg', '1');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('6', '3', './image_upload/kos/kos4.jpg', '1');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('7', '2', './image_upload/kos/kos1.jpg', '1');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('8', '2', './image_upload/kos/kos2.jpg', '2');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('10', '2', './image_upload/kos/kos51.jpg', '3');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('11', '1', './image_upload/kos/zQgBtaVl-540x720.jpg', '1');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('12', '1', './image_upload/kos/yiqFntR9-540x720.jpg', '2');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('13', '1', './image_upload/kos/W8cMOnDX-540x720.jpg', '3');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('14', '1', './image_upload/kos/csxBfxO0-540x720.jpg', '4');
INSERT INTO `kos_gallery` (`id`, `id_kos_master`, `foto`, `urutan`) VALUES ('15', '1', './image_upload/kos/A7yf467y-540x720.jpg', '5');


#
# TABLE STRUCTURE FOR: kos_history
#

DROP TABLE IF EXISTS `kos_history`;

CREATE TABLE `kos_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `awal_kos` date NOT NULL,
  `selesai_kos` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: kos_master
#

DROP TABLE IF EXISTS `kos_master`;

CREATE TABLE `kos_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendor` int(11) NOT NULL,
  `nama_kos` varchar(150) NOT NULL,
  `alamat` text NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `ex_kasur` int(1) NOT NULL,
  `ex_lemari` int(1) NOT NULL,
  `ex_ac` int(1) NOT NULL,
  `ex_kipas` int(11) NOT NULL,
  `ex_wifi` int(1) NOT NULL,
  `ex_tv` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO `kos_master` (`id`, `id_vendor`, `nama_kos`, `alamat`, `harga`, `jumlah`, `deskripsi`, `ex_kasur`, `ex_lemari`, `ex_ac`, `ex_kipas`, `ex_wifi`, `ex_tv`, `status`) VALUES ('1', '2', 'Kost Chintia - Janson Jakarta Barat', ' Grogol petamburan, Jakarta Barat, Jakarta ', '1600000', '4', ' Belakang terminal grogol/Belakang hotel 88 grogol ', '1', '1', '0', '1', '0', '0', '1');
INSERT INTO `kos_master` (`id`, `id_vendor`, `nama_kos`, `alamat`, `harga`, `jumlah`, `deskripsi`, `ex_kasur`, `ex_lemari`, `ex_ac`, `ex_kipas`, `ex_wifi`, `ex_tv`, `status`) VALUES ('2', '3', 'Kos Kedua', 'Bogor', '800000', '2', 'Nyaman', '1', '1', '0', '1', '0', '1', '1');
INSERT INTO `kos_master` (`id`, `id_vendor`, `nama_kos`, `alamat`, `harga`, `jumlah`, `deskripsi`, `ex_kasur`, `ex_lemari`, `ex_ac`, `ex_kipas`, `ex_wifi`, `ex_tv`, `status`) VALUES ('3', '2', 'Kos Ketiga', 'Jakarta', '1000000', '1', 'Nyaman', '1', '0', '0', '0', '0', '0', '1');
INSERT INTO `kos_master` (`id`, `id_vendor`, `nama_kos`, `alamat`, `harga`, `jumlah`, `deskripsi`, `ex_kasur`, `ex_lemari`, `ex_ac`, `ex_kipas`, `ex_wifi`, `ex_tv`, `status`) VALUES ('4', '3', 'Kos Keempat', 'Bandung', '1500000', '2', 'Nyaman', '1', '0', '1', '0', '0', '0', '1');
INSERT INTO `kos_master` (`id`, `id_vendor`, `nama_kos`, `alamat`, `harga`, `jumlah`, `deskripsi`, `ex_kasur`, `ex_lemari`, `ex_ac`, `ex_kipas`, `ex_wifi`, `ex_tv`, `status`) VALUES ('5', '2', 'Kos Kelima', 'Surabaya', '400000', '10', 'Murah', '1', '1', '1', '0', '1', '0', '1');


#
# TABLE STRUCTURE FOR: kos_periode
#

DROP TABLE IF EXISTS `kos_periode`;

CREATE TABLE `kos_periode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jumlah` int(11) NOT NULL,
  `waktu` enum('Bulan','Tahun') NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `kos_periode` (`id`, `jumlah`, `waktu`, `keterangan`) VALUES ('1', '1', 'Bulan', 'Sewa 1 Bulan');
INSERT INTO `kos_periode` (`id`, `jumlah`, `waktu`, `keterangan`) VALUES ('2', '6', 'Bulan', 'Sewa 6 Bulan');
INSERT INTO `kos_periode` (`id`, `jumlah`, `waktu`, `keterangan`) VALUES ('3', '1', 'Tahun', 'Sewa 1 Tahun');


#
# TABLE STRUCTURE FOR: orders
#

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `id_kos_master` int(11) NOT NULL,
  `id_kos_periode` int(11) NOT NULL,
  `waktu_order` datetime NOT NULL,
  `harga_sewa` int(11) NOT NULL,
  `bukti_pembayaran` varchar(200) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO `orders` (`id`, `id_customer`, `id_kos_master`, `id_kos_periode`, `waktu_order`, `harga_sewa`, `bukti_pembayaran`, `status`) VALUES ('1', '2', '1', '1', '2018-10-04 00:00:00', '0', '', '0');
INSERT INTO `orders` (`id`, `id_customer`, `id_kos_master`, `id_kos_periode`, `waktu_order`, `harga_sewa`, `bukti_pembayaran`, `status`) VALUES ('4', '2', '1', '2', '2018-10-08 00:00:00', '0', '', '0');
INSERT INTO `orders` (`id`, `id_customer`, `id_kos_master`, `id_kos_periode`, `waktu_order`, `harga_sewa`, `bukti_pembayaran`, `status`) VALUES ('5', '2', '1', '3', '2018-10-11 00:00:00', '0', '', '0');
INSERT INTO `orders` (`id`, `id_customer`, `id_kos_master`, `id_kos_periode`, `waktu_order`, `harga_sewa`, `bukti_pembayaran`, `status`) VALUES ('6', '2', '1', '2', '2018-10-11 00:00:00', '0', '', '0');
INSERT INTO `orders` (`id`, `id_customer`, `id_kos_master`, `id_kos_periode`, `waktu_order`, `harga_sewa`, `bukti_pembayaran`, `status`) VALUES ('7', '2', '2', '3', '2018-10-11 00:00:00', '0', 'elearning_logo.png', '0');
INSERT INTO `orders` (`id`, `id_customer`, `id_kos_master`, `id_kos_periode`, `waktu_order`, `harga_sewa`, `bukti_pembayaran`, `status`) VALUES ('8', '3', '1', '1', '2018-10-11 00:00:00', '0', 'teamviewer.png', '0');
INSERT INTO `orders` (`id`, `id_customer`, `id_kos_master`, `id_kos_periode`, `waktu_order`, `harga_sewa`, `bukti_pembayaran`, `status`) VALUES ('9', '2', '1', '3', '2018-10-13 00:00:00', '0', '', '0');
INSERT INTO `orders` (`id`, `id_customer`, `id_kos_master`, `id_kos_periode`, `waktu_order`, `harga_sewa`, `bukti_pembayaran`, `status`) VALUES ('10', '2', '1', '1', '2018-10-18 00:00:00', '0', '', '0');
INSERT INTO `orders` (`id`, `id_customer`, `id_kos_master`, `id_kos_periode`, `waktu_order`, `harga_sewa`, `bukti_pembayaran`, `status`) VALUES ('11', '2', '2', '2', '2018-10-18 00:00:00', '0', '', '0');


#
# TABLE STRUCTURE FOR: site_customisation
#

DROP TABLE IF EXISTS `site_customisation`;

CREATE TABLE `site_customisation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `color_scheme` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `site_title` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `site_customisation` (`id`, `company_name`, `email`, `telephone`, `address`, `color_scheme`, `logo`, `site_title`) VALUES ('1', 'Global Asia Sinergi', 'tes@sinergi.com', '02182323277', 'Batavia Tower', 'skin-purple', './image_upload/logo/logo_gas10.jpeg', 'SEMERU');


#
# TABLE STRUCTURE FOR: vendors
#

DROP TABLE IF EXISTS `vendors`;

CREATE TABLE `vendors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text,
  `telepon` varchar(20) DEFAULT NULL,
  `waktu_registrasi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `vendors` (`id`, `nama`, `email`, `alamat`, `telepon`, `waktu_registrasi`, `status`) VALUES ('2', 'Masbro', 'masbro@gmail.com', 'bekasi', '0123456', '2018-10-03 12:09:00', NULL);
INSERT INTO `vendors` (`id`, `nama`, `email`, `alamat`, `telepon`, `waktu_registrasi`, `status`) VALUES ('3', 'Jhedhot', 'jhedhot@gmail.com', 'jln testing', '0842325616', '2018-10-11 09:13:06', '0');
INSERT INTO `vendors` (`id`, `nama`, `email`, `alamat`, `telepon`, `waktu_registrasi`, `status`) VALUES ('6', 'Daniel Adhitya K', 'adhityakristy@gmail.com', 'Bekasi', '08123434343', '2019-04-09 07:22:34', '1');


#
# TABLE STRUCTURE FOR: web_profile
#

DROP TABLE IF EXISTS `web_profile`;

CREATE TABLE `web_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `about_us` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `web_profile` (`id`, `about_us`) VALUES ('1', ' Aktifitas yang padat dan tuntutan peran yang tinggi, menyebabkan para muslimah perlu meluangkan waktu sejenak untuk rileks dan menyegarkan pikiran. Dengan tubuh dan pikiran yang fresh, aktifitas sehari-hari menjadi lebih indah dan bermakna. Itulah mengapa ‘nyalon” kini menjadi kebutuhan dasar bagi setiap muslimah.\r\n<br>\r\nAtas kepedulian kepada para muslimah inilah, maka pada Mei 2002 berdiri Salon Muslimah hijra. Salon khusus muslimah dengan pelayanan yang lengkap dan berkualitas, dengan demikian, muslimah bisa melakukan perawatan kecantikan, tidak hanya cantik fisik tetapi juga cantik dari dalam.\r\n<br>\r\nPelayanan Salon Muslimah hijra terdiri dari perawatan rambut (creambath, masker rambut, hairspa, gunting, coloring, rebonding, dll), perawatan tubuh (pijat, lulur, masker tubuh, sauna), perawatan wajah (termasuk perawatan mata dan telinga), manicure, pedicure, hingga refleksi, totok aura, dan rias pengantin.');


